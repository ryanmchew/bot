namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SubcontractorScheduling")]
    public partial class SubcontractorScheduling
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SubcontractorID { get; set; }

        [StringLength(512)]
        public string Email { get; set; }

        [StringLength(128)]
        public string PrimaryPhone { get; set; }

        [StringLength(128)]
        public string BackupPhone1 { get; set; }

        [StringLength(128)]
        public string BackupPhone2 { get; set; }

        public virtual Subcontractor Subcontractor { get; set; }
    }
}
