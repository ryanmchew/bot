namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobTask")]
    public partial class JobTask
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobTask()
        {
            JobTaskEmployees = new HashSet<JobTaskEmployee>();
            JobTaskSubcontractors = new HashSet<JobTaskSubcontractor>();
        }

        public int JobTaskID { get; set; }

        public int JobID { get; set; }

        public int? ScheduleTaskID { get; set; }

        [Required]
        [StringLength(256)]
        public string TaskName { get; set; }

        [StringLength(16)]
        public string TimeFrame { get; set; }

        public DateTime ScheduleStart { get; set; }

        public DateTime ScheduleFinish { get; set; }

        public DateTime? Start { get; set; }

        public DateTime? Finish { get; set; }

        public DateTime? Signoff { get; set; }

        public int? SignoffEmployeeID { get; set; }

        [StringLength(128)]
        public string GroupName { get; set; }

        public DateTime Updated { get; set; }

        public virtual Job Job { get; set; }

        public virtual ScheduleTask ScheduleTask { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobTaskEmployee> JobTaskEmployees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobTaskSubcontractor> JobTaskSubcontractors { get; set; }
    }
}
