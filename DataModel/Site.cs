namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Site")]
    public partial class Site
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Site()
        {
            Jobs = new HashSet<Job>();
        }

        public int SiteID { get; set; }

        public int BuilderID { get; set; }

        [StringLength(128)]
        public string LotIdentifier { get; set; }

        [StringLength(128)]
        public string AddressLine1 { get; set; }

        [StringLength(128)]
        public string AddressLine2 { get; set; }

        [StringLength(128)]
        public string City { get; set; }

        [StringLength(64)]
        public string State { get; set; }

        [StringLength(64)]
        public string PostalCode { get; set; }

        public virtual Builder Builder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Job> Jobs { get; set; }
    }
}
