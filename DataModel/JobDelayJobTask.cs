namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobDelayJobTask")]
    public partial class JobDelayJobTask
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JobDelayID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JobTaskID { get; set; }

        public DateTime OriginalStart { get; set; }

        public DateTime OriginalFinish { get; set; }

        public DateTime Start { get; set; }

        public DateTime Finish { get; set; }
    }
}
