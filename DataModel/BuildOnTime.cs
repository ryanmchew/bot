namespace DataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BuildOnTime : DbContext
    {
        public BuildOnTime()
            : base("name=BuildOnTime")
        {
        }

        public virtual DbSet<aspnet_Applications> aspnet_Applications { get; set; }
        public virtual DbSet<aspnet_Membership> aspnet_Membership { get; set; }
        public virtual DbSet<aspnet_Paths> aspnet_Paths { get; set; }
        public virtual DbSet<aspnet_PersonalizationAllUsers> aspnet_PersonalizationAllUsers { get; set; }
        public virtual DbSet<aspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser { get; set; }
        public virtual DbSet<aspnet_Profile> aspnet_Profile { get; set; }
        public virtual DbSet<aspnet_Roles> aspnet_Roles { get; set; }
        public virtual DbSet<aspnet_SchemaVersions> aspnet_SchemaVersions { get; set; }
        public virtual DbSet<aspnet_Users> aspnet_Users { get; set; }
        public virtual DbSet<aspnet_WebEvent_Events> aspnet_WebEvent_Events { get; set; }
        public virtual DbSet<AuditLog> AuditLogs { get; set; }
        public virtual DbSet<Builder> Builders { get; set; }
        public virtual DbSet<EMail> EMails { get; set; }
        public virtual DbSet<EMailTemplate> EMailTemplates { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobDelay> JobDelays { get; set; }
        public virtual DbSet<JobDelayJobTask> JobDelayJobTasks { get; set; }
        public virtual DbSet<JobTask> JobTasks { get; set; }
        public virtual DbSet<JobTaskDependent> JobTaskDependents { get; set; }
        public virtual DbSet<Schedule> Schedules { get; set; }
        public virtual DbSet<ScheduleTask> ScheduleTasks { get; set; }
        public virtual DbSet<ScheduleTaskDependent> ScheduleTaskDependents { get; set; }
        public virtual DbSet<Site> Sites { get; set; }
        public virtual DbSet<Subcontractor> Subcontractors { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<SystemUser> SystemUsers { get; set; }
        public virtual DbSet<EmployeeCommunication> EmployeeCommunications { get; set; }
        public virtual DbSet<HomeOwner> HomeOwners { get; set; }
        public virtual DbSet<JobTaskEmployee> JobTaskEmployees { get; set; }
        public virtual DbSet<JobTaskSubcontractor> JobTaskSubcontractors { get; set; }
        public virtual DbSet<LogCRUD> LogCRUDs { get; set; }
        public virtual DbSet<SubcontractorCommunication> SubcontractorCommunications { get; set; }
        public virtual DbSet<SubcontractorScheduling> SubcontractorSchedulings { get; set; }
        public virtual DbSet<vw_aspnet_Applications> vw_aspnet_Applications { get; set; }
        public virtual DbSet<vw_aspnet_MembershipUsers> vw_aspnet_MembershipUsers { get; set; }
        public virtual DbSet<vw_aspnet_Profiles> vw_aspnet_Profiles { get; set; }
        public virtual DbSet<vw_aspnet_Roles> vw_aspnet_Roles { get; set; }
        public virtual DbSet<vw_aspnet_Users> vw_aspnet_Users { get; set; }
        public virtual DbSet<vw_aspnet_UsersInRoles> vw_aspnet_UsersInRoles { get; set; }
        public virtual DbSet<vw_aspnet_WebPartState_Paths> vw_aspnet_WebPartState_Paths { get; set; }
        public virtual DbSet<vw_aspnet_WebPartState_Shared> vw_aspnet_WebPartState_Shared { get; set; }
        public virtual DbSet<vw_aspnet_WebPartState_User> vw_aspnet_WebPartState_User { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Membership)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Paths)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Roles)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Users)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Membership>()
                .HasMany(e => e.EMails)
                .WithRequired(e => e.aspnet_Membership)
                .HasForeignKey(e => e.FromSystemUserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Membership>()
                .HasMany(e => e.Employees)
                .WithOptional(e => e.aspnet_Membership)
                .HasForeignKey(e => e.SystemUserID);

            modelBuilder.Entity<aspnet_Membership>()
                .HasMany(e => e.Subcontractors)
                .WithMany(e => e.aspnet_Membership)
                .Map(m => m.ToTable("SystemUserSubcontractor").MapLeftKey("SystemUserID").MapRightKey("SubcontractorID"));

            modelBuilder.Entity<aspnet_Paths>()
                .HasOptional(e => e.aspnet_PersonalizationAllUsers)
                .WithRequired(e => e.aspnet_Paths);

            modelBuilder.Entity<aspnet_Roles>()
                .HasMany(e => e.aspnet_Users)
                .WithMany(e => e.aspnet_Roles)
                .Map(m => m.ToTable("aspnet_UsersInRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<aspnet_Users>()
                .HasOptional(e => e.aspnet_Membership)
                .WithRequired(e => e.aspnet_Users);

            modelBuilder.Entity<aspnet_Users>()
                .HasOptional(e => e.aspnet_Profile)
                .WithRequired(e => e.aspnet_Users);

            modelBuilder.Entity<aspnet_WebEvent_Events>()
                .Property(e => e.EventId)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<aspnet_WebEvent_Events>()
                .Property(e => e.EventSequence)
                .HasPrecision(19, 0);

            modelBuilder.Entity<aspnet_WebEvent_Events>()
                .Property(e => e.EventOccurrence)
                .HasPrecision(19, 0);

            modelBuilder.Entity<AuditLog>()
                .Property(e => e.Action)
                .IsUnicode(false);

            modelBuilder.Entity<AuditLog>()
                .Property(e => e.Target)
                .IsUnicode(false);

            modelBuilder.Entity<AuditLog>()
                .Property(e => e.Catalyst)
                .IsUnicode(false);

            modelBuilder.Entity<AuditLog>()
                .Property(e => e.Details)
                .IsUnicode(false);

            modelBuilder.Entity<Builder>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Builder>()
                .Property(e => e.AddressLine1)
                .IsUnicode(false);

            modelBuilder.Entity<Builder>()
                .Property(e => e.AddressLine2)
                .IsUnicode(false);

            modelBuilder.Entity<Builder>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Builder>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Builder>()
                .Property(e => e.PostalCode)
                .IsUnicode(false);

            modelBuilder.Entity<Builder>()
                .Property(e => e.OfficePhone)
                .IsUnicode(false);

            modelBuilder.Entity<Builder>()
                .Property(e => e.OfficeFax)
                .IsUnicode(false);

            modelBuilder.Entity<Builder>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.Builder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Builder>()
                .HasMany(e => e.Jobs)
                .WithRequired(e => e.Builder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Builder>()
                .HasMany(e => e.Schedules)
                .WithRequired(e => e.Builder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Builder>()
                .HasMany(e => e.Sites)
                .WithRequired(e => e.Builder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Builder>()
                .HasMany(e => e.Subcontractors)
                .WithRequired(e => e.Builder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EMail>()
                .Property(e => e.To)
                .IsUnicode(false);

            modelBuilder.Entity<EMail>()
                .Property(e => e.CC)
                .IsUnicode(false);

            modelBuilder.Entity<EMail>()
                .Property(e => e.BCC)
                .IsUnicode(false);

            modelBuilder.Entity<EMail>()
                .Property(e => e.Subject)
                .IsUnicode(false);

            modelBuilder.Entity<EMail>()
                .Property(e => e.Body)
                .IsUnicode(false);

            modelBuilder.Entity<EMailTemplate>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<EMailTemplate>()
                .Property(e => e.Version)
                .IsUnicode(false);

            modelBuilder.Entity<EMailTemplate>()
                .Property(e => e.Subject)
                .IsUnicode(false);

            modelBuilder.Entity<EMailTemplate>()
                .Property(e => e.Body)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Roles)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.WorkPhone)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.CellPhone)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.HomePhone)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Employee1)
                .WithOptional(e => e.Employee2)
                .HasForeignKey(e => e.SupervisorID);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.JobTaskEmployees)
                .WithRequired(e => e.Employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.Permit)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.HomeOwners)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobTasks)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobDelay>()
                .Property(e => e.DelayedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobDelay>()
                .Property(e => e.Reason)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.TaskName)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.TimeFrame)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.GroupName)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .HasMany(e => e.JobTaskEmployees)
                .WithRequired(e => e.JobTask)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobTask>()
                .HasMany(e => e.JobTaskSubcontractors)
                .WithRequired(e => e.JobTask)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Schedule>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Schedule>()
                .HasMany(e => e.ScheduleTasks)
                .WithRequired(e => e.Schedule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Schedule>()
                .HasMany(e => e.ScheduleTaskDependents)
                .WithRequired(e => e.Schedule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScheduleTask>()
                .Property(e => e.Task)
                .IsUnicode(false);

            modelBuilder.Entity<ScheduleTask>()
                .Property(e => e.GroupName)
                .IsUnicode(false);

            modelBuilder.Entity<ScheduleTask>()
                .HasMany(e => e.ScheduleTaskDependents)
                .WithRequired(e => e.ScheduleTask)
                .HasForeignKey(e => e.ParentTaskID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScheduleTask>()
                .HasMany(e => e.ScheduleTaskDependents1)
                .WithRequired(e => e.ScheduleTask1)
                .HasForeignKey(e => e.DependentTaskID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScheduleTask>()
                .HasMany(e => e.Subcontractors)
                .WithMany(e => e.ScheduleTasks)
                .Map(m => m.ToTable("SubcontractorScheduleTask").MapLeftKey("ScheduleTaskID").MapRightKey("SubcontractorID"));

            modelBuilder.Entity<Site>()
                .HasMany(e => e.Jobs)
                .WithRequired(e => e.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Subcontractor>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Subcontractor>()
                .Property(e => e.AddressLine1)
                .IsUnicode(false);

            modelBuilder.Entity<Subcontractor>()
                .Property(e => e.AddressLine2)
                .IsUnicode(false);

            modelBuilder.Entity<Subcontractor>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Subcontractor>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Subcontractor>()
                .Property(e => e.PostalCode)
                .IsUnicode(false);

            modelBuilder.Entity<Subcontractor>()
                .Property(e => e.OfficePhone)
                .IsUnicode(false);

            modelBuilder.Entity<Subcontractor>()
                .Property(e => e.OfficeFax)
                .IsUnicode(false);

            modelBuilder.Entity<Subcontractor>()
                .HasMany(e => e.JobTaskSubcontractors)
                .WithRequired(e => e.Subcontractor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Subcontractor>()
                .HasOptional(e => e.SubcontractorScheduling)
                .WithRequired(e => e.Subcontractor);

            modelBuilder.Entity<EmployeeCommunication>()
                .Property(e => e.Reason)
                .IsUnicode(false);

            modelBuilder.Entity<EmployeeCommunication>()
                .Property(e => e.Method)
                .IsUnicode(false);

            modelBuilder.Entity<EmployeeCommunication>()
                .Property(e => e.Channel)
                .IsUnicode(false);

            modelBuilder.Entity<EmployeeCommunication>()
                .Property(e => e.Notes)
                .IsUnicode(false);

            modelBuilder.Entity<HomeOwner>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<HomeOwner>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<HomeOwner>()
                .Property(e => e.HomePhone)
                .IsUnicode(false);

            modelBuilder.Entity<HomeOwner>()
                .Property(e => e.CellPhone)
                .IsUnicode(false);

            modelBuilder.Entity<HomeOwner>()
                .Property(e => e.WorkPhone)
                .IsUnicode(false);

            modelBuilder.Entity<LogCRUD>()
                .Property(e => e.TableName)
                .IsUnicode(false);

            modelBuilder.Entity<LogCRUD>()
                .Property(e => e.Action)
                .IsUnicode(false);

            modelBuilder.Entity<LogCRUD>()
                .Property(e => e.Reason)
                .IsUnicode(false);

            modelBuilder.Entity<LogCRUD>()
                .Property(e => e.Initiator)
                .IsUnicode(false);

            modelBuilder.Entity<SubcontractorCommunication>()
                .Property(e => e.Reason)
                .IsUnicode(false);

            modelBuilder.Entity<SubcontractorCommunication>()
                .Property(e => e.Method)
                .IsUnicode(false);

            modelBuilder.Entity<SubcontractorCommunication>()
                .Property(e => e.Channel)
                .IsUnicode(false);

            modelBuilder.Entity<SubcontractorCommunication>()
                .Property(e => e.Notes)
                .IsUnicode(false);

            modelBuilder.Entity<SubcontractorScheduling>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<SubcontractorScheduling>()
                .Property(e => e.PrimaryPhone)
                .IsUnicode(false);

            modelBuilder.Entity<SubcontractorScheduling>()
                .Property(e => e.BackupPhone1)
                .IsUnicode(false);

            modelBuilder.Entity<SubcontractorScheduling>()
                .Property(e => e.BackupPhone2)
                .IsUnicode(false);
        }
    }
}
