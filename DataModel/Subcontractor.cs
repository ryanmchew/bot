namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Subcontractor")]
    public partial class Subcontractor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Subcontractor()
        {
            JobTaskSubcontractors = new HashSet<JobTaskSubcontractor>();
            ScheduleTasks = new HashSet<ScheduleTask>();
            aspnet_Membership = new HashSet<aspnet_Membership>();
        }

        public int SubcontractorID { get; set; }

        public int BuilderID { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        [StringLength(256)]
        public string AddressLine1 { get; set; }

        [StringLength(256)]
        public string AddressLine2 { get; set; }

        [StringLength(128)]
        public string City { get; set; }

        [StringLength(64)]
        public string State { get; set; }

        [StringLength(64)]
        public string PostalCode { get; set; }

        [StringLength(64)]
        public string OfficePhone { get; set; }

        [StringLength(64)]
        public string OfficeFax { get; set; }

        public virtual Builder Builder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobTaskSubcontractor> JobTaskSubcontractors { get; set; }

        public virtual SubcontractorScheduling SubcontractorScheduling { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScheduleTask> ScheduleTasks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<aspnet_Membership> aspnet_Membership { get; set; }
    }
}
