namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EMailTemplate")]
    public partial class EMailTemplate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EMailTemplate()
        {
            EMails = new HashSet<EMail>();
        }

        public int EMailTemplateID { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        [Required]
        [StringLength(256)]
        public string Version { get; set; }

        [Required]
        [StringLength(256)]
        public string Subject { get; set; }

        [Required]
        public string Body { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EMail> EMails { get; set; }
    }
}
