namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobTaskEmployee")]
    public partial class JobTaskEmployee
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JobTaskID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeID { get; set; }

        public DateTime? Rejected { get; set; }

        public DateTime? Confirmed { get; set; }

        public DateTime? Finished { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime Assigned { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual JobTask JobTask { get; set; }
    }
}
