namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LogCRUD")]
    public partial class LogCRUD
    {
        [Key]
        [Column(Order = 0)]
        public int LogCrudId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string TableName { get; set; }

        [Key]
        [Column(Order = 2)]
        public string Action { get; set; }

        [StringLength(256)]
        public string Reason { get; set; }

        [Key]
        [Column(Order = 3, TypeName = "xml")]
        public string Value { get; set; }

        [StringLength(256)]
        public string Initiator { get; set; }

        public int? UserId { get; set; }

        [Key]
        [Column(Order = 4)]
        public DateTime LogDateTime { get; set; }
    }
}
