namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobTaskSubcontractor")]
    public partial class JobTaskSubcontractor
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JobTaskID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SubcontractorID { get; set; }

        public DateTime? Rejected { get; set; }

        public DateTime? Confirmed { get; set; }

        public DateTime? Finished { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime Assigned { get; set; }

        public virtual JobTask JobTask { get; set; }

        public virtual Subcontractor Subcontractor { get; set; }
    }
}
