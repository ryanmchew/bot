namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AuditLog")]
    public partial class AuditLog
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuditLogID { get; set; }

        [Key]
        [Column(Order = 0)]
        public Guid SystemUserID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(32)]
        public string Action { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(64)]
        public string Target { get; set; }

        [Required]
        [StringLength(32)]
        public string Catalyst { get; set; }

        [Required]
        [StringLength(2000)]
        public string Details { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTime Occurred { get; set; }
    }
}
