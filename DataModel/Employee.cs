namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employee")]
    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            Employee1 = new HashSet<Employee>();
            JobTaskEmployees = new HashSet<JobTaskEmployee>();
        }

        public int EmployeeID { get; set; }

        public int BuilderID { get; set; }

        public Guid? SystemUserID { get; set; }

        public int? SupervisorID { get; set; }

        [Required]
        [StringLength(512)]
        public string Roles { get; set; }

        [Required]
        [StringLength(256)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(256)]
        public string LastName { get; set; }

        [StringLength(256)]
        public string WorkPhone { get; set; }

        [StringLength(256)]
        public string CellPhone { get; set; }

        [StringLength(256)]
        public string HomePhone { get; set; }

        [StringLength(256)]
        public string Fax { get; set; }

        public DateTime Updated { get; set; }

        public virtual aspnet_Membership aspnet_Membership { get; set; }

        public virtual Builder Builder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employee> Employee1 { get; set; }

        public virtual Employee Employee2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobTaskEmployee> JobTaskEmployees { get; set; }
    }
}
