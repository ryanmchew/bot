namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EmployeeCommunication")]
    public partial class EmployeeCommunication
    {
        [Key]
        [Column(Order = 0)]
        public int EmployeeCommunicationID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BuilderID { get; set; }

        public int? EmployeeCommunicatorID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeID { get; set; }

        public int? JobTaskID { get; set; }

        public int? EmailID { get; set; }

        [Key]
        [Column(Order = 3)]
        public string Reason { get; set; }

        [Key]
        [Column(Order = 4)]
        public string Method { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(1024)]
        public string Channel { get; set; }

        [StringLength(2048)]
        public string Notes { get; set; }

        [Key]
        [Column(Order = 6)]
        public DateTime Created { get; set; }
    }
}
