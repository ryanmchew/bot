namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EMail")]
    public partial class EMail
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmailID { get; set; }

        [Key]
        [Column(Order = 0)]
        public Guid FromSystemUserID { get; set; }

        public int? EMailTemplateID { get; set; }

        public string To { get; set; }

        public string CC { get; set; }

        public string BCC { get; set; }

        [StringLength(256)]
        public string Subject { get; set; }

        public string Body { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime Sent { get; set; }

        public virtual aspnet_Membership aspnet_Membership { get; set; }

        public virtual EMailTemplate EMailTemplate { get; set; }
    }
}
