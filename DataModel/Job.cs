namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Job")]
    public partial class Job
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Job()
        {
            HomeOwners = new HashSet<HomeOwner>();
            JobTasks = new HashSet<JobTask>();
        }

        public int JobID { get; set; }

        public int BuilderID { get; set; }

        public int? EmployeeID { get; set; }

        public int SiteID { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        [StringLength(128)]
        public string Permit { get; set; }

        public string Description { get; set; }

        public DateTime? Created { get; set; }

        public virtual Builder Builder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HomeOwner> HomeOwners { get; set; }

        public virtual Site Site { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobTask> JobTasks { get; set; }
    }
}
