namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ScheduleTask")]
    public partial class ScheduleTask
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ScheduleTask()
        {
            JobTasks = new HashSet<JobTask>();
            ScheduleTaskDependents = new HashSet<ScheduleTaskDependent>();
            ScheduleTaskDependents1 = new HashSet<ScheduleTaskDependent>();
            Subcontractors = new HashSet<Subcontractor>();
        }

        public int ScheduleTaskID { get; set; }

        public int ScheduleID { get; set; }

        public int? DefaultSubcontractorID { get; set; }

        public int? DefaultEmployeeID { get; set; }

        [Required]
        [StringLength(256)]
        public string Task { get; set; }

        public int DayNumber { get; set; }

        public int Duration { get; set; }

        [StringLength(128)]
        public string GroupName { get; set; }

        public DateTime Updated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobTask> JobTasks { get; set; }

        public virtual Schedule Schedule { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScheduleTaskDependent> ScheduleTaskDependents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScheduleTaskDependent> ScheduleTaskDependents1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Subcontractor> Subcontractors { get; set; }
    }
}
