namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobDelay")]
    public partial class JobDelay
    {
        public int JobDelayID { get; set; }

        public int JobID { get; set; }

        [Required]
        [StringLength(128)]
        public string DelayedBy { get; set; }

        public int? DelayedByEmployeeID { get; set; }

        public int? DelayedBySubcontractorID { get; set; }

        public int DelayLength { get; set; }

        [StringLength(2000)]
        public string Reason { get; set; }

        public DateTime Entered { get; set; }
    }
}
