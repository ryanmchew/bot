namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Schedule")]
    public partial class Schedule
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Schedule()
        {
            ScheduleTasks = new HashSet<ScheduleTask>();
            ScheduleTaskDependents = new HashSet<ScheduleTaskDependent>();
        }

        public int ScheduleID { get; set; }

        public int BuilderID { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        public virtual Builder Builder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScheduleTask> ScheduleTasks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScheduleTaskDependent> ScheduleTaskDependents { get; set; }
    }
}
