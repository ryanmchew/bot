﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{
    /// <summary>
    /// Naming convention is 
    /// WhoWhatAction (e.g. BuilderJobRejected means e-mail is going to builder about a job rejected)
    /// </summary>
    public enum EMailTemplateEnum
    {
        BuilderJobAccepted,
        BuilderJobFinished,
        BuilderJobRejected,
        BuilderWelcome,
        PasswordReset,
        SubJobReminder,
        SubJobRequest,
        SubJobUpdated,
        SubScheduleUpdate,
        SubWelcome
    }

    public abstract class EMailBase
    {
        public string Subject { get; set; }
        public string Body { get; set; }
    }

    public class EMail : EMailBase
    {
        public object Identifier { get; set; }
        public object EmailTemplateID { get; set; }
        public object SenderSystemUserID { get; set; }
        public string To { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string From { get; set; }
        public string ReplyTo { get; set; }
        public DateTime Sent { get; set; }
    }

    public class EMailTemplate : EMailBase
    {
        public object Identifier { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
