﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{
    public enum CommunicationMethodEnum
    {
        EMail,
        Phone,
        Fax,
        TextMessage
    }

    public enum CommunicationReasonEnum
    {
        Signup,
        Scheduling,        
        Reminding,
        Rescheduling,
        Accepting,
        Rejecting,
        Finishing,
        Composite
    }

    public class Communication
    {
        public object Identifier { get; set; }
        public object BuilderID { get; set; }
        public object EmployeeCommunicatorID { get; set; }
        public object JobTaskID { get; set; }
        public object EMailID { get; set; }
        public CommunicationReasonEnum Reason { get; set; }
        public CommunicationMethodEnum Method { get; set; }
        public string Channel { get; set; }
        public string Notes { get; set; }
        public DateTime Created { get; set; }

        public void CopyTo(Communication communication)
        {
            communication.BuilderID = this.BuilderID;
            communication.Channel = this.Channel;
            communication.Created = this.Created;
            communication.EMailID = this.EMailID;
            communication.EmployeeCommunicatorID = this.EmployeeCommunicatorID;
            communication.Identifier = this.Identifier;
            communication.JobTaskID = this.JobTaskID;
            communication.Method = this.Method;
            communication.Notes = this.Notes;
            communication.Reason = this.Reason;
        }
    }

    public class EmployeeCommunication : Communication
    {
        public object EmployeeID { get; set; }
    }

    public class SubcontractorCommunication: Communication
    {
        public object SubcontractorID { get; set; }

        public void CopyTo(SubcontractorCommunication result)
        {
            base.CopyTo(result);
            result.SubcontractorID = this.SubcontractorID;
        }
    }
}
