﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{
    /*
    public partial class SystemUser : Persistable
    {
        public SystemUser()
        {

        }

        public SystemUser(SystemUser copyFrom)
        {
            this.CellPhone = copyFrom.CellPhone;
            this.EMail = copyFrom.EMail;
            this.Fax = copyFrom.Fax;
            this.FirstName = copyFrom.FirstName;
            this.HomePhone = copyFrom.HomePhone;
            this.Identifier = copyFrom.Identifier;
            this.LastLogin = copyFrom.LastLogin;
            this.LastName = copyFrom.LastName;
            this.Username = copyFrom.Username;
            this.WorkPhone = copyFrom.WorkPhone;
        }

        public DateTime LastLogin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string Username { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string Fax { get; set; }
    }

    public class UserBuilder : SystemUser
    {
        public UserBuilder() :
            base()
        {
        }

        public UserBuilder(SystemUser user) :
            base(user)
        {
            if (user.GetType() == typeof(UserBuilder))
            {
                this.BuilderID = ((UserBuilder)user).BuilderID;
            }
        }

        public object BuilderID { get; set; }
    }

    public class UserSubcontractor : SystemUser
    {
        public UserSubcontractor() :
            base()
        {
        }

        public UserSubcontractor(SystemUser user) :
            base(user)
        {
            if (user.GetType() == typeof(UserSubcontractor))
            {
                this.SubcontractorID = ((UserSubcontractor)user).SubcontractorID;
            }
        }

        public object SubcontractorID { get; set; }
    }
    public class UserLoginClass
    {
        public Boolean Successful { get; set; }
        public string ErrorMessage { get; set; }
    }
    */
}
