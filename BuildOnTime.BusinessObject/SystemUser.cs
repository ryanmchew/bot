﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{
    public class SystemUserException : ApplicationException
    {
        public SystemUserException() : base() { }
        public SystemUserException(string message) : base(message) { }
        public SystemUserException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class UserPasswordChange : SystemUserException 
    { 
        public UserPasswordChange() : base() { }
        public UserPasswordChange(string message) : base(message) { }
        public UserPasswordChange(string message, Exception innerException) : base(message, innerException) { }
    }
    public class UserInvalidPassword : SystemUserException 
    { 
        public UserInvalidPassword() : base() { }
        public UserInvalidPassword(string message) : base(message) { }
        public UserInvalidPassword(string message, Exception innerException) : base(message, innerException) { }
    }

    public enum Role
    {
        Administrator,
        Superintendent,
        Subcontractor,
        Worker
    }

    public class SystemUser: Persistable
    {
        public SystemUser()
        {
            Roles = new List<Role>();
        }

        public SystemUser(object identifier, string userName, string email, List<Role> roles)
        {
            Roles = new List<Role>();
            Roles.AddRange(roles);
            this.Identifier = identifier;
            this.Username = userName;
            this.EMail = email;
        }

        public SystemUser(object identifier, string userName, string email, List<Role> roles, DateTime lastLogin)
        {
            Roles = new List<Role>();
            Roles.AddRange(roles);
            this.Identifier = identifier;
            this.Username = userName;
            this.EMail = email;
            this.LastLogin = lastLogin;
        }

        public string EMail { get; set; }
        public DateTime? LastLogin { get; set; }
        public string Username { get; set; }
        public List<Role> Roles { get; set; }
    }

    public enum AuditEnum
    {
        Acknowledge,
        Create,
        Login,        
        View,
        Print,
        Update,
        Delete
    }

    public enum AuditTargetEnum
    {        
        Communication,
        CommunicationList,
        Employee,
        EmployeeList,
        Job,
        JobList,
        Subcontractor,        
        SubcontractorList,
        SubcontractorUser,
        SubcontractorUserList,
        Task,
        TaskList
    }

    public enum CatalystEnum
    {
        Email,
        Webpage
    }

    public class SystemUserAudit : Persistable
    {
        public object SystemUserID { get; set; }
        public AuditEnum Action { get; set; }
        public AuditTargetEnum Target { get; set; }
        public CatalystEnum Catalyst { get; set; }
        public string CatalystDetails { get; set; }
        public DateTime Occurred { get; set; }

        public override bool Equals(object obj)
        {
            bool result = obj.GetType().Equals(typeof(SystemUserAudit));
            if (result)
            {
                SystemUserAudit compareTo = (SystemUserAudit)obj;
                result = (compareTo.Action.Equals(this.Action) &&
                    compareTo.Catalyst.Equals(this.Catalyst) &&
                    compareTo.CatalystDetails.Equals(this.CatalystDetails, StringComparison.OrdinalIgnoreCase) &&
                    compareTo.Identifier.ToString().Equals(this.Identifier.ToString(), StringComparison.OrdinalIgnoreCase) &&
                    compareTo.Occurred.Equals(this.Occurred) &&
                    compareTo.SystemUserID.ToString().Equals(this.SystemUserID.ToString(), StringComparison.OrdinalIgnoreCase) &&
                    compareTo.Target.Equals(this.Target));
            }
            return result;
        }
    }

    public class UserSubcontractor : SystemUser
    {
        public UserSubcontractor() :
            base()
        {
        }

/*        
        public UserSubcontractor(SystemUser user) :
            base(user)
        {
            if (user.GetType() == typeof(UserSubcontractor))
            {
                this.SubcontractorID = ((UserSubcontractor)user).SubcontractorID;
            }
        }
*/
        public object SubcontractorID { get; set; }
    }
}
