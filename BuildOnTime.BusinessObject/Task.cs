﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{
    public enum TaskGroupEnum
    {
        Primary,
        Interior,
        Exterior
    }

    public class Task
    {
        public Task()
        {
        }

        public Task(int dayNumber, int duration, string name)
        {
            DayNumber = dayNumber;
            Duration = duration;
            Name = name;
        }

        public object Identifier { get; set; }
        public int DayNumber { get; set; }
        public int Duration { get; set; }
        public string Name { get; set; }
        public TaskGroupEnum? Group { get; set; }
    }
}
