﻿using System;
using System.Collections.Generic;
using System.Text;
using Omu.ValueInjecter;

namespace BuildOnTime.BusinessObject
{
    public enum TaskAssignmentStatusEnum
    {
        UnAssigned,
        Assigned,
        Accepted,
        Rejected
    }

    public enum TaskStatusEnum
    {
        Scheduled,
        InProcess,
        Finished,
        Reviewed
    }

    public class JobTask
    {
        public object Identifier { get; set; }
        public object BuilderID { get; set; }
        public object EmployeeID { get; set; }
        public object SubcontractorID { get; set; }
        public object ScheduleTaskID { get; set; }
        public WorkerEnum? WorkerType 
        {
            get
            {
                WorkerEnum? result = null;
                if (EmployeeID != null)
                {
                    result = WorkerEnum.Employee;
                }
                else if (SubcontractorID != null)
                {
                    result = WorkerEnum.Subcontractor;
                }
                return result;
            }
        }

        public string WorkerID
        {
            get
            {
                return Worker.WorkerID(WorkerType, (EmployeeID != null) ? EmployeeID : SubcontractorID);
            }
        }

        public object JobID { get; set; }
        public int Duration { get; set; }
        public string TaskName { get; set; }
        public DateTime ScheduledStart { get; set; }
        public DateTime ScheduledFinish { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public DateTime? WorkerAssigned { get; set; }
        public DateTime? WorkerAccepted { get; set; }
        public DateTime? WorkerFinish { get; set; }
        public DateTime? Signoff { get; set; }
        public string TimeFrame { get; set; }
        public TaskGroupEnum? Group { get; set; }
        public DateTime Updated { get; set; }
        public TaskAssignmentStatusEnum AssignmentStatus
        {
            get
            {
                DateTime today = DateTime.Now.Date;
                TaskAssignmentStatusEnum result = TaskAssignmentStatusEnum.UnAssigned;
                if (WorkerID != null)
                {
                    if (WorkerAccepted != null)
                    {
                        result = TaskAssignmentStatusEnum.Accepted;
                    }
                    else
                    {
                        result = TaskAssignmentStatusEnum.Assigned;
                    }
                }
                return result;
            } 
        }

        public TaskStatusEnum Status 
        {
            get 
            {
                DateTime today = DateTime.Now.Date;
                TaskStatusEnum result = TaskStatusEnum.Scheduled;
                if (Signoff != null)
                {
                    result = TaskStatusEnum.Reviewed;
                }
                else if ((Finish < today) || (WorkerFinish != null))
                {
                    result = TaskStatusEnum.Finished;
                }
                else if (Start < today)
                {
                    result = TaskStatusEnum.InProcess;
                }
                return result;
            } 
        }

        public void CopyTo(JobTask destination)
        {
            destination.InjectFrom(this);
/*
            destination.EmployeeID = this.EmployeeID;
            destination.Finish = this.Finish;
            destination.Group = this.Group;
            destination.Identifier = this.Identifier;
            destination.JobID = this.JobID;
            destination.ScheduledFinish = this.ScheduledFinish;
            destination.ScheduledStart = this.ScheduledStart;
            destination.Signoff = this.Signoff;
            destination.Start = this.Start;
            destination.Duration = this.Duration;
            destination.SubcontractorID = this.SubcontractorID;
            destination.TaskName = this.TaskName;
            destination.TimeFrame = this.TimeFrame;
            destination.Updated = this.Updated;
            destination.WorkerAccepted = this.WorkerAccepted;
            destination.WorkerAssigned = this.WorkerAssigned;
            destination.WorkerFinish = this.WorkerFinish;
 */ 
        }
    }

    public class JobTaskDelay
    {
        public object JobTaskID { get; set; }
        public object JobTaskDelayerID { get; set; }
        public object JobDelayID { get; set; }
        public TaskGroupEnum? Group { get; set; }
        public DateTime OriginalStart { get; set; }
        public DateTime OriginalFinish { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public DateTime Entered { get; set; }

        public void CopyTo(JobTaskDelay destination)
        {
            destination.InjectFrom(this);
            /*
            destination.Finish = this.Finish;
            destination.JobDelayID = this.JobDelayID;
            destination.JobTaskDelayerID = this.JobTaskDelayerID;
            destination.JobTaskID = this.JobTaskID;
            destination.OriginalFinish = this.OriginalFinish;
            destination.OriginalStart = this.OriginalStart;
            destination.Group = this.Group;
            destination.Start = this.Start;
            destination.Entered = this.Entered;
             */ 
        }
    }

    public class JobTaskDependency
    {
        public JobTaskDependency()
        {
        }

        public JobTaskDependency(object jobID, object parentID, object dependentID)
        {
            JobID = jobID;
            ParentID = parentID;
            DependentID = dependentID;
        }

        public object JobID { get; set; }
        public object ParentID { get; set; }
        public object DependentID { get; set; }
    }
}
