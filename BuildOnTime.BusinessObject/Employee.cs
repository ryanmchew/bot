﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{
    public class Employee : Person
    {
        public Employee()
        {
            Roles = new List<Role>();
        }

        public Employee(Employee employee) : base(employee)
        {
            Roles = new List<Role>();
            this.Identifier = employee.Identifier;
            this.Builder = employee.Builder;
            this.Supervisor = employee.Supervisor;
            this.SystemUser = employee.SystemUser;
            this.Roles.AddRange(employee.Roles);
        }

        public Builder Builder { get; set; }
        public Employee Supervisor { get; set; }
        public SystemUser SystemUser { get; set; }
        public List<Role> Roles { get; set; }
        public string Password { get; set; }
    }
}
