﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{
    public class Schedule
    {
        public override string ToString()
        {
            return Name;
        }

        public object Identifier { get; set; }
        public object BuilderID { get; set; }
        public string Name { get; set; }
    }

    public class ScheduleTask: Task
    {
        public object ScheduleIdentifier { get; set; }
        public ScheduleTask()
            : base() { }

        public ScheduleTask(int dayNumber, int duration, string name)
            : base (dayNumber, duration, name) { }

        public ScheduleTask(object identifier, object scheduleIdentifier, string name, TaskGroupEnum? group, object defaultEmployeeID, object defaultSubcontractorID, int dayNumber, int duration, DateTime updated)
        {
            this.DayNumber = dayNumber;
            this.DefaultEmployeeID = defaultEmployeeID;
            this.DefaultSubcontractorID = defaultSubcontractorID;
            this.Duration = duration;
            this.Group = group;
            this.Identifier = identifier;
            this.Name = name;
            this.ScheduleIdentifier = scheduleIdentifier;
            this.Updated = updated;
        }

        public object DefaultEmployeeID { get; set; }
        public object DefaultSubcontractorID { get; set; }
        public DateTime Updated { get; set; }

        public void CopyTo(ScheduleTask destination)
        {
            destination.DayNumber = this.DayNumber;
            destination.DefaultEmployeeID = this.DefaultEmployeeID;
            destination.DefaultSubcontractorID = this.DefaultSubcontractorID;
            destination.Duration = this.Duration;
            destination.Group = this.Group;
            destination.Identifier = this.Identifier;
            destination.Name = this.Name;
            destination.ScheduleIdentifier = this.ScheduleIdentifier;
            destination.Updated = this.Updated;
        }
    }

    public class ScheduleTaskDependency
    {
        public object ScheduleID { get; set; }
        public object ParentID { get; set; }
        public object DependentID { get; set; }
    }
}
