﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{
    public class Subcontractor
    {
        public object Identifier { get; set; }
        public object BuilderID { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string OfficePhone { get; set; }
        public string OfficeFax { get; set; }
        public string CellPhone { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public class SubcontractorScheduling
    {
        public object SubcontractorID { get; set; }
        public string EMail { get; set; }
        public string PrimaryPhone { get; set; }
        public string BackupPhone1 { get; set; }
        public string BackupPhone2 { get; set; }
    }

}
