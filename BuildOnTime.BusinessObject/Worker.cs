﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{
    public enum WorkerEnum
    {
        Subcontractor,
        Employee
    }

    public class Worker
    {
        public static string EmployeeID(string workerID)
        {
            return (workerID == null) ? null : workerID.Contains(WorkerEnum.Employee.ToString()) ? workerID.Replace(WorkerEnum.Employee.ToString(), "") : null;
        }

        public static string SubcontractorID(string workerID)
        {
            return (workerID == null) ? null : workerID.Contains(WorkerEnum.Subcontractor.ToString()) ? workerID.Replace(WorkerEnum.Subcontractor.ToString(), "") : null;
        }

        public static string WorkerID(WorkerEnum? workerType, object workerID)
        {
            return (workerType == null) ? null : workerType.ToString() + workerID;
        }

        public Worker(string name, WorkerEnum? workerType, string identifier)
        {
            this.Name = name;
            this.Identifier = WorkerID(workerType, identifier);
        }

        public string Name { get; set; }
        public string Identifier { get; set; }
    }
}
