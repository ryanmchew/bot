﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{    
    public class Person: Persistable
    {
        public Person() { }
        public Person(Person copyFrom)
        {
            this.CellPhone = copyFrom.CellPhone;
            this.Email = copyFrom.Email;
            this.Fax = copyFrom.Fax;
            this.FirstName = copyFrom.FirstName;
            this.HomePhone = copyFrom.HomePhone;
            this.LastName = copyFrom.LastName;
            this.WorkPhone = copyFrom.WorkPhone;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name
        {
            get { return FirstName + " " + LastName; }
        }
        public string Email { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string Fax { get; set; }
    }
}
