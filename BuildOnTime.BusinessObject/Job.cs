﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{
    public class Subdivision
    {
        public object Identifier { get; set; }
        public object BuilderID { get; set; }
        public string Name { get; set; }
        public string County { get; set; }
        public string State { get; set; }
    }

    public class Site
    {
        public int? BuilderID { get; set; }
        public int? Identifier { get; set; }
        public string LotIdentifier { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
    }

    public class Job
    {
        public int? BuilderID { get; set; }
        public int? Identifier { get; set; }
        public int? SuperID { get; set; }
        public int? SiteID { get; set; }
        public string Permit { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        // These fields are calcualted from the Tasks in the database hence are in lower case
        // You cannot set them.
        public DateTime? start { get; set; }
        public DateTime? finish { get; set; }
        public override string ToString()
        {
            return Name;
        }

        public void CopyTo(Job job)
        {
            job.BuilderID = this.BuilderID;
            job.Description = this.Description;
            job.Identifier = this.Identifier;
            job.Name = this.Name;
            job.SiteID = this.SiteID;
            job.SuperID = this.SuperID;
            job.start = this.start;
            job.finish = this.finish;
        }
    }

    public class JobDelay
    {
        public object Identifier { get; set; }
        public object JobID { get; set; }
        public string DelayedBy { get; set; }
        public object DelayedByEmployeeID { get; set; }
        public object DelayedBySubcontractorID { get; set; }
        public int DelayLength { get; set; }
        public string Reason { get; set; }
        public DateTime Entered { get; set; }

        public void CopyTo(JobDelay destination)
        {
            destination.DelayedBy = this.DelayedBy;
            destination.DelayedByEmployeeID = this.DelayedByEmployeeID;
            destination.DelayedBySubcontractorID = this.DelayedBySubcontractorID;
            destination.DelayLength = this.DelayLength;
            destination.Entered = this.Entered;
            destination.Identifier = this.Identifier;
            destination.JobID = this.JobID;
            destination.Reason = this.Reason;
        }
    }
}
