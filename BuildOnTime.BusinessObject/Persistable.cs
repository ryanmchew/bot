﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildOnTime.BusinessObject
{
    public class Persistable
    {
        public object Identifier { get; set; }
        public DateTime Updated;
    }
}
