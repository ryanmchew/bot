﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using BuildOnTime.BusinessObject;

namespace Website.Models
{
    public class TaskModel
    {
        public string Id { get; set; }
        public TaskAssignmentStatusEnum Progress { get; set; }
        public TaskStatusEnum OverallProgress { get; set; }
        public DateTime? WorkerFinish { get; set; }
        public string Status { get; set; }
        public string Builder { get; set; }
        public string Job { get; set; }
        public string Super { get; set; }
        [Display(Description="Task")]
        public string Name { get; set; }
        [Display(Description = "Updated")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StatusDate { get; set; }
        [Display(Description = "Start")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDate { get; set; }
        public string Duration { get; set; }
    }

    public class TaskAssignModel
    {
        public IEnumerable<BuildOnTime.Presentation.TaskAction> TaskActions { get; set; }
        public IEnumerable<SelectListItem> WorkerList { get; set; }
    }

    public class WorkerModel
    {
        public string WorkerID { get; set; }
        public string WorkerName { get; set; }
    }

    public class TaskWorkerModel
    {
        public string TaskID { get; set; }
        public string WorkerID { get; set; }
    }

    public class TaskCommunicationModel: TaskWorkerModel
    {
        public CommunicationReasonEnum Reason { get; set; }
    }
}