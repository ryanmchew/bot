﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class JobModel
    {
        public int? BuilderID { get; set; }
        public int? Identifier { get; set; }
        [Display(Name = "Super")]
        public int? SuperID { get; set; }
        [Display(Name = "Schedule")]
        public int? ScheduleID { get; set; }
        public string Permit { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string SuperName { get; set; }
        public string LotIdentifier { get; set; }
        [Display(Name = "Location")]
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        [Display(Name = "Start")]
        public DateTime? start { get; set; }
        [Display(Name = "Finish")]
        public DateTime? finish { get; set; }
    }

    //public class JobModel
    //{
    //    public int Id { get; set; }

    //    [DisplayName("Lot")]
    //    public string LotNumber { get; set; }

    //    [DisplayName("Super")]
    //    public string Superintendent { get; set; }


    //    [DisplayName("Floor Plan")]
    //    public string FloorPlan { get; set; }

    //    public string Permit { get; set; }

    //    public string Description { get; set; }

    //    public string Name { get; set; }

    //    [DisplayName("Zip Code")]
    //    [DataType(DataType.PostalCode)]
    //    public string PostalCode { get; set; }

    //    public string Address { get; set; }

    //    public string City { get; set; }

    //    public string State { get; set; }

    //    [DataType(DataType.Date)]
    //    public DateTime Start { get; set; }

    //    [DataType(DataType.Date)]
    //    public DateTime Finish { get; set; }

    //    public int? SuperId { get; set; }
    //}

    public class JobListItemModel
     {
        public int Id { get; set; }

        [DisplayName("Lot")]
        public string LotNumber { get; set; }

        [DisplayName("Super")]
        public string Superintendent { get; set; }

        public string Description { get; set; }

        public string Permit { get; set; }

        public string Start { get; set; }

        public string Finish { get; set; }

    }
}