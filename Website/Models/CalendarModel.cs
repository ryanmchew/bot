﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuildOnTime.BusinessObject;

namespace Website.Models
{
    public class CalendarDependentModel
    {
        public string id { get; set; }
        public int? startBuffer { get; set; }
        public int? endBuffer { get; set; }
    }

    public class CalendarModel
    {
        public CalendarModel()
        {
            dependents = new List<CalendarDependentModel>();
        }

        public string id { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public int duration { get; set; } 
        public string url { get; set; }
        public bool allDay { get; set; }
        public List<CalendarDependentModel> dependents { get; set; }
        public int delta { get; set; }
        public string workerID { get; set; }
        public string taskName { get; set; }
        public bool weekends { get; set; }
    }

    public class JobWorkerModel
    {
        public List<Job> Jobs { get; set; }
        public List<Worker> Workers { get; set; }
        public string JobID { get; set; }
        public string JobName { get; set; }
    }

    public class SubLoadModel
    {
        public string SubcontractorName { get; set; }
        public string SubcontractorID { get; set; }
        public int TaskCount { get; set; }
        public List<CalendarModel> Tasks { get; set; }
    }

    public class BuilderSubContractorWorkloadModel
    {
        public List<SubLoadModel> SubcontractorWorkloads { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }


}