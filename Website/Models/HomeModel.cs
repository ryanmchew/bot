﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class HomeModel
    {
        public HomeModel()
        {
            Jobs = new List<JobModel>();
        }

        public List<JobModel> Jobs { get; set; }
    }

}