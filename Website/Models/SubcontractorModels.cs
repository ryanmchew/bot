﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website.Models
{
    //public class SubcontractorModel
    //{
    //    public object Identifier { get; set; }
    //    public object BuilderID { get; set; }
    //    public string Name { get; set; }
    //    public string AddressLine1 { get; set; }
    //    public string AddressLine2 { get; set; }
    //    public string City { get; set; }
    //    public string State { get; set; }
    //    public string PostalCode { get; set; }
    //    public string OfficePhone { get; set; }
    //    public string OfficeFax { get; set; }
    //}

    public class SubcontractorModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [DisplayName("Address")]
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        [DisplayName("Zip")]
        [DataType(DataType.PostalCode)]
        public string PostalCode { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Company")]
        public string CompanyName { get; set; }

        [Display(Name = "Office phone")]
        [DataType(DataType.PhoneNumber)]
        public string OfficePhone { get; set; }

        [Display(Name = "Cell phone")]
        [DataType(DataType.PhoneNumber)]
        public string CellPhone { get; set; }

        [Display(Name = "Office Fax")]
        [DataType(DataType.PhoneNumber)]
        public string OfficeFax { get; set; }
        
    }

    public class SubcontractorListItemModel
    {
        public int Id { get; set; }

        [Display(Name = "Company")]
        public string CompanyName { get; set; }

        public string Name { get; set; }

        [Display(Name = "Office phone")]
        public string OfficePhone { get; set; }

        [Display(Name = "Cell phone")]
        public string CellPhone { get; set; }

        [Display(Name = "Office Fax")]
        public string OfficeFax { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}