﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
//using System.Runtime.Caching;

namespace Website.Models
{
    //public class BlackDiamondUserStore : IUserStore<AuthUserModel, int>,
    //    IUserPasswordStore<AuthUserModel, int>,
    //    IUserSecurityStampStore<AuthUserModel, int>,
    //    IUserClaimStore<AuthUserModel, int>,
    //    IUserEmailStore<AuthUserModel, int>,
    //    IUserPhoneNumberStore<AuthUserModel, int>,
    //    IQueryableUserStore<AuthUserModel, int>,
    //    IUserTwoFactorStore<AuthUserModel, int>,
    //    IUserLockoutStore<AuthUserModel, int>
    //{
    //    private static ObjectCache authCache = MemoryCache.Default;
    //    CacheItemPolicy policy;

    //    private bool _disposed = false;
    //    private const string CACHE_STRING_USERID = "authUserModel";

    //    public BlackDiamondUserStore()
    //    {
    //        policy = new CacheItemPolicy { SlidingExpiration = new TimeSpan(0, 0, 30) };
    //    }

    //    ~BlackDiamondUserStore()
    //    {
    //        Dispose(false);
    //    }

    //    private void UpdateLocalObject(AuthUserModel localAuth, AuthUserModel persistedAuth)
    //    {
    //        localAuth.Id = persistedAuth.Id;
    //    }

    //    private AuthUserModel FindUserModelCache(int? userId, string email, string userName)
    //    {
    //        AuthUserModel aum = null;
    //        if (userId.HasValue)
    //        {
    //            string cacheKey = CACHE_STRING_USERID + userId.ToString();
    //            aum = authCache[cacheKey] as AuthUserModel;
    //            if (aum == null)
    //            {
    //                AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //                aum = alc.AuthUserFindById(userId.Value);
    //                authCache.Set(cacheKey, aum, policy);
    //                authCache.Set(aum.UserName, cacheKey, policy);
    //                authCache.Set(aum.Email, cacheKey, policy);
    //            }
    //        }
    //        else if (email != null)
    //        {
    //            string cacheKey = authCache[email] as string;
    //            if (!cacheKey.IsEmpty()) { aum = authCache[cacheKey] as AuthUserModel; }
    //            if (aum == null)
    //            {
    //                AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //                aum = alc.AuthUserFindByEmail(email);
    //                cacheKey = CACHE_STRING_USERID + aum.Id;
    //                authCache.Set(cacheKey, aum, policy);
    //                authCache.Set(aum.UserName, cacheKey, policy);
    //                authCache.Set(aum.Email, cacheKey, policy);
    //            }
    //        }
    //        else if (userName != null)
    //        {
    //            string cacheKey = authCache[userName] as string;
    //            if (!cacheKey.IsEmpty()) { aum = authCache[cacheKey] as AuthUserModel; }
    //            if (aum == null)
    //            {
    //                AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //                aum = alc.AuthUserFindByName(userName);
    //                cacheKey = CACHE_STRING_USERID + aum.Id;
    //                authCache.Set(cacheKey, aum, policy);
    //                authCache.Set(aum.UserName, cacheKey, policy);
    //                authCache.Set(aum.Email, cacheKey, policy);
    //            }
    //        }
    //        return aum;
    //    }

    //    public Task CreateAsync(AuthUserModel user)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        // Because login immediately is called after creation by the login controller.  We need to make sure the user id gets set from the service call.
    //        // This should set the AuthUserModel so it will have the correct Id for subsequent calls.
    //        return alc.AuthUserCreateAsync(user).ContinueWith(aum => UpdateLocalObject(user, aum.Result));
    //    }

    //    public Task DeleteAsync(AuthUserModel user)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            alc.AuthUserDeleteAsync(user);
    //        }
    //        return null;
    //    }

    //    public Task<AuthUserModel> FindByIdAsync(int userId)
    //    {
    //        //AuthUserModel aum = authCache["authUserModel" + userId.ToString()] as AuthUserModel;
    //        //if (aum == null)
    //        //{
    //        //    AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        //    if (userId > 0)
    //        //    {
    //        //        aum = alc.AuthUserFindById(userId);
    //        //        authCache.Set("authUserModel" + userId.ToString(), aum, policy);
    //        //        authCache.Set(aum.UserName, aum, policy);
    //        //        authCache.Set(aum.Email, aum, policy);
    //        //    }
    //        //    else
    //        //    {
    //        //        aum = null;
    //        //    }
    //        //}
    //        return Task.Run(() => FindUserModelCache(userId, null, null));
    //    }

    //    public Task<AuthUserModel> FindByNameAsync(string userName)
    //    {
    //        //AuthUserModel aum = authCache[userName] as AuthUserModel;
    //        //if (aum == null)
    //        //{
    //        //    AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        //    aum = alc.AuthUserFindByName(userName);
    //        //    authCache.Set("authUserModel" + aum.Id.ToString(), aum, policy);
    //        //    authCache.Set(aum.UserName, aum, policy);
    //        //}
    //        return Task.Run(() => FindUserModelCache(null, null, userName));
    //    }

    //    public Task UpdateAsync(AuthUserModel user)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        return alc.AuthUserUpdateAsync(user);
    //    }

    //    protected virtual void Dispose(bool disposing)
    //    {
    //        if (!_disposed)
    //        {
    //            if (disposing)
    //            {

    //            }

    //            // Now disposed of any unmanaged objects
    //            // ...

    //            _disposed = true;
    //        }
    //    }

    //    public void Dispose()
    //    {
    //        Dispose(true);
    //        GC.SuppressFinalize(this);
    //    }

    //    public Task<string> GetPasswordHashAsync(AuthUserModel user)
    //    {
    //        if (user.PasswordHash.IsEmpty() && user.Id > 0)
    //        {
    //            AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //            return alc.AuthUserGetPasswordHashAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.PasswordHash);
    //        }
    //    }

    //    public Task<bool> HasPasswordAsync(AuthUserModel user)
    //    {
    //        if (user.PasswordHash.IsEmpty() && user.Id > 0)
    //        {
    //            AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //            return alc.AuthUserHasPasswordAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => !user.PasswordHash.IsEmpty());
    //        }
    //    }

    //    public Task SetPasswordHashAsync(AuthUserModel user, string passwordHash)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AuthUserGetPasswordHashAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.PasswordHash = passwordHash);
    //        }
    //    }

    //    public Task<string> GetSecurityStampAsync(AuthUserModel user)
    //    {
    //        if (user.SecurityStamp.IsEmpty() && user.Id > 0)
    //        {
    //            AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //            return alc.AuthUserGetSecurityStampAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.SecurityStamp);
    //        }
    //    }

    //    public Task SetSecurityStampAsync(AuthUserModel user, string stamp)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AuthUserSetSecurityStampAsync(user, stamp);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.SecurityStamp = stamp);
    //        }
    //    }

    //    public Task<AuthUserModel> FindByEmailAsync(string email)
    //    {

    //        //AuthUserModel aum = authCache[email] as AuthUserModel;
    //        //if (aum == null)
    //        //{
    //        //    AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        //    return alc.AuthUserFindByEmailAsync(email);
    //        //}
    //        //else
    //        //{
    //        //}
    //        return Task.Run(() => FindUserModelCache(null, email, null));
    //    }

    //    public Task<string> GetEmailAsync(AuthUserModel user)
    //    {
    //        if (user.Email.IsEmpty())
    //        {
    //            AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //            return alc.AuthUserGetEmailAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.Email);
    //        }
    //    }

    //    public Task<bool> GetEmailConfirmedAsync(AuthUserModel user)
    //    {
    //        if (user.Email.IsEmpty())
    //        {
    //            AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //            return alc.AuthUserGetEmailConfirmedAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.EmailConfirmed);
    //        }
    //    }

    //    public Task SetEmailAsync(AuthUserModel user, string email)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AuthUserSetEmailAsync(user, email);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.Email = email);
    //        }
    //    }

    //    public Task SetEmailConfirmedAsync(AuthUserModel user, bool confirmed)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AuthUserSetEmailConfirmedAsync(user, confirmed);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.EmailConfirmed = confirmed);
    //        }
    //    }

    //    public Task<string> GetPhoneNumberAsync(AuthUserModel user)
    //    {
    //        if (user.PhoneNumber.IsEmpty())
    //        {
    //            AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //            return alc.AuthUserGetPhoneNumberAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.PhoneNumber);
    //        }
    //    }

    //    public Task<bool> GetPhoneNumberConfirmedAsync(AuthUserModel user)
    //    {
    //        if (user.PhoneNumber.IsEmpty())
    //        {
    //            AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //            return alc.AuthUserGetPhoneNumberConfirmedAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.PhoneNumberConfirmed);
    //        }
    //    }

    //    public Task SetPhoneNumberAsync(AuthUserModel user, string phoneNumber)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AuthUserSetPhoneNumberAsync(user, phoneNumber);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.PhoneNumber = phoneNumber);
    //        }
    //    }

    //    public Task SetPhoneNumberConfirmedAsync(AuthUserModel user, bool confirmed)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AuthUserSetPhoneNumberConfirmedAsync(user, confirmed);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.PhoneNumberConfirmed = confirmed);
    //        }
    //    }

    //    public IQueryable<AuthUserModel> Users
    //    {
    //        get { return null; }
    //    }

    //    public Task<bool> GetTwoFactorEnabledAsync(AuthUserModel user)
    //    {
    //        if (user.Email.IsEmpty())
    //        {
    //            AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //            return alc.AuthUserGetTwoFactorEnabledAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.TwoFactorEnabled);
    //        }
    //    }

    //    public Task SetTwoFactorEnabledAsync(AuthUserModel user, bool enabled)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AuthUserSetTwoFactorEnabledAsync(user, enabled);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.TwoFactorEnabled = enabled);
    //        }
    //    }

    //    public Task<int> GetAccessFailedCountAsync(AuthUserModel user)
    //    {
    //        if (user.Email.IsEmpty())
    //        {
    //            AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //            return alc.AuthUserGetAccessFailedCountAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.AccessFailedCount);
    //        }
    //    }

    //    public Task<bool> GetLockoutEnabledAsync(AuthUserModel user)
    //    {
    //        if (user.Email.IsEmpty())
    //        {
    //            AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //            return alc.AuthUserGetLockoutEnabledAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.LockoutEnabled);
    //        }
    //    }

    //    public Task<DateTimeOffset> GetLockoutEndDateAsync(AuthUserModel user)
    //    {
    //        if (user.Email.IsEmpty())
    //        {
    //            AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //            return Task.Run(() => new DateTimeOffset(alc.AuthUserGetLockoutEndDate(user)));
    //        }
    //        else
    //        {
    //            return Task.Run(() => new DateTimeOffset(user.LockoutEndDateUTC));
    //        }
    //    }

    //    public Task<int> IncrementAccessFailedCountAsync(AuthUserModel user)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AuthUserIncrementAccessFailedCountAsync(user);
    //        }
    //        return Task.Run(() => 0);
    //    }

    //    public Task ResetAccessFailedCountAsync(AuthUserModel user)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AuthUserResetAccessFailedCountAsync(user);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.AccessFailedCount = 0);
    //        }
    //    }

    //    public Task SetLockoutEnabledAsync(AuthUserModel user, bool enabled)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AuthUserSetLockoutEnabledAsync(user, enabled);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.LockoutEnabled = enabled);
    //        }
    //    }

    //    public Task SetLockoutEndDateAsync(AuthUserModel user, DateTimeOffset lockoutEnd)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AuthUserSetLockoutEndDateAsync(user, lockoutEnd.LocalDateTime);
    //        }
    //        else
    //        {
    //            return Task.Run(() => user.LockoutEndDateUTC = lockoutEnd.LocalDateTime);
    //        }
    //    }

    //    public Task AddClaimAsync(AuthUserModel user, System.Security.Claims.Claim claim)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.AddClaimAsync(user, claim);
    //        }
    //        else
    //        {
    //            // Throw a delay in for now.
    //            return Task.Delay(2);
    //        }
    //    }

    //    public Task<IList<System.Security.Claims.Claim>> GetClaimsAsync(AuthUserModel user)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.GetClaimsAsync(user).ContinueWith<IList<System.Security.Claims.Claim>>(t => t.Result, TaskContinuationOptions.ExecuteSynchronously);
    //        }
    //        else
    //        {
    //            // Throw a delay in for now.
    //            return Task.Run(() => new List<System.Security.Claims.Claim>() as IList<System.Security.Claims.Claim>);
    //        }
    //    }

    //    public Task RemoveClaimAsync(AuthUserModel user, System.Security.Claims.Claim claim)
    //    {
    //        AuthenticationLogic.AuthenticationLogicClient alc = new AuthenticationLogic.AuthenticationLogicClient();
    //        if (user.Id > 0)
    //        {
    //            return alc.RemoveClaimAsync(user, claim);
    //        }
    //        else
    //        {
    //            // Throw a delay in for now.
    //            return Task.Delay(2);
    //        }
    //    }
    //}
}