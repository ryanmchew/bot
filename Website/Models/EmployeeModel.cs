﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class EmployeeModel
    {
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string Name { get; set; }

        public int Id { get; set; }
        
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Cell phone")]
        [DataType(DataType.PhoneNumber)]
        public string CellPhone { get; set; }

        public string Password { get; set; }
    }

public class EmployeeListItemModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [Display(Name = "Cell phone")]
        [DataType(DataType.PhoneNumber)]
        public string CellPhone { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

    }
}