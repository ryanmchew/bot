var con1 = document.getElementById('container1');
var con2 = document.getElementById('container2');

con1.style.display = "block";
con2.style.display = "none";

function grabValue(input) {
    return document.getElementById(input).value;
};

function enter(input, output) {
    document.getElementById(output).innerHTML = grabValue(input);
    window.scroll(0, 0);

    con1.style.display = "none";
    con2.style.display = "block";
};

function submit() {
    con1.style.display = "none";
    con2.style.display = "none";

    document.write('Sub Created!');
};

function changes() {
    con1.style.display = "block";
    con2.style.display = "none";
}