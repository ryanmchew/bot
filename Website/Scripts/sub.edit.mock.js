﻿var con1 = document.getElementById('container1');
var con2 = document.getElementById('container2');

con1.style.display = "block";
con2.style.display = "none";

function grabInput(input) {
    return document.getElementById(input).value;
};

function link(input, output) {
    con1.style.display = "none";
    con2.style.display = "block";

    document.getElementById(output).innerHTML = grabInput(input);

    window.scroll(0, 0);
};

function submit() {
    con1.style.display = "none";
    con2.style.display = "none";

    document.write('Sub was Edited!');
    //take back to sub list page
};

function changes() {
    con1.style.display = "block";
    con2.style.display = "none";
};