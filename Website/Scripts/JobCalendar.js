﻿var startDrag = null;
var endDrag = null;
var eventStartDate = null;

var natDays = [
          [1, 1],
          [7, 4],
          [12, 25]
];

var calendarEvents;

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function holiday(date) {
    for (i = 0; i < natDays.length; i++) {
        if (date.getMonth() == natDays[i][0] - 1 && date.getDate() == natDays[i][1]) {
            return true;
        }
    }
    return false;
}


function weekendOrHoliday(date) {
    var weekend = ((date.getDay() == 0) || (date.getDay() == 6));
    if (!weekend) {
        return holiday(date);
    } else {
        return weekend;
    }
}


function AddBusinessDays(curdate, weekDaysToAdd) {
    var realDaysToAdd = 0;
    var sign = 1;
    if (weekDaysToAdd < 0) { sign = -1 }
    while (sign * weekDaysToAdd > 0) {
        curdate.setDate(curdate.getDate() + sign);
        realDaysToAdd = realDaysToAdd + (sign);
        //check if current day is business day
        if (!weekendOrHoliday(curdate)) {
            weekDaysToAdd = weekDaysToAdd - (sign);
        }
    }
    return realDaysToAdd;
} 

function BusinessDayDifference(curDate, newDate) {
    var realDaysToAdd = 0;
    var startDate = new Date(curDate);
    var endDate = new Date(newDate);
    var sign = 1;
    if (startDate > endDate) {
        sign = -1;
        startDate = new Date(newDate);
        endDate = new Date(curDate);
    }
    while (startDate < endDate) {
        startDate.setDate(startDate.getDate() + 1);
        //check if current day is business day
        if (!weekendOrHoliday(startDate)) {
            realDaysToAdd = realDaysToAdd + 1;
        }
    }
    return realDaysToAdd * sign;
}

function RealDayDifference(curDate, newDate) {
    var realDaysToAdd = 0;
    var startDate = new Date(curDate);
    var endDate = new Date(newDate);
    var sign = 1;
    if (startDate > endDate) {
        sign = -1;
        startDate = new Date(newDate);
        endDate = new Date(curDate);
    }
    while (startDate < endDate) {
        startDate.setDate(startDate.getDate() + 1);
        //check if current day is business day
        realDaysToAdd = realDaysToAdd + 1;
    }
    return realDaysToAdd * sign;
}






function momHoliday(date) {
    for (i = 0; i < natDays.length; i++) {
        if (date.month() == natDays[i][0] - 1 && date.day() == natDays[i][1]) {
            return true;
        }
    }
    return false;
}

function momWeekendOrHoliday(date) {
    var weekend = ((date.day() == 0) || (date.day() == 6));
    if (!weekend) {
        return momHoliday(date);
    } else {
        return weekend;
    }
}


function momAddBusinessDays(curdate, weekDaysToAdd) {
    return curdate.businessAdd(weekDaysToAdd);
}

function momBusinessDayDifference(curDate, newDate) {
    return curdate.businessDiff(newDate);
}

function momRealDayDifference(curDate, newDate) {
    var realDaysToAdd = 0;
    var startDate = new Date(curDate);
    var endDate = new Date(newDate);
    var sign = 1;
    if (startDate > endDate) {
        sign = -1;
        startDate = new Date(newDate);
        endDate = new Date(curDate);
    }
    while (startDate < endDate) {
        startDate.setDate(startDate.getDate() + 1);
        //check if current day is business day
        realDaysToAdd = realDaysToAdd + 1;
    }
    return realDaysToAdd * sign;
}









function taskDuration(duration) {
    return (duration > 0) ? duration - 1 : 0;
}

function moveChildrenFloat(calEvent, delta) {
    $(calEvent.dependents).each(function (index, value) {
        var eventChild = $('#calendar').fullCalendar('clientEvents', value.id)[0];
        //var startDays = delta;
        var startDays = 0;
        var newDate = calEvent.end;
        if (value.startBuffer != null) {
            newDate = calEvent.start;
            startDays = momAddBusinessDays(newDate, value.startBuffer);
        }
        else {
            startDays = momAddBusinessDays(newDate, value.endBuffer);
        }
        if (eventChild.start < newDate) {
            startDays = RealDayDifference(eventChild.start, newDate);
            eventChild.start = newDate;
        }
        else {
            startDays = 0;
        }
        if (startDays != 0) {
            eventChild.end = eventChild.start;
            var endDays = momAddBusinessDays(eventChild.end, taskDuration(eventChild.duration));
            eventChild.delta = eventChild.delta + startDays;
            checkMove(eventChild);
            moveChildrenFloat(eventChild, startDays);
        }
    });
}

function enableSelectWorker() {
    $('#selectWorker').removeAttr('disabled');
    //$('#labelEnableWorker').hide("fast");
}

function selectEvent(calEvent, jsEvent, view) {
    // change the border color just for fun
    $($('#calendar').fullCalendar('clientEvents')).each(function (index, value) {
        if (value.className != 'fc-moved') {
            value.backgroundColor = '';
            value.textColor = '';
        }
        else {
            value.backgroundColor = 'Green';
            value.textColor = 'Black';
        }
    });
    $('#taskSelect').val(calEvent.id);
    $('#buttonUpdateWorker').removeAttr('disabled');
    $('#selectWorker').val(calEvent.workerID);
    $('#labelNewWorkerTask').text(calEvent.taskName);
    if ((calEvent.workerID != null) && (calEvent.workerID.length > 0)) {
        $('#buttonUpdateWorker').val('Change worker');
        $('#buttonUpdateWorker').attr('title', getWorkerName(calEvent.workerID));
        //$('#selectWorker').attr('disabled', 'disabled');
        //$('#labelEnableWorker').show("fast");
    }
    else {
        $('#buttonUpdateWorker').val('Select worker');
        $('#buttonUpdateWorker').attr('title', 'Unassigned');
        //enableSelectWorker();
    }
    highlightChildren(calEvent);
    $('#calendar').fullCalendar('updateEvent', calEvent, calEvent);
}

function selectTask(taskID) {
    if (taskID != '') {
        for (var i = 0; i < calendarEvents.length; i++) {
            if (calendarEvents[i].id == taskID) {
                selectEvent(calendarEvents[i], calendarEvents[i], null);
            }
        }
    } else {
        $('#selectWorker').val('');
        $('#buttonUpdateWorker').attr('disabled', 'disabled');
    }
}

function selectTaskFromDropDown(taskID) {
    if (taskID != '') {
        for (var i = 0; i < calendarEvents.length; i++) {
            if (calendarEvents[i].id == taskID) {
                selectEvent(calendarEvents[i], calendarEvents[i], null);
                var startDate = new Date(calendarEvents[i].start);
                $('#calendar').fullCalendar('gotoDate', startDate);
            }
        }
    } else {
        $('#selectWorker').val('');
        $('#buttonUpdateWorker').attr('disabled', 'disabled');
    }
}

function moveChildren(calEvent, delta) {
    $(calEvent.dependents).each(function (index, value) {
        var eventChild = $('#calendar').fullCalendar('clientEvents', value.id)[0];
        var startDays = momAddBusinessDays(eventChild.start, delta);
        eventChild.end = new Date(eventChild.start.getFullYear(), eventChild.start.getMonth(), eventChild.start.getDate());
        var duration = calEvent.duration - 1;
        var endDays = momAddBusinessDays(eventChild.end, taskDuration(eventChild.duration));
        eventChild.delta = eventChild.delta + startDays;
        checkMove(eventChild);
        moveChildren(eventChild, delta);
    });
}

function moveAll(calEvent, startDate, delta) {
    $(calendarEvents).each(function (index, value) {
        var eventChild = $('#calendar').fullCalendar('clientEvents', value.id)[0];
        if ((eventChild.id != calEvent.id) && (eventChild.start >= startDate)) {
            var startDays = momAddBusinessDays(eventChild.start, delta);
            eventChild.end = new Date(eventChild.start.getFullYear(), eventChild.start.getMonth(), eventChild.start.getDate());
            var endDays = momAddBusinessDays(eventChild.end, taskDuration(eventChild.duration));
            eventChild.delta = eventChild.delta + startDays;
            checkMove(eventChild);
        }
    });
}

function checkMove(calEvent) {
    if (calEvent.delta != 0) {
        calEvent.className = 'fc-moved';
        calEvent.backgroundColor = 'Green';
        calEvent.textColor = 'Black';
    }
    else {
        calEvent.className = '';
        calEvent.backgroundColor = '';
        calEvent.textColor = '';
    }
}

function findDay(calEvent, jsEvent, view) {
    var mousex = jsEvent.pageX;
    var mousey = jsEvent.pageY;
    $('td').each(function (index) {
        var offset = $(this).offset();
        if ((offset.left + $(this).outerWidth()) > mousex && offset.left < mousex && (offset.top + $(this).outerHeight()) > mousey && offset.top < mousey) {

            if ($(this).hasClass('fc-other-month')) {
                //Its a day on another month
                //a high minus number means previous month
                //a low minus number means next month
                day = '-' + $(this).find('.fc-day-number').html();
                $(this).css('color', 'red');
            }
            else {
                //This is a day on the current month
                day = $(this).find('.fc-day-number').html();
                $(this).css('color', 'yellow');
            }

            alert(day);
        }
    });
}

function getDateFromCell(td, calInstance) {
    var cellPos = {
        row: td.parents('tbody').children().index(td.parent()),
        col: td.parent().children().index(td)
    };
    return calInstance.fullCalendar('getView').cellDate(cellPos);
}

function eventDragStart(event, jsEvent, ui, view) {
    eventStartDate = event.start;
    var currentView = $('#calendar').fullCalendar('getView');
    $('td[class*="fc-day"]').each(function () {
        // Get current day
        if (
            ($(this).offset().top < jsEvent.pageY) && ($(this).offset().top + $(this).height() > jsEvent.pageY) &&
            ($(this).offset().left < jsEvent.pageX) && ($(this).offset().left + $(this).width() > jsEvent.pageX)
           ) {
            startDrag = $(this).data('date');
        }
    });
}

function eventDragStop(event, jsEvent, ui, view) {
    var currentView = $('#calendar').fullCalendar('getView');
    $('td[class*="fc-day"]').each(function () {
        // Get current day
        if (
            ($(this).offset().top < jsEvent.pageY) && ($(this).offset().top + $(this).height() > jsEvent.pageY) &&
            ($(this).offset().left < jsEvent.pageX) && ($(this).offset().left + $(this).width() > jsEvent.pageX)
           ) {
            endDrag = $(this).data('date');
        }
    });
}

function moveEvent(calEvent, delta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
    // We are going to assume they want to leave start on weekend if that is where they put it.
    // !!! WHEN SHOW WEEKENDS IS NOT TRUE THIS IS NOT WORING CORRECTLY WILL LEAVE ON SAT OR SUNDAY !!!
    // If the new date is dropped on a holiday or weekend set it to the day before.
    var dayDiff = null;
    var start = calEvent.start;
    var newDate = start;
    var oldDate = start.add(delta, 'days');// new Date(start.getFullYear(), start.getMonth(), start.getDate()).setDate(newDate.getDate() - delta);
    // This is global variable information (startDrag, endDrag, and eventStartDate).
    if ((startDrag != null) && (endDrag != null)) {
        var startDragDate = new Date(startDrag);
        var endDragDate = new Date(endDrag);
        dayDiff = BusinessDayDifference(startDragDate, endDragDate);
        momAddBusinessDays(eventStartDate, dayDiff);
        start = eventStartDate;
    }
    else {
        dayDiff = BusinessDayDifference(oldDate, newDate);
        start = eventStartDate;
        calEvent.end = start;
    }
    calEvent.delta = calEvent.delta + dayDiff;
    calEvent.end = start;
    var endDays = momAddBusinessDays(calEvent.end, taskDuration(calEvent.duration));
    checkMove(calEvent);
    if ($("#MoveStyle").val() == 'Locked') {
        moveChildren(calEvent, dayDiff);
    }
    else if ($("#MoveStyle").val() == 'Float') {
        if (delta > 0) {
            moveChildrenFloat(calEvent, dayDiff);
        }
        else {
            moveChildren(calEvent, dayDiff);
        }
    }
    else if ($("#MoveStyle").val() == 'All') {
        moveAll(calEvent, oldDate, dayDiff);
    }
    selectEvent(calEvent, calEvent, null);
}

function refreshCalendar() {
    delete calendarEvents;
    calendarEvents = null;
    $('#calendar').fullCalendar('destroy');
    renderCalendar();
}

function cancelUpdates() {
    refreshCalendar();
}

function postCalendarUpdates() {
    var events = $('#calendar').fullCalendar('clientEvents', function (event) { return event.delta != 0; });
    if ((events != null) && (events.length > 0)) {
        $.ajax({
            url: root + '/Super/JobUpdate',
            data: JSON.stringify(events),
            success: function () { refreshCalendar(); },
            error: function (XMLHttpRequest, textStatus, errorThrown) { alert(errorThrown); },
            type: 'POST',
            cache: false,
            contentType: 'application/json, charset=utf-8',
            dataType: 'json'
        });
    }
}

function highlightChildren(calEvent) {
    calEvent.backgroundColor = 'Yellow';
    calEvent.textColor = 'Black';
    if (($("#MoveStyle").val() == 'Locked') || ($("#MoveStyle").val() == 'Float')) {
        $(calEvent.dependents).each(function (index, value) {
            $('#calendar').fullCalendar('clientEvents', value.id)[0].backgroundColor = 'Yellow';
            $('#calendar').fullCalendar('clientEvents', value.id)[0].textColor = 'Black';
            highlightChildren($('#calendar').fullCalendar('clientEvents', value.id)[0]);
        });
    }
    else if ($("#MoveStyle").val() == 'All') {
        $(calendarEvents).each(function (index, value) {
            var eventChild = $('#calendar').fullCalendar('clientEvents', value.id)[0];
            if ((eventChild.id != calEvent.id) && (eventChild.start >= calEvent.start)) {
                $('#calendar').fullCalendar('clientEvents', value.id)[0].backgroundColor = 'Yellow';
                $('#calendar').fullCalendar('clientEvents', value.id)[0].textColor = 'Black';
            }
        });
    }
}

function sortByTaskName(a, b) {
    var aName = a.taskName.toLowerCase();
    var bName = b.taskName.toLowerCase();
    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}

function sortByName(a, b) {
    var aName = a.Name.toLowerCase();
    var bName = b.Name.toLowerCase();
    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}

function fillInTasks() {
    var sortedTasks = $.extend(true, [], calendarEvents);
    sortedTasks.sort(sortByTaskName);
    var options = '<option value=""></option>';
    for (var i = 0; i < sortedTasks.length; i++) {
        options += '<option value="' + sortedTasks[i].id + '">' + sortedTasks[i].taskName + '</option>';
    }
    $('#taskSelect').html(options);
}

function fillInUnassignedTasksAndWorkers() {
    var unassignedTasks = $.grep(calendarEvents, function (n, i) {
        return ((n.workerID == null) || (n.workerID.length == 0));
    });

    unassignedTasks.sort(sortByTaskName);
    options = '';
    for (var i = 0; i < unassignedTasks.length; i++) {
        options += '<tr><td width="50%"><label>' + unassignedTasks[i].taskName + '</label></td><td width="50%"><select class="workerSelect" id="' + unassignedTasks[i].id + '"></select></td></tr>';
    }
    $('#tableAssignTasks').html(options);
    $('#buttonUnassigned').text('Unassigned tasks - ' + unassignedTasks.length.toString());
    fillInWorkers();
}

function getWorkerName(workerID) {
    if (workers != null) {
        var worker = $.grep(workers, function (n, i) {
            return n.Identifier == workerID;
        });
        if (worker.length > 0) {
            return worker[0].Name;
        } else {
            return 'Unassigned';
        }
    }
}

function fillInWorkers() {
    // workers comes from the main page JobCalendar.cshtml
    var sortedWorkers = $.extend(true, [], workers);
    sortedWorkers.sort(sortByName);
    var options = '<option value=""></option>';
    for (var i = 0; i < sortedWorkers.length; i++) {
        options += '<option value="' + sortedWorkers[i].Identifier + '">' + sortedWorkers[i].Name + '</option>';
    }
    $('.workerSelect').html(options);
    $('#selectWorker').html(options);
}

function fillInJobs() {
    // jobs comes from the main page JobCalendar.cshtml
    var sortedJobs = $.extend(true, [], jobs);
    sortedJobs.sort(sortByName);
    //var options = '<option value=""></option>';
    var options = '';
    for (var i = 0; i < sortedJobs.length; i++) {
        options += '<option value="' + sortedJobs[i].Identifier + '">' + sortedJobs[i].Name + '</option>';
    }
    $('#jobSelect').html(options);
    $('#jobSelect').val(jobID);
}

function formatEventDateTime(event) {
    event.start = moment(event.start).format();
    event.end = moment(event.end).format();
}

function formatEventTitle(event) {
    event.title = event.taskName + ' - ' + getWorkerName(event.workerID);
}

function formatEvents(rawData) {
    for (var i = 0; i < rawData.length; i++) {
        formatEventTitle(rawData[i]);
        formatEventDateTime(rawData[i]);
    }
    calendarEvents = rawData;
}

function getEvents(start, end, callback) {
    if ((calendarEvents == null) || (calendarEvents.length == 0)) {
        var jobid = getUrlVars()["jobID"];
        $.ajax({
            url: root + '/Super/JobCalendarEvents?jobID=' + jobid,
            success: function (data) {
                formatEvents(data); callback(calendarEvents); fillInJobs(); fillInTasks(); fillInUnassignedTasksAndWorkers();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) { alert(errorThrown); },
            cache: false,
            type: 'GET',
            contentType: 'application/json, charset=utf-8',
            dataType: 'json'
        });
    }
    else {
        callback(calendarEvents);
    }
}

function renderCalendar() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    // TODO: !!! FIX Touch input !!!
    //$('.ui-draggable').addTouch();

    $('#calendar').fullCalendar({
        aspectRatio: 3.0,

        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },

        weekends: $("#buttonShowWeekends").is(':checked'),

        editable: true,

        events: function (start, end, timezone, callback) { getEvents(start, end, callback); },

        eventDragStart: function (event, jsEvent, ui, view) { eventDragStart(event, jsEvent, ui, view); },

        eventDragStop: function (event, jsEvent, ui, view) { eventDragStop(event, jsEvent, ui, view); },

        eventDrop: function (event, delta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) { moveEvent(event, delta, minuteDelta, allDay, revertFunc, jsEvent, ui, view); },

        eventClick: function (calEvent, jsEvent, view) {
            selectEvent(calEvent, jsEvent, view);
        },

        eventAfterRender: function (event, element, view) {
            // Add touch dragging to event element 
            // TODO: !!! FIX Touch input !!!
            //element.addTouch();
        },

        eventRender: function (event, element) {
            element.attr('title', event.title);
        },

    });
}

function postData(url, data, successFunction, errorFunction) {
    $.ajax({
        async: true,
        url: url,
        data: JSON.stringify(data),
        success: successFunction,
        error: errorFunction,
        type: 'POST',
        cache: false,
        contentType: 'application/json, charset=utf-8',
        dataType: 'json'
    });
}

function changeWorker() {
    var calEvent = null;
    var taskID = $('#taskSelect option:selected').val();
    if (taskID != '') {
        postData(root + '/Super/AssignWorkerTask', { TaskID: $('#taskSelect option:selected').val(), WorkerID: $('#selectWorker option:selected').val() });
        for (var i = 0; i < calendarEvents.length; i++) {
            if (calendarEvents[i].id == taskID) {
                calEvent = calendarEvents[i];
                calendarEvents[i].workerID = $('#selectWorker option:selected').val();
                //$('#selectWorker').attr('disabled', 'disabled');
                formatEventTitle(calendarEvents[i]);
            }
        }
        //$('#calendar').fullCalendar('destroy');
        //renderCalendar();
        $('#calendar').fullCalendar('renderEvent', calEvent);
    }
    fillInUnassignedTasksAndWorkers();
}

function changeJob() {
    var jobID = $('#jobSelect option:selected').val();
    var url = root + '/Super/JobCalendar?jobID=' + jobID;
    window.location = url;
}

$(document).ready(function () {
    renderCalendar();
    $("#buttonShowWeekends").click(function () {
        $('#calendar').fullCalendar('destroy');
        renderCalendar();
    });

    $("input[name='MoveStyle']").change(function () {
        $($('#calendar').fullCalendar('clientEvents')).each(function (index, value) {
            if (value.className != 'fc-moved') {
                value.backgroundColor = '';
                value.textColor = '';
            }
            else {
                value.backgroundColor = 'Green';
                value.textColor = 'Black';
            }
        });
        selectTask($('#taskSelect option:selected').val());
    });

    $('#taskSelect').change(function () { selectTaskFromDropDown($('#taskSelect option:selected').val()); });
});
