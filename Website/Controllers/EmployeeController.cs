﻿using BuildOnTime.BusinessLogic;
using BuildOnTime.BusinessObject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Website.Models;

namespace Website.Controllers
{
    [BotAuthorize(Roles = "Administrator")]
    public class EmployeeController : Controller
    {
        public static List<EmployeeModel> employeeList = new List<EmployeeModel>();

        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(EmployeeModel EmployeeModel)
        {
            if (ModelState.IsValid)
            {
                int newId = employeeList.Count == 0 ? 0 : (employeeList.Max(sl => sl.Id) + 1);
                EmployeeModel.Id = newId;
                employeeList.Add(EmployeeModel);
                EmployeeBLL.Create(new Employee
                {
                    FirstName = EmployeeModel.FirstName,
                    LastName = EmployeeModel.LastName,
                    Email = EmployeeModel.Email,
                    CellPhone = EmployeeModel.CellPhone,

                }, EmployeeModel.Password);
                return RedirectToAction("List");

            }
            return View(EmployeeModel);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Employee emp = EmployeeBLL.Read(id);
            return View(new EmployeeModel
            {
                FirstName = emp.FirstName,
                LastName = emp.LastName,
                Password = emp.Password,
            });
        }
        [HttpPost]
        public ActionResult Edit(EmployeeModel EmployeeModel)
        {
            if (ModelState.IsValid)
            {
                EmployeeBLL.Update(new Employee
                {
                    FirstName = EmployeeModel.FirstName,
                    LastName = EmployeeModel.LastName,
                    Password = EmployeeModel.Password,
                });
                return RedirectToAction("List");
            }
            return View(EmployeeModel);
        }

        [HttpGet]
        public ActionResult List()
        {
            List<Website.Models.EmployeeListItemModel> employeeList = new List<EmployeeListItemModel>();
            var storedEmployeeList = EmployeeBLL.List(BuilderBLL.Read(3));
            foreach(var employee in storedEmployeeList)
            {
                employeeList.Add(new EmployeeListItemModel
                {
                    Id = (int)employee.Identifier,
                    Name = employee.Name,
                    CellPhone = employee.CellPhone,
                    Email = employee.Email,
                });
            }

            //List<Website.Models.SubcontractorListItemModel> subList = new List<SubcontractorListItemModel>();
            //foreach (SubcontractorModel sub in subcontractorList)
            //{
            //    subList.Add(new SubcontractorListItemModel
            //    {
            //        CellPhone = sub.CellPhone,
            //        CompanyName = sub.CompanyName,
            //        Id = sub.Id,
            //        Name = sub.Name,
            //        OfficePhone = sub.OfficePhone
            //    });
            //}
            employeeList = employeeList.OrderBy(el => el.Name).ToList();
            return View(employeeList);
        }
    }
}