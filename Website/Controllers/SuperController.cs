﻿using BuildOnTime.BusinessLogic;
using BuildOnTime.BusinessObject;
using BuildOnTime.Presentation;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Website.Models;

namespace Website.Controllers
{
    [BotAuthorize(Roles = "Administrator,Superintendent")]
    public class SuperController : Controller
    {
        public ActionResult Index()
        {
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            DailyTaskList dailyTasks = new DailyTaskList(employee);
            List<SelectListItem> itemList = new SelectList(WorkerPLL.List(employee.Builder), "Identifier", "Name").ToList();
            itemList.Insert(0, new SelectListItem { Text = "", Value = "" });
            ViewBag.WorkerList = new JavaScriptSerializer().Serialize(itemList.Select(sl => new { sl.Value, sl.Text }));
            return View("Index", dailyTasks);
        }

        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult Jobs()
        {
            HomeModel homeModel = new HomeModel();
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            DateTime cutOffDate = DateTime.Now.AddDays(-7);
            List<Job> jobList = JobBLL.Search(employee.Builder.Identifier, cutOffDate);
            List<Employee> employees = EmployeeBLL.List(employee.Builder);
            List<JobModel> jobModels = new List<JobModel>();
            foreach (Job job in jobList)
            {
                JobModel jobModel = new JobModel();
                jobModel.InjectFrom(job);
                jobModel.SuperName = employees.Find(delegate (Employee em) { return em.Identifier.Equals(job.SuperID); }).Name;
                homeModel.Jobs.Add(jobModel);
            }
            return View("Jobs", homeModel);
        }

        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult SubcontractorList()
        {
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            Builder builder = BuilderBLL.Read(employee.Builder.Identifier);
            List<Subcontractor> subcontractors = SubcontractorBLL.List(builder.Identifier);

            List<TaskModel> taskModels = new List<TaskModel>();
            return View("SubcontractorList", taskModels);
        }

        [HttpPost]
        public JsonResult AssignWorkerTask(TaskWorkerModel taskWorkerModel)
        {
            JobTask jobTask = JobTaskBLL.Read(taskWorkerModel.TaskID);
            JobTaskBLL.WorkerDissociate(jobTask);
            string employeeID = Worker.EmployeeID(taskWorkerModel.WorkerID);
            string subcontractorID = Worker.SubcontractorID(taskWorkerModel.WorkerID);
            if (employeeID != null)
            {
                JobTaskBLL.EmployeeAssociate(jobTask, employeeID);
            }
            else if (subcontractorID != null)
            {
                JobTaskBLL.SubcontractorAssociate(jobTask, subcontractorID);
            }
            return Json(new { Status = "Success" });
        }

        [HttpPost]
        public JsonResult AssignWorkersTasks(IEnumerable<TaskWorkerModel> tasksWorkers)
        {
            foreach (TaskWorkerModel taskWorkerModel in tasksWorkers)
            {
                JobTask jobTask = JobTaskBLL.Read(taskWorkerModel.TaskID);
                string employeeID = Worker.EmployeeID(taskWorkerModel.WorkerID);
                string subcontractorID = Worker.SubcontractorID(taskWorkerModel.WorkerID);
                if (employeeID != null)
                {
                    JobTaskBLL.EmployeeAssociate(jobTask, employeeID);
                }
                else if (subcontractorID != null)
                {
                    JobTaskBLL.SubcontractorAssociate(jobTask, subcontractorID);
                }
            }
            return Json(new { Status = "Success" });
        }


        [HttpPost]
        public JsonResult CalledWorkersTasks(List<TaskCommunicationModel> calledWorkers)
        {
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            Builder builder = BuilderBLL.Read(employee.Builder.Identifier);
            foreach (TaskCommunicationModel calledWorker in calledWorkers)
            {
                JobTask jobTask = JobTaskBLL.Read(calledWorker.TaskID);
                string employeeID = Worker.EmployeeID(calledWorker.WorkerID);
                string subcontractorID = Worker.SubcontractorID(calledWorker.WorkerID);

                //CommunicationReasonEnum communicationReason = (CommunicationReasonEnum)Enum.Parse(typeof(CommunicationReasonEnum), gridView.DataKeys[gridViewRow.RowIndex].Values["Reason"].ToString());
                if (subcontractorID != null)
                {
                    CommunicationPLL.SubcontractorCommunicate(employee.Builder.Identifier, subcontractorID, jobTask.Identifier, null, employee.Identifier, calledWorker.Reason, CommunicationMethodEnum.Phone, "blank");
                }
                if (employeeID != null)
                {
                    CommunicationPLL.EmployeeCommunicate(employee.Builder.Identifier, employeeID, jobTask.Identifier, null, employee.Identifier, calledWorker.Reason, CommunicationMethodEnum.Phone, "blank");
                }
            }
            return Json(new { Status = "Success" });
        }

        [HttpPost]
        public JsonResult ReviewedTasks(List<string> reviewedTasks)
        {
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            Builder builder = BuilderBLL.Read(employee.Builder.Identifier);
            foreach (string reviewedTask in reviewedTasks)
            {
                JobTask jobTask = JobTaskBLL.Read(reviewedTask);
                jobTask.Signoff = DateTime.Now;
                JobTaskBLL.Update(jobTask);
            }
            return Json(new { Status = "Success" });
        }

        [HttpPost]
        public JsonResult EmailTasks(List<TaskCommunicationModel> emailTasks)
        {
            List<CommunicationEMailTask> communicationTasks = new List<CommunicationEMailTask>();
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            Builder builder = BuilderBLL.Read(employee.Builder.Identifier);
            foreach (TaskCommunicationModel emailTask in emailTasks)
            {
                JobTask jobTask = JobTaskBLL.Read(emailTask.TaskID);
                string employeeID = Worker.EmployeeID(emailTask.WorkerID);
                string subcontractorID = Worker.SubcontractorID(emailTask.WorkerID);

                communicationTasks.Add(new CommunicationEMailTask(jobTask, employee, emailTask.Reason));
            }
            CommunicationPLL.SubcontractorEmail(communicationTasks);
            return Json(new { Status = "Success" });
        }

        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult SubcontractorView(int subcontractorid)
        {
            DateTime startDate = DateTime.Now.AddDays(-7);
            DateTime endDate = startDate.AddDays(365);
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            Builder builder = BuilderBLL.Read(employee.Builder.Identifier);
            Subcontractor subcontractor = SubcontractorBLL.Read(subcontractorid);
            List<SubJobTaskDisplay> jobTasks = SubcontractorPLL.SubcontractorTasks(builder, subcontractor, startDate, endDate);
            List<TaskModel> taskModels = new List<TaskModel>();
            foreach (SubJobTaskDisplay taskDisplay in jobTasks)
            {
                TaskModel taskModel = new TaskModel();
                taskModel.Builder = taskDisplay.BuilderName;
                taskModel.Duration = taskDisplay.TimeFrame;
                taskModel.Job = taskDisplay.JobName;
                taskModel.Name = taskDisplay.TaskName;
                taskModel.StartDate = taskDisplay.Start;
                taskModel.Status = taskDisplay.Status.ToString();
                taskModel.StatusDate = taskDisplay.Updated;
                taskModel.Super = taskDisplay.SuperName;
                taskModels.Add(taskModel);
            }
            return View("SubcontractorView", taskModels);
        }

        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult JobCalendar(int jobID)
        {
            Job job = JobBLL.Read(jobID);
            JobWorkerModel calendarModel = new JobWorkerModel();
            ViewBag.TitleDescription = job.Name;
            calendarModel.JobID = job.Identifier.ToString();
            calendarModel.JobName = job.Name;
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            calendarModel.Workers = WorkerPLL.List(employee.Builder);
            calendarModel.Jobs = JobBLL.ActiveJobs(employee.Builder.Identifier);
            return View("JobCalendar", calendarModel);
        }

        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0)]
        public JsonResult JobCalendarEvents(int jobID)
        {
            Job job = JobBLL.Read(jobID);
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            List<JobEvent> events = CalendarEventPLL.JobEvents(employee, jobID);
            List<CalendarModel> calendarTasks = new List<CalendarModel>();
            foreach (JobEvent timeEvent in events)
            {
                CalendarModel model = new CalendarModel();
                model.allDay = true;
                // Take off one second so day is correct.  In database a job of one day to start Thursday May 24, 2012 would be stored start 05-24-2012 end 05-25-2012
                // this is how the web forms control handle it MVC FullCalendar wants them to be the same day.
                model.end = timeEvent.End.AddSeconds(-1).ToString("yyyy-MM-dd");
                model.start = timeEvent.Start.ToString("yyyy-MM-dd");
                model.title = "";
                model.id = timeEvent.JobTaskID.ToString();
                model.workerID = timeEvent.WorkerID;
                model.taskName = timeEvent.TaskName;
                model.weekends = false;
                //model.dependents.AddRange(timeEvent.Dependents);
                foreach (string dependentID in timeEvent.Dependents)
                {
                    int? startBuffer = null;
                    int? endBuffer = null;
                    JobEvent dependentEvent = events.First(je => je.JobTaskID.ToString() == dependentID);
                    if (dependentEvent.Start > timeEvent.End)
                    {
                        endBuffer = 1;
                    }
                    else
                    {
                        startBuffer = CalendarBLL.WorkDateDifference(timeEvent.Start, dependentEvent.Start);
                    }
                    model.dependents.Add(new CalendarDependentModel { id = dependentID, startBuffer = startBuffer, endBuffer = endBuffer });
                }
                model.duration = timeEvent.Duration;
                calendarTasks.Add(model);
            }
            return Json(calendarTasks.ToArray(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult JobUpdate(CalendarModel[] calendarUpdates)
        {
            if (calendarUpdates.Length > 0)
            {
                Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
                JobDelay jobDelay = null;
                int delayLength = 0;
                delayLength = calendarUpdates[0].delta;
                if (jobDelay == null)
                {
                    JobTask jobTask = JobTaskBLL.Read(calendarUpdates[0].id);
                    jobDelay = new JobDelay();
                    jobDelay.DelayedBy = "Unknown";
                    jobDelay.DelayedByEmployeeID = employee.Identifier;
                    jobDelay.DelayedBySubcontractorID = null;
                    jobDelay.DelayLength = delayLength;
                    jobDelay.JobID = jobTask.JobID;
                    jobDelay.Reason = "Unknown";
                    jobDelay = JobDelayBLL.Create(jobDelay);
                }

                foreach (CalendarModel update in calendarUpdates)
                {
                    JobTask jobTask = JobTaskBLL.Read(update.id);
                    DateTime originalStart = jobTask.Start;
                    DateTime originalFinish = jobTask.Finish;
                    jobTask.Start = DateTimeOffset.Parse(update.start).Date;
                    // fullCalendar send in one day all day events with a null end date; compensate.
                    if (update.end == null)
                    {
                        jobTask.Finish = DateTimeOffset.Parse(update.start).Date.AddDays(1);
                    }
                    else
                    {
                        jobTask.Finish = DateTimeOffset.Parse(update.end).Date.AddDays(1);
                    }
                    JobTaskBLL.Update(jobTask);
                    JobDelayBLL.AssociateJobTask(jobDelay.Identifier, jobTask.Identifier, originalStart, originalFinish, jobTask.Start, jobTask.Finish);
                }
            }
            return null;
        }

        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult Home()
        {
            HomeModel homeModel = new HomeModel();
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            DateTime cutOffDate = DateTime.Now.AddDays(-7);
            List<Job> jobList = JobBLL.Search(employee.Builder.Identifier, cutOffDate);
            List<Employee> employees = EmployeeBLL.List(employee.Builder);
            List<JobModel> jobModels = new List<JobModel>();
            foreach (Job job in jobList)
            {
                JobModel jobModel = new JobModel();
                jobModel.InjectFrom(job);
                jobModel.SuperName = employees.Find(delegate (Employee em) { return em.Identifier.Equals(job.SuperID); }).Name;
                homeModel.Jobs.Add(jobModel);
            }
            return View("Index", homeModel);
        }


        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0)]
        public JsonResult Subload(DateTime start, DateTime end)
        {
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            Builder builder = BuilderBLL.Read(employee.Builder.Identifier);
            DateTime monthStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime monthEnd = monthStart.AddMonths(1);
            List<SubcontractorLoad> subLoads = new List<SubcontractorLoad>();
            List<JobTaskDisplay> jobTasks = SubcontractorPLL.SubcontractorTasks(builder.Identifier, monthStart, monthEnd, out subLoads);
            BuilderSubContractorWorkloadModel workLoadModel = new BuilderSubContractorWorkloadModel();
            workLoadModel.Start = start;
            workLoadModel.End = end;
            workLoadModel.SubcontractorWorkloads = new List<SubLoadModel>();
            foreach (SubcontractorLoad subLoad in subLoads)
            {
                SubLoadModel subLoadModel = new SubLoadModel();
                subLoadModel.Tasks = new List<CalendarModel>();
                subLoadModel.SubcontractorName = subLoad.SubcontractorName;
                subLoadModel.TaskCount = subLoad.JobTasks.Count;
                subLoadModel.SubcontractorID = subLoad.SubcontractorIdentifier.ToString();
                foreach (JobTaskDisplay jtd in subLoad.JobTasks)
                {
                    CalendarModel calendarModel = new CalendarModel();
                    calendarModel.allDay = true;
                    calendarModel.start = jtd.Start.ToString("yyyy-MM-dd hh:mm:ss");
                    calendarModel.end = jtd.Finish.AddSeconds(-1).ToString("yyyy-MM-dd hh:mm:ss");
                    calendarModel.title = jtd.SubcontractorName + " - " + jtd.JobName + ": " + jtd.TaskName;
                    calendarModel.id = jtd.Identifier.ToString();
                    subLoadModel.Tasks.Add(calendarModel);
                }
                workLoadModel.SubcontractorWorkloads.Add(subLoadModel);

                //foreach(
                //subLoadModel.Tasks = subLoad.JobTasks;
            }
            return Json(workLoadModel, JsonRequestBehavior.AllowGet);
        }
    }

}