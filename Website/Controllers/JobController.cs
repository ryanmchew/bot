﻿using BuildOnTime.BusinessLogic;
using BuildOnTime.BusinessObject;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    [BotAuthorize(Roles = "Administrator,Superintendent")]
    public class JobController : Controller
    {
        //public static List<JobModel> jobList = new List<JobModel>(); 

        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult Index()
        {
            HomeModel homeModel = new HomeModel();
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            DateTime cutOffDate = DateTime.Now.AddDays(-7);
            List<Job> jobList = JobBLL.Search(employee.Builder.Identifier, cutOffDate);
            List<Employee> employees = EmployeeBLL.List(employee.Builder);
            List<JobModel> jobModels = new List<JobModel>();
            foreach (Job job in jobList)
            {
                JobModel jobModel = new JobModel();
                jobModel.InjectFrom(job);
                jobModel.SuperName = employees.Find(delegate (Employee em) { return em.Identifier.Equals(job.SuperID); }).Name;
                homeModel.Jobs.Add(jobModel);
            }
            return View(homeModel.Jobs);
        }

        //
        // GET: /Job/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Job/Create
        [HttpGet]
        public ActionResult Create()
        {
            JobModel jobModel = new JobModel();
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            List<Employee> supers = EmployeeBLL.ListSuper(employee.Builder);
            ViewBag.supers = supers;
            List<Schedule> schedules = ScheduleBLL.Search(employee.Builder.Identifier, null);
            ViewBag.schedules = schedules;
            return View(jobModel);
        }

        //
        // POST: /Job/Create

        [HttpPost]
        public ActionResult Create(JobModel model)
        {
            try
            {
                Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
                Job job = new Job();
                Job persistedJob = JobBLL.CreateJobSchedule((int)employee.Builder.Identifier, (int)model.SuperID, (int)model.ScheduleID.Value, model.start.Value, model.Name, model.Permit, model.Description, model.LotIdentifier, model.Address1, model.City, model.PostalCode, model.State);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Job/Edit/5

        public ActionResult Edit(int id)
        {
            Job job = JobBLL.Read(id);
            Site site = SiteBLL.Read(job.SiteID);
            JobModel jobModel = new JobModel();
            jobModel.InjectFrom(site);
            jobModel.InjectFrom(job);
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            List<Employee> supers = EmployeeBLL.ListSuper(employee.Builder);
            ViewBag.supers = supers;
            return View(jobModel);
        }

        //
        // POST: /Job/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, JobModel model)
        {
            try
            {
                Job job = JobBLL.Read(id);
                job.Identifier = id;
                Site site = SiteBLL.Read(job.SiteID);
                job.InjectFrom(model);
                site.InjectFrom(model);
                site.Identifier = job.SiteID;
                JobBLL.Update(job);
                SiteBLL.Update(site);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Job/Retire/5 
        public ActionResult Retire(int id)
        {
            Job job = JobBLL.Read(id);
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            JobBLL.Delete(job, (int)employee.Identifier, Request.UserHostAddress + ": " + Request.UserHostName + " - JobController.Delete");
            return RedirectToAction("Index");
        }

        //
        // POST: /Job/Delete/5
        [HttpPost]
        public ActionResult Retire(int id, FormCollection collection)
        {
            return Retire(id);
        }
    }
    // GET: Job
    //    public ActionResult Index()
    //    {
    //        return View();
    //    }

    //    [HttpGet]
    //    public ActionResult Create()
    //    {
    //        var asb = BuilderBLL.Read(3);
    //        var employeeList = EmployeeBLL.ListSuper(asb);
    //        var superList = new SelectList(employeeList.OrderBy(x => x.Name), "Identifier", "Name");
    //        ViewBag.SuperDropDownList = superList;
    //        return View();
    //    }

    //    [HttpPost]
    //    public ActionResult Create(JobModel JobModel)
    //    {
    //        if (ModelState.IsValid)
    //        {
    //            Site site = SiteBLL.Create(new Site
    //            {
    //                Address1 = JobModel.Address,
    //                BuilderID = 3,
    //                City = JobModel.City,
    //                LotIdentifier = JobModel.LotNumber,
    //                PostalCode = JobModel.PostalCode,
    //                State = JobModel.State
    //            });
    //            Job job = JobBLL.CreateJobSchedule(3, JobModel.SuperId,  6, JobModel.Start, JobModel.Name, JobModel.Permit, JobModel.Description, JobModel.LotNumber, JobModel.Address, JobModel.City, JobModel.PostalCode, JobModel.State);
    //            return RedirectToAction("List");

    //        }
    //        return View(JobModel);
    //    }

    //    [HttpGet]
    //    public ActionResult Edit(int id)
    //    {
    //        JobModel job = jobList.FirstOrDefault(sl => sl.Id == id);
    //        return View(job);
    //    }

    //    [HttpPost]
    //    public ActionResult Edit(JobModel JobModel)
    //    {
    //        if (ModelState.IsValid)
    //        {
    //            JobModel job = jobList.FirstOrDefault(sl => sl.Id == JobModel.Id);
    //            jobList.Remove(job);
    //            jobList.Add(JobModel);
    //            return RedirectToAction("List");
    //        }
    //        return View(JobModel);
    //    }

    //    [HttpGet]
    //    public ActionResult List()
    //    {
    //        List<Website.Models.JobListItemModel> jobList = new List<JobListItemModel>();
    //        var storedJobList = JobBLL.ActiveJobs(3);
    //        foreach(var job in storedJobList)
    //        {
    //            var site = SiteBLL.Read(job.SiteID);
    //            var super = EmployeeBLL.Read(job.SuperID);
    //            jobList.Add(new JobListItemModel
    //            {
    //                Description = job.Description,
    //                Finish = job.finish?.ToString(),                                          
    //                Id = job.Identifier.Value,
    //                LotNumber = site.LotIdentifier,
    //                Permit = job.Permit,
    //                Start = job.start?.ToString(),
    //                Superintendent = super.FirstName + " " + super.LastName,
    //            });
    //        }

    //        //List<Website.Models.JobListItemModel> jobList = new List<JobListItemModel>();
    //        //foreach (JobModel job in jobList)
    //        //{
    //        //    jobList.Add(new JobListItemModel
    //        //    {
    //        //      LotNumber = job.LotNumber,
    //        //        Id = (int)job.Identifier,
    //        //        Superintendent = job.Superintendent,
    //        //        Description = job.Description,
    //        //        Permit = job.Permit,
    //        //        Start = job.Start,
    //        //        Finish = job.Finish,
    //        //    });
    //        //}
    //        return View(jobList);
    //    }
    //}
}