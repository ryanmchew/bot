﻿using BuildOnTime.BusinessLogic;
using BuildOnTime.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;

namespace Website.Controllers
{
    public class BotAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var userId = httpContext.Session["SystemUserID"];
            // If the user is not authorzied don't go any further
            if (userId == null) { return false; }
            var systemUser = SystemUserBLL.Read(userId);
            var possibleRoles = Roles.ToString().Split(',');
            bool authorized = false;
            foreach (var testRole in possibleRoles)
            {
                Role okRole = (Role)Enum.Parse(typeof(Role), testRole, true);
                if (systemUser.Roles.Contains(okRole))
                {
                    authorized = true;
                    break;
                }
            }
            if (!authorized) { return false; }
            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {

            if (filterContext.HttpContext.Items.Contains("redirectToCompleteProfile"))
            {
                var routeValues = new RouteValueDictionary(new
                {
                    controller = "someController",
                    action = "someAction",
                });
                filterContext.Result = new RedirectToRouteResult(routeValues);
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }

        private bool IsProfileCompleted(string user)
        {
            // You know what to do here => go hit your database to verify if the
            // current user has already completed his profile by checking
            // the corresponding field
            throw new NotImplementedException();
        }
    }
}