﻿using BuildOnTime.BusinessLogic;
using BuildOnTime.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    [BotAuthorize(Roles = "Administrator,Superintendent")]
    public class SubcontractorController : Controller
    {
        public static List<SubcontractorModel> subcontractorList = new List<SubcontractorModel>();

        // GET: Subcontractor
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(SubcontractorModel SubcontractorModel)
        {
            if (ModelState.IsValid)
            {
                int newId = subcontractorList.Count == 0 ? 0 : (subcontractorList.Max(sl => sl.Id) + 1);
                SubcontractorModel.Id = newId;
                subcontractorList.Add(SubcontractorModel);
                SubcontractorBLL.Create(new Subcontractor
                {
                    AddressLine1 = SubcontractorModel.AddressLine1,
                    AddressLine2 = SubcontractorModel.AddressLine2,
                    City = SubcontractorModel.City,
                    BuilderID = 3,
                    Name = SubcontractorModel.Name,
                    CellPhone = SubcontractorModel.CellPhone,
                    OfficeFax = SubcontractorModel.OfficeFax,
                    OfficePhone = SubcontractorModel.OfficePhone,
                    PostalCode = SubcontractorModel.PostalCode,
                    State = SubcontractorModel.State,

                });
                return RedirectToAction("List");

            }
            return View(SubcontractorModel);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Subcontractor sub = SubcontractorBLL.Read(id);
            return View(new SubcontractorModel
            {
                AddressLine1 = sub.AddressLine1,
                AddressLine2 = sub.AddressLine2,
                City = sub.City,
                CellPhone = sub.CellPhone,
                Id = (int) sub.Identifier,
                Email = sub.Email,
                Name = sub.Name,
                OfficeFax = sub.OfficeFax,
                OfficePhone = sub.OfficePhone,
                PostalCode = sub.PostalCode,
                State = sub.State,

            });
        }

        [HttpPost]
        public ActionResult Edit(SubcontractorModel SubcontractorModel)
        {
            if (ModelState.IsValid)
            {
                SubcontractorBLL.Update(new Subcontractor
                {
                    AddressLine1 = SubcontractorModel.AddressLine1,
                    AddressLine2 = SubcontractorModel.AddressLine2,
                    City = SubcontractorModel.City,
                    BuilderID = 3,
                    Name = SubcontractorModel.Name,
                    Identifier = SubcontractorModel.Id,
                    CellPhone = SubcontractorModel.CellPhone,
                    OfficeFax = SubcontractorModel.OfficeFax,
                    OfficePhone = SubcontractorModel.OfficePhone,
                    PostalCode = SubcontractorModel.PostalCode,
                    State = SubcontractorModel.State,

                });
                return RedirectToAction("List");
            }
            return View(SubcontractorModel);
        }

        [HttpGet]
        public ActionResult List()
        {
            List<Website.Models.SubcontractorListItemModel> subList = new List<SubcontractorListItemModel>();
            var storedSubList = SubcontractorBLL.List(3);
            foreach(var sub in storedSubList)
            {
                subList.Add(new SubcontractorListItemModel
                {
                    //CellPhone = sub.CellPhone,
                    CompanyName = sub.Name,
                    Id = (int)sub.Identifier,
                    Name = sub.Name,
                    OfficePhone = sub.OfficePhone,
                    OfficeFax = sub.OfficeFax,
                    CellPhone = sub.CellPhone,
                });
            }

            //List<Website.Models.SubcontractorListItemModel> subList = new List<SubcontractorListItemModel>();
            //foreach (SubcontractorModel sub in subcontractorList)
            //{
            //    subList.Add(new SubcontractorListItemModel
            //    {
            //        CellPhone = sub.CellPhone,
            //        CompanyName = sub.CompanyName,
            //        Id = sub.Id,
            //        Name = sub.Name,
            //        OfficePhone = sub.OfficePhone
            //    });
            //}
            return View(subList);
        }
    }
}