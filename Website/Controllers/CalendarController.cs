﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuildOnTimeMVC.Models;
using System.Globalization;
using BuildOnTime.Presentation;
using BuildOnTime.BusinessLogic;
using BuildOnTime.BusinessObject;
using Website.Models;

namespace Website.Controllers
{
    [BotAuthorize(Roles = "Administrator,Superintendent")]
    public class CalendarController : Controller
    {
        //
        // GET: /Calendar/

        private double ConvertToTimestamp(DateTime value)
        {
            return DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
        }

        public ActionResult Index()
        {
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            Builder builder = BuilderBLL.Read(employee.Builder.Identifier);
            DateTime monthStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime monthEnd = monthStart.AddMonths(1);
            List<SubcontractorLoad> subLoads = new List<SubcontractorLoad>();
            List<JobTaskDisplay> jobTasks = SubcontractorPLL.SubcontractorTasks(builder.Identifier, monthStart, monthEnd, out subLoads);
            BuilderSubContractorWorkloadModel workLoadModel = new BuilderSubContractorWorkloadModel();
            workLoadModel.SubcontractorWorkloads = new List<SubLoadModel>();
            foreach (SubcontractorLoad subLoad in subLoads)
            {
                SubLoadModel subLoadModel = new SubLoadModel();
                subLoadModel.Tasks = new List<CalendarModel>();
                subLoadModel.SubcontractorName = subLoad.SubcontractorName;
                subLoadModel.TaskCount = subLoad.JobTasks.Count;
                subLoadModel.SubcontractorID = subLoad.SubcontractorIdentifier.ToString();
                foreach (JobTaskDisplay jtd in subLoad.JobTasks)
                {
                    CalendarModel calendarModel = new CalendarModel();
                    calendarModel.allDay = true;
                    calendarModel.start = jtd.Start.ToString("yyyy-MM-dd hh:mm:ss");
                    calendarModel.end = jtd.Finish.AddSeconds(-1).ToString("yyyy-MM-dd hh:mm:ss");
                    calendarModel.title = jtd.SubcontractorName + " - " + jtd.JobName + ": " + jtd.TaskName;
                    calendarModel.id = jtd.Identifier.ToString();
                    subLoadModel.Tasks.Add(calendarModel);
                }
                workLoadModel.SubcontractorWorkloads.Add(subLoadModel);

                //foreach(
                //subLoadModel.Tasks = subLoad.JobTasks;
            }
            return View("SubcontractorLoad", workLoadModel);
        }

        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0)]
        public JsonResult SubLoad(string start, string end)
        {
            Employee employee = EmployeeBLL.ReadBySystemUser(Session["SystemUserID"]);
            Builder builder = BuilderBLL.Read(employee.Builder.Identifier);
            DateTime monthStart = DateTime.Parse(start);
            DateTime monthEnd = DateTime.Parse(end);
            List<SubcontractorLoad> subLoads = new List<SubcontractorLoad>();
            List<JobTaskDisplay> jobTasks = SubcontractorPLL.SubcontractorTasks(builder.Identifier, monthStart, monthEnd, out subLoads);
            BuilderSubContractorWorkloadModel workLoadModel = new BuilderSubContractorWorkloadModel();
            workLoadModel.Start = DateTime.Parse(start);
            workLoadModel.End = DateTime.Parse(end);
            workLoadModel.SubcontractorWorkloads = new List<SubLoadModel>();
            foreach (SubcontractorLoad subLoad in subLoads)
            {
                SubLoadModel subLoadModel = new SubLoadModel();
                subLoadModel.Tasks = new List<CalendarModel>();
                subLoadModel.SubcontractorName = subLoad.SubcontractorName;
                subLoadModel.TaskCount = subLoad.JobTasks.Count;
                subLoadModel.SubcontractorID = subLoad.SubcontractorIdentifier.ToString();
                foreach (JobTaskDisplay jtd in subLoad.JobTasks)
                {
                    CalendarModel calendarModel = new CalendarModel();
                    calendarModel.allDay = true;
                    calendarModel.start = jtd.Start.ToString("yyyy-MM-dd hh:mm:ss");
                    calendarModel.end = jtd.Finish.AddSeconds(-1).ToString("yyyy-MM-dd hh:mm:ss");
                    calendarModel.title = jtd.SubcontractorName + " - " + jtd.JobName + ": " + jtd.TaskName;
                    calendarModel.id = jtd.Identifier.ToString();
                    subLoadModel.Tasks.Add(calendarModel);
                }
                workLoadModel.SubcontractorWorkloads.Add(subLoadModel);

                //foreach(
                //subLoadModel.Tasks = subLoad.JobTasks;
            }
            return Json(workLoadModel, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult Calendar()
        {
            return Index();
        }

        public ActionResult SubcontractorLoad()
        {
            return Index();
        }

        //
        // GET: /Calendar/Details/5
    }
}
