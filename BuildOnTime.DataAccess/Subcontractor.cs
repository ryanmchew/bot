﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using BuildOnTime.BusinessObject;

namespace BuildOnTime.DataAccess
{
    public static class SubcontractorDAL
    {
        public static Subcontractor Create(Subcontractor subcontractor)
        {
            Subcontractor result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorCreate";
            SqlParameter subcontractorID = command.Parameters.Add("@SubcontractorID", System.Data.SqlDbType.Int);
            subcontractorID.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@BuilderID", subcontractor.BuilderID);
            DataAccessHelper.AddWithValue(command, "@Name", subcontractor.Name);
            DataAccessHelper.AddWithValue(command, "@AddressLine1", subcontractor.AddressLine1);
            DataAccessHelper.AddWithValue(command, "@AddressLine2", DBNull.Value);
            DataAccessHelper.AddWithValue(command, "@City", subcontractor.City);
            DataAccessHelper.AddWithValue(command, "@State", subcontractor.State);
            DataAccessHelper.AddWithValue(command, "@PostalCode", subcontractor.PostalCode);
            DataAccessHelper.AddWithValue(command, "@OfficePhone", subcontractor.OfficePhone);
            DataAccessHelper.AddWithValue(command, "@OfficeFax", subcontractor.OfficeFax);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new Subcontractor();
                result.Identifier = (int)subcontractorID.Value;
                result.AddressLine1 = subcontractor.AddressLine1;
                result.AddressLine2 = subcontractor.AddressLine2;
                result.Name = subcontractor.Name;
                result.PostalCode = subcontractor.PostalCode;
                result.State = subcontractor.State;
                result.OfficePhone = subcontractor.OfficePhone;
                result.OfficeFax = subcontractor.OfficeFax;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static Subcontractor Read(SystemUser user)
        {
            Subcontractor result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorReadUser";
            DataAccessHelper.AddWithValue(command, "@SystemUserID", user.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new Subcontractor();
                    result.Identifier = (int)dataReader["SubcontractorID"];
                    result.BuilderID = (int)dataReader["BuilderID"];
                    result.AddressLine1 = dataReader["AddressLine1"].ToString();
                    result.AddressLine2 = dataReader["AddressLine2"].ToString();
                    result.City = dataReader["City"].ToString();
                    result.Name = dataReader["Name"].ToString();
                    result.PostalCode = dataReader["PostalCode"].ToString();
                    result.State = dataReader["State"].ToString();
                    result.OfficeFax = dataReader["OfficeFax"].ToString();
                    result.OfficePhone = dataReader["OfficePhone"].ToString();
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static Subcontractor Read(object identifier)
        {
            Subcontractor result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorRead";
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", int.Parse(identifier.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new Subcontractor();
                    result.Identifier = (int)dataReader["SubcontractorID"];
                    result.BuilderID = (int)dataReader["BuilderID"];
                    result.AddressLine1 = dataReader["AddressLine1"].ToString();
                    result.AddressLine2 = dataReader["AddressLine2"].ToString();
                    result.City = dataReader["City"].ToString();
                    result.Name = dataReader["Name"].ToString();
                    result.PostalCode = dataReader["PostalCode"].ToString();
                    result.State = dataReader["State"].ToString();
                    result.OfficeFax = dataReader["OfficeFax"].ToString();
                    result.OfficePhone = dataReader["OfficePhone"].ToString();
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static Subcontractor ReadByName(string name)
        {
            Subcontractor result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorReadName";
            DataAccessHelper.AddWithValue(command, "@Name", name);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new Subcontractor();
                    result.Identifier = (int)dataReader["SubcontractorID"];
                    result.BuilderID = (int)dataReader["BuilderID"];
                    result.AddressLine1 = dataReader["AddressLine1"].ToString();
                    result.AddressLine2 = dataReader["AddressLine2"].ToString();
                    result.City = dataReader["City"].ToString();
                    result.Name = dataReader["Name"].ToString();
                    result.PostalCode = dataReader["PostalCode"].ToString();
                    result.State = dataReader["State"].ToString();
                    result.OfficeFax = dataReader["OfficeFax"].ToString();
                    result.OfficePhone = dataReader["OfficePhone"].ToString();
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void Update(Subcontractor subcontractor)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorUpdate";
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", (int)subcontractor.Identifier);
            DataAccessHelper.AddWithValue(command, "@BuilderID", subcontractor.BuilderID);
            DataAccessHelper.AddWithValue(command, "@Name", subcontractor.Name);
            DataAccessHelper.AddWithValue(command, "@AddressLine1", subcontractor.AddressLine1);
            DataAccessHelper.AddWithValue(command, "@AddressLine2", DBNull.Value);
            DataAccessHelper.AddWithValue(command, "@City", subcontractor.City);
            DataAccessHelper.AddWithValue(command, "@State", subcontractor.State);
            DataAccessHelper.AddWithValue(command, "@PostalCode", subcontractor.PostalCode);
            DataAccessHelper.AddWithValue(command, "@OfficePhone", subcontractor.OfficePhone);
            DataAccessHelper.AddWithValue(command, "@OfficeFax", subcontractor.OfficeFax);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Delete(Subcontractor subcontractor)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorDelete";
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", subcontractor.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<Subcontractor> List(object builderID)
        {
            List<Subcontractor> result = new List<Subcontractor>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorList";
            DataAccessHelper.AddWithValue(command, "@BuilderID", builderID);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Subcontractor subcontractor = new Subcontractor();
                    subcontractor.Identifier = (int)dataReader["SubcontractorID"];
                    subcontractor.BuilderID = (int)dataReader["BuilderID"];
                    subcontractor.AddressLine1 = dataReader["AddressLine1"].ToString();
                    subcontractor.AddressLine2 = dataReader["AddressLine2"].ToString();
                    subcontractor.City = dataReader["City"].ToString();
                    subcontractor.Name = dataReader["Name"].ToString();
                    subcontractor.PostalCode = dataReader["PostalCode"].ToString();
                    subcontractor.State = dataReader["State"].ToString();
                    subcontractor.OfficeFax = dataReader["OfficeFax"].ToString();
                    subcontractor.OfficePhone = dataReader["OfficePhone"].ToString();
                    result.Add(subcontractor);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void AcceptJobTask(object subcontractorID, object jobTaskID)
        {

        }

        public static List<Subcontractor> List(SystemUser subcontractorUser)
        {
            List<Subcontractor> result = new List<Subcontractor>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorListBySucontractorUser";
            DataAccessHelper.AddWithValue(command, "@SystemUserID", subcontractorUser.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Subcontractor subcontractor = new Subcontractor();
                    subcontractor.Identifier = (int)dataReader["SubcontractorID"];
                    subcontractor.BuilderID = (int)dataReader["BuilderID"];
                    subcontractor.AddressLine1 = dataReader["AddressLine1"].ToString();
                    subcontractor.AddressLine2 = dataReader["AddressLine2"].ToString();
                    subcontractor.City = dataReader["City"].ToString();
                    subcontractor.Name = dataReader["Name"].ToString();
                    subcontractor.PostalCode = dataReader["PostalCode"].ToString();
                    subcontractor.State = dataReader["State"].ToString();
                    subcontractor.OfficeFax = dataReader["OfficeFax"].ToString();
                    subcontractor.OfficePhone = dataReader["OfficePhone"].ToString();
                    result.Add(subcontractor);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;            
        }
    }

    public static class SubcontractorSchedulingDAL
    {
        public static SubcontractorScheduling Create(SubcontractorScheduling subcontractorScheduling)
        {
            SubcontractorScheduling result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorSchedulingCreate";
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", (int)subcontractorScheduling.SubcontractorID); 
            DataAccessHelper.AddWithValue(command, "@EMail", subcontractorScheduling.EMail);
            DataAccessHelper.AddWithValue(command, "@PrimaryPhone", subcontractorScheduling.PrimaryPhone);
            DataAccessHelper.AddWithValue(command, "@BackupPhone1", subcontractorScheduling.BackupPhone1);
            DataAccessHelper.AddWithValue(command, "@BackupPhone2", subcontractorScheduling.BackupPhone2);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new SubcontractorScheduling();
                result.SubcontractorID = subcontractorScheduling.SubcontractorID;
                result.EMail = subcontractorScheduling.EMail;
                result.PrimaryPhone = subcontractorScheduling.PrimaryPhone;
                result.BackupPhone1 = subcontractorScheduling.BackupPhone1;
                result.BackupPhone2 = subcontractorScheduling.BackupPhone2;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static SubcontractorScheduling Read(object subcontractorID)
        {
            SubcontractorScheduling result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorSchedulingRead";
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", subcontractorID);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new SubcontractorScheduling();
                    result.SubcontractorID = (int)dataReader["SubcontractorID"];
                    result.EMail = dataReader["EMail"].ToString();
                    result.PrimaryPhone = dataReader["PrimaryPhone"].ToString();
                    result.BackupPhone1 = dataReader["BackupPhone1"].ToString();
                    result.BackupPhone2 = dataReader["BackupPhone2"].ToString();
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void Update(SubcontractorScheduling subcontractorScheduling)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorSchedulingUpdate";
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", (int)subcontractorScheduling.SubcontractorID);
            DataAccessHelper.AddWithValue(command, "@EMail", subcontractorScheduling.EMail);
            DataAccessHelper.AddWithValue(command, "@PrimaryPhone", subcontractorScheduling.PrimaryPhone);
            DataAccessHelper.AddWithValue(command, "@BackupPhone1", subcontractorScheduling.BackupPhone1);
            DataAccessHelper.AddWithValue(command, "@BackupPhone2", subcontractorScheduling.BackupPhone2);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Delete(SubcontractorScheduling subcontractorScheduling)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorSchedulingDelete";
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", subcontractorScheduling.SubcontractorID);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }
    }

    public static class SubcontractorCommunicationDAL
    {
        public static SubcontractorCommunication Create(SubcontractorCommunication subcontractorCommunication)
        {
            SubcontractorCommunication result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorCommunicationCreate";
            SqlParameter subCommunicationID = DataAccessHelper.AddParameter(command, "@SubcontractorCommunicationID", System.Data.SqlDbType.Int, null, System.Data.ParameterDirection.Output);
            SqlParameter created = DataAccessHelper.AddParameter(command, "@Created", System.Data.SqlDbType.DateTime, null, System.Data.ParameterDirection.Output);
            DataAccessHelper.AddWithValue(command, "@BuilderID", subcontractorCommunication.BuilderID);
            DataAccessHelper.AddWithValue(command, "@Channel", subcontractorCommunication.Channel);
            DataAccessHelper.AddWithValue(command, "@EMailID", subcontractorCommunication.EMailID);
            DataAccessHelper.AddWithValue(command, "@EmployeeCommunicatorID", subcontractorCommunication.EmployeeCommunicatorID);
            DataAccessHelper.AddWithValue(command, "@JobTaskID", subcontractorCommunication.JobTaskID);
            DataAccessHelper.AddWithValue(command, "@Method", subcontractorCommunication.Method.ToString());
            DataAccessHelper.AddWithValue(command, "@Notes", subcontractorCommunication.Notes);
            DataAccessHelper.AddWithValue(command, "@Reason", subcontractorCommunication.Reason.ToString());
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", subcontractorCommunication.SubcontractorID);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new SubcontractorCommunication();
                subcontractorCommunication.CopyTo(result);
                result.Identifier = subCommunicationID.Value;
                result.Created = (DateTime)created.Value;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static List<SubcontractorCommunication> SearchJob(object jobID)
        {
            List<SubcontractorCommunication> result = new List<SubcontractorCommunication>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorCommunicationSearchJob";
            DataAccessHelper.AddWithValue(command, "@JobID", int.Parse(jobID.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    SubcontractorCommunication subcontractorCommunication = new SubcontractorCommunication();
                    subcontractorCommunication.BuilderID = DataAccessHelper.FieldIntValue(dataReader, "BuilderID");
                    subcontractorCommunication.Channel = DataAccessHelper.FieldStringValue(dataReader, "Channel");
                    subcontractorCommunication.Created = DataAccessHelper.FieldDateTimeValue(dataReader, "Created").Value;
                    subcontractorCommunication.EMailID = DataAccessHelper.FieldIntValue(dataReader, "EMailID");
                    subcontractorCommunication.EmployeeCommunicatorID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeCommunicatorID");
                    subcontractorCommunication.Identifier = DataAccessHelper.FieldIntValue(dataReader, "SubcontractorCommunicationID");
                    subcontractorCommunication.JobTaskID = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID");
                    subcontractorCommunication.Method = (CommunicationMethodEnum)Enum.Parse(typeof(CommunicationMethodEnum), DataAccessHelper.FieldStringValue(dataReader, "Method"));
                    subcontractorCommunication.Notes = DataAccessHelper.FieldStringValue(dataReader, "Notes");
                    subcontractorCommunication.Reason = (CommunicationReasonEnum)Enum.Parse(typeof(CommunicationReasonEnum), DataAccessHelper.FieldStringValue(dataReader, "Reason"));
                    subcontractorCommunication.SubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "SubcontractorID");
                    result.Add(subcontractorCommunication);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static List<SubcontractorCommunication> SearchSubcontractorCommunication(object subcontractorID, DateTime start, DateTime end)
        {
            List<SubcontractorCommunication> result = new List<SubcontractorCommunication>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorCommunicationSearchSubcontractor";
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", subcontractorID);
            DataAccessHelper.AddWithValue(command, "@StartDate", start);
            DataAccessHelper.AddWithValue(command, "@EndDate", end);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    SubcontractorCommunication subcontractorCommunication = new SubcontractorCommunication();
                    subcontractorCommunication.BuilderID = DataAccessHelper.FieldIntValue(dataReader, "BuilderID");
                    subcontractorCommunication.Channel = DataAccessHelper.FieldStringValue(dataReader, "Channel");
                    subcontractorCommunication.Created = DataAccessHelper.FieldDateTimeValue(dataReader, "Created").Value;
                    subcontractorCommunication.EMailID = DataAccessHelper.FieldIntValue(dataReader, "EMailID");
                    subcontractorCommunication.EmployeeCommunicatorID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeCommunicatorID");
                    subcontractorCommunication.Identifier = DataAccessHelper.FieldIntValue(dataReader, "SubcontractorCommunicationID");
                    subcontractorCommunication.JobTaskID = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID");
                    subcontractorCommunication.Method = (CommunicationMethodEnum)Enum.Parse(typeof(CommunicationMethodEnum), DataAccessHelper.FieldStringValue(dataReader, "Method"));
                    subcontractorCommunication.Notes = DataAccessHelper.FieldStringValue(dataReader, "Notes");
                    subcontractorCommunication.Reason = (CommunicationReasonEnum)Enum.Parse(typeof(CommunicationReasonEnum), DataAccessHelper.FieldStringValue(dataReader, "Reason"));
                    subcontractorCommunication.SubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "SubcontractorID");
                    result.Add(subcontractorCommunication);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }                       

        public static List<SubcontractorCommunication> Search(object jobTaskID)
        {
            List<SubcontractorCommunication> result = new List<SubcontractorCommunication>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubcontractorCommunicationSearchJobTask";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", jobTaskID);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    SubcontractorCommunication subcontractorCommunication = new SubcontractorCommunication();
                    subcontractorCommunication.BuilderID = DataAccessHelper.FieldIntValue(dataReader, "BuilderID");
                    subcontractorCommunication.Channel = DataAccessHelper.FieldStringValue(dataReader, "Channel");
                    subcontractorCommunication.Created = DataAccessHelper.FieldDateTimeValue(dataReader, "Created").Value;
                    subcontractorCommunication.EMailID = DataAccessHelper.FieldIntValue(dataReader, "EMailID");
                    subcontractorCommunication.EmployeeCommunicatorID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeCommunicatorID");
                    subcontractorCommunication.Identifier = DataAccessHelper.FieldIntValue(dataReader, "SubcontractorCommunicationID");
                    subcontractorCommunication.JobTaskID = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID");
                    subcontractorCommunication.Method = (CommunicationMethodEnum)Enum.Parse(typeof(CommunicationMethodEnum), DataAccessHelper.FieldStringValue(dataReader, "Method"));
                    subcontractorCommunication.Notes = DataAccessHelper.FieldStringValue(dataReader, "Notes");
                    subcontractorCommunication.Reason = (CommunicationReasonEnum)Enum.Parse(typeof(CommunicationReasonEnum), DataAccessHelper.FieldStringValue(dataReader, "Reason"));
                    subcontractorCommunication.SubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "SubcontractorID");
                    result.Add(subcontractorCommunication);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
    }
}
