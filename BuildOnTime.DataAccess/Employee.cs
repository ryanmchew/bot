﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuildOnTime.BusinessObject;
using System.Data.SqlClient;
using System.Configuration;

namespace BuildOnTime.DataAccess
{
    public static class EmployeeDAL
    {
        public static string RolesToString(List<Role> roles)
        {
            string result = "";
            if (roles.Count > 0)
            {
                result = roles[0].ToString();
                for (int i = 1; i < roles.Count; i++)
                {
                    result += ";" + roles[i].ToString();
                }
            }
            return result;
        }

        public static List<Role> StringToRoles(string roles)
        {
            List<Role> result = new List<Role>();
            string[] roleArray = roles.Split(';');
            foreach(string role in roleArray)
            {
                Role addRole = (Role)Enum.Parse(typeof(Role), role);
                result.Add(addRole);
            }
            return result;
        }

        public static Employee Create(Employee employee, string password)
        {
            // We have to create a system user first.
            if (employee.SystemUser != null)
            {
                SystemUser newSystemUser = new SystemUser(null, employee.Email, employee.Email, employee.Roles);
                SystemUser systemUser = SystemUserDAL.Create(newSystemUser, password);
                employee.SystemUser = systemUser;
            }
            Employee result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEmployeeCreate";
            SqlParameter employeeID = DataAccessHelper.AddParameter(command, "@EmployeeID", System.Data.SqlDbType.Int, null, System.Data.ParameterDirection.Output);
            SqlParameter updated = DataAccessHelper.AddParameter(command, "@Updated", System.Data.SqlDbType.DateTime, null, System.Data.ParameterDirection.Output);
            DataAccessHelper.AddWithValue(command, "@BuilderID", employee.Builder.Identifier);
            if (employee.SystemUser != null)
            {
                DataAccessHelper.AddWithValue(command, "@SystemUserID", employee.SystemUser.Identifier);
            }
            else
            {
                DataAccessHelper.AddWithValue(command, "@SystemUserID", null);
            }
            if (employee.Supervisor != null)
            {
                DataAccessHelper.AddWithValue(command, "@SupervisorID", employee.Supervisor.Identifier);
            }
            else
            {
                DataAccessHelper.AddWithValue(command, "@SupervisorID", null);
            }
            DataAccessHelper.AddWithValue(command, "@Roles", RolesToString(employee.Roles));
            DataAccessHelper.AddWithValue(command, "@CellPhone", employee.CellPhone);
            DataAccessHelper.AddWithValue(command, "@Fax", employee.Fax);
            DataAccessHelper.AddWithValue(command, "@FirstName", employee.FirstName);
            DataAccessHelper.AddWithValue(command, "@HomePhone", employee.HomePhone);
            DataAccessHelper.AddWithValue(command, "@LastName", employee.LastName);
            DataAccessHelper.AddWithValue(command, "@WorkPhone", employee.WorkPhone);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new Employee();
                result.Builder = employee.Builder;
                result.CellPhone = employee.CellPhone;
                result.Email = employee.Email;
                result.Fax = employee.Fax;
                result.FirstName = employee.FirstName;
                result.HomePhone = employee.HomePhone;
                result.Identifier = DataAccessHelper.ParamIntValue(employeeID);
                result.LastName = employee.LastName;
                result.Supervisor = employee.Supervisor;
                result.SystemUser = employee.SystemUser;
                result.Updated = DataAccessHelper.ParamDateTimeValue(updated);
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static Employee Read(object identifier)
        {
            Employee user = new Employee();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEmployeeRead";
            DataAccessHelper.AddWithValue(command, "@EmployeeID", identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    user.Builder = BuilderDAL.Read(DataAccessHelper.FieldIntValue(dataReader, "BuilderID"));
                    user.CellPhone = DataAccessHelper.FieldStringValue(dataReader, "CellPhone");                    
                    user.Fax = DataAccessHelper.FieldStringValue(dataReader, "Fax");
                    user.FirstName = DataAccessHelper.FieldStringValue(dataReader, "FirstName");
                    user.HomePhone = DataAccessHelper.FieldStringValue(dataReader, "HomePhone");
                    user.Identifier = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID");
                    user.LastName = DataAccessHelper.FieldStringValue(dataReader, "LastName");
                    user.Roles = StringToRoles(DataAccessHelper.FieldStringValue(dataReader, "Roles"));
                    int? supervisorID = DataAccessHelper.FieldIntValue(dataReader, "SupervisorID");
                    if (supervisorID.HasValue)
                    {
                        user.Supervisor = EmployeeDAL.Read(supervisorID.Value);
                    }
                    if (DataAccessHelper.FieldGuidValue(dataReader, "SystemUserID").HasValue)
                    {
                        user.SystemUser = SystemUserDAL.Read(DataAccessHelper.FieldGuidValue(dataReader, "SystemUserID").Value);
                    }
                    if (user.SystemUser != null)
                    {
                        user.Email = user.SystemUser.EMail;
                    }
                    user.WorkPhone = DataAccessHelper.FieldStringValue(dataReader, "WorkPhone");                    
                }
            }
            finally
            {
                connection.Close();
            }
            return user;
        }

        public static Employee Update(Employee employee)
        {
            Employee result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEmployeeUpdate";
            DataAccessHelper.AddWithValue(command, "@EmployeeID", employee.Identifier);
            SqlParameter updated = DataAccessHelper.AddParameter(command, "@Updated", System.Data.SqlDbType.DateTime, null, System.Data.ParameterDirection.Output);
            DataAccessHelper.AddWithValue(command, "@BuilderID", employee.Builder.Identifier);
            if (employee.SystemUser != null)
            {
                DataAccessHelper.AddWithValue(command, "@SystemUserID", employee.SystemUser.Identifier);
            }
            if (employee.Supervisor != null)
            {
                DataAccessHelper.AddWithValue(command, "@SupervisorID", employee.Supervisor.Identifier);
            }
            DataAccessHelper.AddWithValue(command, "@Roles", RolesToString(employee.Roles));
            DataAccessHelper.AddWithValue(command, "@CellPhone", employee.CellPhone);
            DataAccessHelper.AddWithValue(command, "@Fax", employee.Fax);
            DataAccessHelper.AddWithValue(command, "@FirstName", employee.FirstName);
            DataAccessHelper.AddWithValue(command, "@HomePhone", employee.HomePhone);
            DataAccessHelper.AddWithValue(command, "@LastName", employee.LastName);
            DataAccessHelper.AddWithValue(command, "@WorkPhone", employee.WorkPhone);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new Employee();
                result.Builder = employee.Builder;
                result.CellPhone = employee.CellPhone;
                result.Email = employee.Email;
                result.Fax = employee.Fax;
                result.FirstName = employee.FirstName;
                result.HomePhone = employee.HomePhone;
                result.Identifier = employee.Identifier;
                result.LastName = employee.LastName;
                result.Supervisor = employee.Supervisor;
                result.SystemUser = employee.SystemUser;
                result.Updated = DataAccessHelper.ParamDateTimeValue(updated);
            }
            finally
            {
                connection.Close();
            }
            return result;
        }


        public static void Delete(Employee employee)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEmployeeDelete";
            DataAccessHelper.AddWithValue(command, "@EmployeeID", employee.Identifier);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static Employee ReadBySystemUser(object identifier)
        {
            Employee user = new Employee();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEmployeeReadBySystemUserID";
            DataAccessHelper.AddWithValue(command, "@SystemUserID", identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    user.Builder = BuilderDAL.Read(DataAccessHelper.FieldIntValue(dataReader, "BuilderID"));
                    user.CellPhone = DataAccessHelper.FieldStringValue(dataReader, "CellPhone");
                    user.Fax = DataAccessHelper.FieldStringValue(dataReader, "Fax");
                    user.FirstName = DataAccessHelper.FieldStringValue(dataReader, "FirstName");
                    user.HomePhone = DataAccessHelper.FieldStringValue(dataReader, "HomePhone");
                    user.Identifier = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID");
                    user.LastName = DataAccessHelper.FieldStringValue(dataReader, "LastName");
                    user.Roles = StringToRoles(DataAccessHelper.FieldStringValue(dataReader, "Roles"));
                    int? supervisorID = DataAccessHelper.FieldIntValue(dataReader, "SupervisorID");
                    if (supervisorID.HasValue)
                    {
                        user.Supervisor = EmployeeDAL.Read(supervisorID.Value);
                    }
                    if (DataAccessHelper.FieldGuidValue(dataReader, "SystemUserID").HasValue)
                    {
                        user.SystemUser = SystemUserDAL.Read(DataAccessHelper.FieldGuidValue(dataReader, "SystemUserID").Value);
                    }
                    if (user.SystemUser != null)
                    {
                        user.Email = user.SystemUser.EMail;
                    }
                    user.WorkPhone = DataAccessHelper.FieldStringValue(dataReader, "WorkPhone");
                }
            }
            finally
            {
                connection.Close();
            }
            return user;            
        }

        public static List<Employee> List(Builder builder)
        {
            List<Employee> employeeList = new List<Employee>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEmployeeList";
            DataAccessHelper.AddWithValue(command, "@BuilderID", builder.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Employee employee = new Employee();
                    employee.Builder = builder;
                    employee.CellPhone = DataAccessHelper.FieldStringValue(dataReader, "CellPhone");
                    employee.Fax = DataAccessHelper.FieldStringValue(dataReader, "Fax");
                    employee.FirstName = DataAccessHelper.FieldStringValue(dataReader, "FirstName");
                    employee.HomePhone = DataAccessHelper.FieldStringValue(dataReader, "HomePhone");
                    employee.Identifier = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID");
                    employee.LastName = DataAccessHelper.FieldStringValue(dataReader, "LastName");
                    employee.Roles = StringToRoles(DataAccessHelper.FieldStringValue(dataReader, "Roles"));
                    int? supervisorID = DataAccessHelper.FieldIntValue(dataReader, "SupervisorID");
                    if (supervisorID.HasValue)
                    {
                        employee.Supervisor = EmployeeDAL.Read(supervisorID);
                    }
                    if (DataAccessHelper.FieldGuidValue(dataReader, "SystemUserID").HasValue)
                    {
                        employee.SystemUser = SystemUserDAL.Read(DataAccessHelper.FieldGuidValue(dataReader, "SystemUserID").Value);
                    }
                    if (employee.SystemUser != null)
                    {
                        employee.Email = employee.SystemUser.EMail;
                    }

                    employee.WorkPhone = DataAccessHelper.FieldStringValue(dataReader, "WorkPhone");
                    employeeList.Add(employee);
                }
            }
            finally
            {
                connection.Close();
            }
            return employeeList;
        }
    }

    public static class EmployeeCommunicationDAL
    {
        public static EmployeeCommunication Create(EmployeeCommunication employeeCommunication)
        {
            EmployeeCommunication result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEmployeeCommunicationCreate";
            SqlParameter subCommunicationID = DataAccessHelper.AddParameter(command, "@EmployeeCommunicationID", System.Data.SqlDbType.Int, null, System.Data.ParameterDirection.Output);
            SqlParameter created = DataAccessHelper.AddParameter(command, "@Created", System.Data.SqlDbType.DateTime, null, System.Data.ParameterDirection.Output);
            DataAccessHelper.AddWithValue(command, "@BuilderID", employeeCommunication.BuilderID);
            DataAccessHelper.AddWithValue(command, "@Channel", employeeCommunication.Channel);
            DataAccessHelper.AddWithValue(command, "@EMailID", employeeCommunication.EMailID);
            DataAccessHelper.AddWithValue(command, "@EmployeeCommunicatorID", employeeCommunication.EmployeeCommunicatorID);
            DataAccessHelper.AddWithValue(command, "@JobTaskID", employeeCommunication.JobTaskID);
            DataAccessHelper.AddWithValue(command, "@Method", employeeCommunication.Method.ToString());
            DataAccessHelper.AddWithValue(command, "@Notes", employeeCommunication.Notes);
            DataAccessHelper.AddWithValue(command, "@Reason", employeeCommunication.Reason.ToString());
            DataAccessHelper.AddWithValue(command, "@EmployeeID", employeeCommunication.EmployeeID);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new EmployeeCommunication();
                employeeCommunication.CopyTo(result);
                result.Identifier = subCommunicationID.Value;
                result.Created = (DateTime)created.Value;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static List<EmployeeCommunication> SearchJob(object jobID)
        {
            List<EmployeeCommunication> result = new List<EmployeeCommunication>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEmployeeCommunicationSearchJob";
            DataAccessHelper.AddWithValue(command, "@JobID", jobID);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    EmployeeCommunication employeeCommunication = new EmployeeCommunication();
                    employeeCommunication.BuilderID = DataAccessHelper.FieldIntValue(dataReader, "BuilderID");
                    employeeCommunication.Channel = DataAccessHelper.FieldStringValue(dataReader, "Channel");
                    employeeCommunication.Created = DataAccessHelper.FieldDateTimeValue(dataReader, "Created").Value;
                    employeeCommunication.EMailID = DataAccessHelper.FieldIntValue(dataReader, "EMailID");
                    employeeCommunication.EmployeeCommunicatorID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeCommunicatorID");
                    employeeCommunication.Identifier = DataAccessHelper.FieldIntValue(dataReader, "EmployeeCommunicationID");
                    employeeCommunication.JobTaskID = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID");
                    employeeCommunication.Method = (CommunicationMethodEnum)Enum.Parse(typeof(CommunicationMethodEnum), DataAccessHelper.FieldStringValue(dataReader, "Method"));
                    employeeCommunication.Notes = DataAccessHelper.FieldStringValue(dataReader, "Notes");
                    employeeCommunication.Reason = (CommunicationReasonEnum)Enum.Parse(typeof(CommunicationReasonEnum), DataAccessHelper.FieldStringValue(dataReader, "Reason"));
                    employeeCommunication.EmployeeID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID");
                    result.Add(employeeCommunication);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static List<EmployeeCommunication> Search(object jobTaskID)
        {
            List<EmployeeCommunication> result = new List<EmployeeCommunication>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEmployeeCommunicationSearchJobTask";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", jobTaskID);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    EmployeeCommunication employeeCommunication = new EmployeeCommunication();
                    employeeCommunication.BuilderID = DataAccessHelper.FieldIntValue(dataReader, "BuilderID");
                    employeeCommunication.Channel = DataAccessHelper.FieldStringValue(dataReader, "Channel");
                    employeeCommunication.Created = DataAccessHelper.FieldDateTimeValue(dataReader, "Created").Value;
                    employeeCommunication.EMailID = DataAccessHelper.FieldIntValue(dataReader, "EMailID");
                    employeeCommunication.EmployeeCommunicatorID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeCommunicatorID");
                    employeeCommunication.Identifier = DataAccessHelper.FieldIntValue(dataReader, "EmployeeCommunicationID");
                    employeeCommunication.JobTaskID = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID");
                    employeeCommunication.Method = (CommunicationMethodEnum)Enum.Parse(typeof(CommunicationMethodEnum), DataAccessHelper.FieldStringValue(dataReader, "Method"));
                    employeeCommunication.Notes = DataAccessHelper.FieldStringValue(dataReader, "Notes");
                    employeeCommunication.Reason = (CommunicationReasonEnum)Enum.Parse(typeof(CommunicationReasonEnum), DataAccessHelper.FieldStringValue(dataReader, "Reason"));
                    employeeCommunication.EmployeeID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID");
                    result.Add(employeeCommunication);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
    }
}
