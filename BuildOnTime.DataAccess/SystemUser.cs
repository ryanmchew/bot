﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using BuildOnTime.BusinessObject;
using System.Data.SqlClient;
using System.Configuration;

namespace BuildOnTime.DataAccess
{
    public static class SystemUserDAL
    {
        public static void ChangePassword(SystemUser systemUser, string oldPassword, string newPassword)
        {
            MembershipUser user = Membership.GetUser(systemUser.Username);
            user.ChangePassword(oldPassword, newPassword);
        }

        public static string ResetPassword(string username)
        {
            MembershipUser user = Membership.GetUser(username);
            if (user.IsLockedOut)
            {
                user.UnlockUser();
            }
            return user.ResetPassword();
        }

        public static SystemUser Create(SystemUser systemUser, string password)
        {
            SystemUser result = new SystemUser();            
            MembershipUser member = Membership.CreateUser(systemUser.Username, password, systemUser.EMail);
            foreach(Role role in systemUser.Roles)
            {
                Roles.AddUserToRole(member.UserName, role.ToString());
            }
            result.EMail = systemUser.EMail;
            result.Identifier = member.ProviderUserKey;
            result.LastLogin = member.LastLoginDate;
            result.Username = systemUser.Username;
            return result;
        }

        private static List<Role> GetRoles(MembershipUser member)
        {
            List<Role> result = new List<Role>();
            string[] userRoles = Roles.GetRolesForUser(member.UserName);
            foreach (string role in userRoles)
            {
                Role userRole = (Role)Enum.Parse(typeof(Role), role);
                result.Add(userRole);
            }
            return result;
        }

        private static SystemUser Generate(MembershipUser member)
        {
            SystemUser result = null;
            if (member != null)
            {
                result = new SystemUser();
                result.EMail = member.Email;
                result.Identifier = member.ProviderUserKey;
                result.LastLogin = member.LastLoginDate;
                result.Roles = GetRoles(member);
                result.Username = member.UserName;
            }
            return result;
        }

        public static SystemUser Read(object identifier)
        {
            return Generate(Membership.GetUser(identifier));
        }

        public static SystemUser Read(string username)
        {
            return Generate(Membership.GetUser(username));
        }

        public static SystemUser ReadByEMail(string email)
        {
            SystemUser result = null;
            string userName = Membership.GetUserNameByEmail(email);
            if ((userName != null) && (userName.Length > 0))
            {
                result = Read(userName);
            }
            return result;
        }

        public static List<string> ReadUserRoles(SystemUser user)
        {
            return new List<string>(Roles.GetRolesForUser(user.Username));
        }

        public static int RequiredPasswordLength()
        {
            return Membership.MinRequiredPasswordLength;
        }

        private static bool UserPasswordChangeRead(object identity)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Authentication"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalMembershipForcePasswordChangeRead";
            DataAccessHelper.AddWithValue(command, "@Identifier", identity);
            SqlParameter passwordChange = DataAccessHelper.AddParameter(command, "@ForcePasswordChange", System.Data.SqlDbType.Bit, null, System.Data.ParameterDirection.Output);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
            return (bool)passwordChange.Value;
        }

        public static bool Login(string username, string password, out SystemUser systemUser)
        {
            bool validated = false;
            systemUser = null;
            // This sets the user LastActivityDate and make the user appear online.
            validated = Membership.ValidateUser(username, password);
            if (validated)
            {
                MembershipUser user = Membership.GetUser(username);
                if (UserPasswordChangeRead(user.ProviderUserKey))
                {
                    throw new UserPasswordChange("Password needs to be updated.");
                }
                systemUser = SystemUserDAL.Read(user.ProviderUserKey);
            }
            return validated;
        }

        public static void AssociateUserSubcontractor(SystemUser user, object subcontractorID)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSystemUserSubcontractorAssociate";
            DataAccessHelper.AddWithValue(command, "@SystemUserIdentifier", user.Identifier);
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", subcontractorID);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void UserPasswordChangeUpdate(SystemUser user, bool forcePasswordChange)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Authentication"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalMembershipForcePasswordChangeUpdate";
            DataAccessHelper.AddWithValue(command, "@Identifier", user.Identifier);
            DataAccessHelper.AddWithValue(command, "@ForcePasswordChange", forcePasswordChange);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<SystemUser> SearchBySubcontractor(object subcontractorID)
        {
            List<SystemUser> result = new List<SystemUser>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSystemUserSearchSubcontractor";
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", subcontractorID);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    SystemUser systemUser = new SystemUser();
                    systemUser.EMail = DataAccessHelper.FieldStringValue(dataReader, "Email");
                    systemUser.Identifier = DataAccessHelper.FieldGuidValue(dataReader, "SystemUserID");
                    systemUser.LastLogin = DataAccessHelper.FieldDateTimeValue(dataReader, "LastLoginDate");
                    MembershipUser member = Membership.GetUser(systemUser.Identifier);
                    if (member != null)
                    {
                        systemUser.Username = member.UserName;
                        systemUser.Roles = GetRoles(member);
                    }
                    result.Add(systemUser);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
    }

    public static class SystemUserAuditDAL
    {
        public static SystemUserAudit Create(object systemUserID, AuditEnum action, AuditTargetEnum target, CatalystEnum catalyst, string details)
        {
            SystemUserAudit result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalAuditLogCreate";
            SqlParameter auditLogID = command.Parameters.Add("@AuditLogID", System.Data.SqlDbType.Int);
            auditLogID.Direction = System.Data.ParameterDirection.Output;
            SqlParameter occurred = command.Parameters.Add("@Occurred", System.Data.SqlDbType.DateTime);
            occurred.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@SystemUserID", systemUserID);
            DataAccessHelper.AddWithValue(command, "@Action", action.ToString());
            DataAccessHelper.AddWithValue(command, "@Target", target.ToString());
            DataAccessHelper.AddWithValue(command, "@Catalyst", catalyst.ToString());
            DataAccessHelper.AddWithValue(command, "@Details", details);  
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new SystemUserAudit();
                result.Identifier = (int)auditLogID.Value;
                result.Occurred = (DateTime)occurred.Value;
                result.Action = action;
                result.Catalyst = catalyst;
                result.CatalystDetails = details;
                result.SystemUserID = systemUserID;
                result.Target = target;
            }
            finally
            {
                connection.Close();
            }

            return result;     
        }


        public static SystemUserAudit Read(object identifier)
        {
            SystemUserAudit result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalAuditLogRead";
            DataAccessHelper.AddWithValue(command, "@AuditLogID", int.Parse(identifier.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new SystemUserAudit();
                    result.Identifier = DataAccessHelper.FieldIntValue(dataReader, "AuditLogID");
                    result.Action = (AuditEnum)Enum.Parse(typeof(AuditEnum), DataAccessHelper.FieldStringValue(dataReader, "Action"));
                    result.Catalyst = (CatalystEnum)Enum.Parse(typeof(CatalystEnum), DataAccessHelper.FieldStringValue(dataReader, "Catalyst"));
                    result.CatalystDetails = DataAccessHelper.FieldStringValue(dataReader, "Details");
                    result.Occurred = DataAccessHelper.FieldDateTimeValue(dataReader, "Occurred").Value;
                    result.SystemUserID = DataAccessHelper.FieldGuidValue(dataReader, "SystemUserID");
                    result.Target = (AuditTargetEnum)Enum.Parse(typeof(AuditTargetEnum), DataAccessHelper.FieldStringValue(dataReader, "Target"));
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }


        public static void Delete(object identifier)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalAuditLogDelete";
            DataAccessHelper.AddWithValue(command, "@AuditLogID", int.Parse(identifier.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<SystemUserAudit> FindRecent(object systemUserID, AuditEnum action, AuditTargetEnum target)
        {
            List<SystemUserAudit> result = new List<SystemUserAudit>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalAuditLogSearch";
            DataAccessHelper.AddWithValue(command, "@SystemUserID", systemUserID);
            DataAccessHelper.AddWithValue(command, "@Action", action.ToString());
            DataAccessHelper.AddWithValue(command, "@Target", target.ToString());
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    SystemUserAudit audit = new SystemUserAudit();
                    audit.Identifier = DataAccessHelper.FieldIntValue(dataReader, "AuditLogID");
                    audit.Action = (AuditEnum)Enum.Parse(typeof(AuditEnum), DataAccessHelper.FieldStringValue(dataReader, "Action"));
                    audit.Catalyst = (CatalystEnum)Enum.Parse(typeof(CatalystEnum), DataAccessHelper.FieldStringValue(dataReader, "Catalyst"));
                    audit.CatalystDetails = DataAccessHelper.FieldStringValue(dataReader, "Details");
                    audit.Occurred = DataAccessHelper.FieldDateTimeValue(dataReader, "Occurred").Value;
                    audit.SystemUserID = DataAccessHelper.FieldGuidValue(dataReader, "SystemUserID");
                    audit.Target = (AuditTargetEnum)Enum.Parse(typeof(AuditTargetEnum), DataAccessHelper.FieldStringValue(dataReader, "Target"));
                    result.Add(audit);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
    }
}
