﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuildOnTime.BusinessObject;
using System.Data.SqlClient;
using System.Configuration;

namespace BuildOnTime.DataAccess
{
    public static class BuilderDAL
    {
        public static Builder Create(Builder builder)
        {
            Builder result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalBuilderCreate";
            SqlParameter builderID = command.Parameters.Add("@BuilderID", System.Data.SqlDbType.Int);
            builderID.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@Name", builder.Name);
            DataAccessHelper.AddWithValue(command, "@AddressLine1", builder.AddressLine1);
            DataAccessHelper.AddWithValue(command, "@AddressLine2", DBNull.Value);
            DataAccessHelper.AddWithValue(command, "@City", builder.City);
            DataAccessHelper.AddWithValue(command, "@State", builder.State);
            DataAccessHelper.AddWithValue(command, "@PostalCode", builder.PostalCode);
            DataAccessHelper.AddWithValue(command, "@OfficePhone", builder.OfficePhone);
            DataAccessHelper.AddWithValue(command, "@OfficeFax", builder.OfficeFax);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new Builder();
                result.Identifier = (int)builderID.Value;
                result.AddressLine1 = builder.AddressLine1;
                result.AddressLine2 = builder.AddressLine2;
                result.Name = builder.Name;
                result.PostalCode = builder.PostalCode;
                result.State = builder.State;
                result.OfficePhone = builder.OfficePhone;
                result.OfficeFax = builder.OfficeFax;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static Builder Read(object identifier)
        {
            Builder result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalBuilderRead";
            DataAccessHelper.AddWithValue(command, "@BuilderID", identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new Builder();
                    result.Identifier = (int)dataReader["BuilderID"];
                    result.AddressLine1 = dataReader["AddressLine1"].ToString();
                    result.AddressLine2 = dataReader["AddressLine2"].ToString();
                    result.City = dataReader["City"].ToString();
                    result.Name = dataReader["Name"].ToString();
                    result.PostalCode = dataReader["PostalCode"].ToString();
                    result.State = dataReader["State"].ToString();
                    result.OfficeFax = dataReader["OfficeFax"].ToString();
                    result.OfficePhone = dataReader["OfficePhone"].ToString();
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static Builder Read(SystemUser user)
        {
            Builder result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalBuilderReadUser";
            DataAccessHelper.AddWithValue(command, "@SystemUserID", user.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new Builder();
                    result.Identifier = (int)dataReader["BuilderID"];
                    result.AddressLine1 = dataReader["AddressLine1"].ToString();
                    result.AddressLine2 = dataReader["AddressLine2"].ToString();
                    result.City = dataReader["City"].ToString();
                    result.Name = dataReader["Name"].ToString();
                    result.PostalCode = dataReader["PostalCode"].ToString();
                    result.State = dataReader["State"].ToString();
                    result.OfficeFax = dataReader["OfficeFax"].ToString();
                    result.OfficePhone = dataReader["OfficePhone"].ToString();
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static Builder ReadByName(string name)
        {
            Builder result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalBuilderReadName";
            DataAccessHelper.AddWithValue(command, "@Name", name);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new Builder();
                    result.Identifier = (int)dataReader["BuilderID"];
                    result.AddressLine1 = dataReader["AddressLine1"].ToString();
                    result.AddressLine2 = dataReader["AddressLine2"].ToString();
                    result.City = dataReader["City"].ToString();
                    result.Name = dataReader["Name"].ToString();
                    result.PostalCode = dataReader["PostalCode"].ToString();
                    result.State = dataReader["State"].ToString();
                    result.OfficeFax = dataReader["OfficeFax"].ToString();
                    result.OfficePhone = dataReader["OfficePhone"].ToString();
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void Update(Builder builder)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalBuilderUpdate";
            DataAccessHelper.AddWithValue(command, "@BuilderID", (int)builder.Identifier);
            DataAccessHelper.AddWithValue(command, "@Name", builder.Name);
            DataAccessHelper.AddWithValue(command, "@AddressLine1", builder.AddressLine1);
            DataAccessHelper.AddWithValue(command, "@AddressLine2", DBNull.Value);
            DataAccessHelper.AddWithValue(command, "@City", builder.City);
            DataAccessHelper.AddWithValue(command, "@State", builder.State);
            DataAccessHelper.AddWithValue(command, "@PostalCode", builder.PostalCode);
            DataAccessHelper.AddWithValue(command, "@OfficePhone", builder.OfficePhone);
            DataAccessHelper.AddWithValue(command, "@OfficeFax", builder.OfficeFax);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Delete(Builder builder)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalBuilderDelete";
            DataAccessHelper.AddWithValue(command, "@BuilderID", builder.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<Builder> List()
        {
            List<Builder> result = new List<Builder>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalBuilderList";
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Builder builder = new Builder();
                    builder.Identifier = (int)dataReader["BuilderID"];
                    builder.AddressLine1 = dataReader["AddressLine1"].ToString();
                    builder.AddressLine2 = dataReader["AddressLine2"].ToString();
                    builder.City = dataReader["City"].ToString();
                    builder.Name = dataReader["Name"].ToString();
                    builder.PostalCode = dataReader["PostalCode"].ToString();
                    builder.State = dataReader["State"].ToString();
                    builder.OfficeFax = dataReader["OfficeFax"].ToString();
                    builder.OfficePhone = dataReader["OfficePhone"].ToString();
                    result.Add(builder);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
    }
}
