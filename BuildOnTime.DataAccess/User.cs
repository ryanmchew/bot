﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using BuildOnTime.BusinessObject;
using System.Data.SqlClient;
using System.Configuration;

namespace BuildOnTime.DataAccess
{
    public static class UserDAL
    {
        /*
                public static class BuilderDAL
                {
                    public static UserBuilder CreateUserBuilder(UserBuilder user)
                    {
                        UserBuilder result = null;
                        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
                        SqlCommand command = new SqlCommand();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "dalBuilderUserCreate";
                        DataAccessHelper.AddWithValue(command, "@Identifier", user.Identifier);
                        DataAccessHelper.AddWithValue(command, "@BuilderID", user.BuilderID);
                        DataAccessHelper.AddWithValue(command, "@CellPhone", user.CellPhone);
                        DataAccessHelper.AddWithValue(command, "@Fax", user.Fax);
                        DataAccessHelper.AddWithValue(command, "@FirstName", user.FirstName);
                        DataAccessHelper.AddWithValue(command, "@HomePhone", user.HomePhone);
                        DataAccessHelper.AddWithValue(command, "@LastName", user.LastName);
                        command.Connection = connection;
                        connection.Open();
                        try
                        {
                            command.ExecuteNonQuery();
                            result = new UserBuilder();
                            result.BuilderID = user.BuilderID;
                            result.CellPhone = user.CellPhone;
                            result.EMail = user.EMail;
                            result.Fax = user.Fax;
                            result.FirstName = user.FirstName;
                            result.HomePhone = user.HomePhone;
                            result.Identifier = user.Identifier;
                            result.LastLogin = user.LastLogin;
                            result.LastName = user.LastName;
                            result.Username = user.Username;
                        }
                        finally
                        {
                            connection.Close();
                        }
                        return result;
                    }

                    private static UserBuilder ReadUserBuilder(MembershipUser member)
                    {
                        UserBuilder user = new UserBuilder();
                        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
                        SqlCommand command = new SqlCommand();

                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "dalBuilderUserRead";
                        DataAccessHelper.AddWithValue(command, "@Identifier", member.ProviderUserKey);
                        command.Connection = connection;
                        connection.Open();
                        try
                        {
                            SqlDataReader dataReader = command.ExecuteReader();
                            if (dataReader.Read())
                            {
                                user.Identifier = member.ProviderUserKey;
                                user.BuilderID = (int)dataReader["BuilderID"];
                                user.CellPhone = dataReader["CellPhone"].ToString();
                                user.EMail = member.Email;
                                user.Fax = dataReader["Fax"].ToString();
                                user.FirstName = dataReader["FirstName"].ToString();
                                user.HomePhone = dataReader["HomePhone"].ToString();
                                user.LastName = dataReader["LastName"].ToString();
                                user.LastLogin = member.LastLoginDate;
                                user.Username = member.UserName;
                            }
                        }
                        finally
                        {
                            connection.Close();
                        }
                        return user;
                    }

                    public static UserBuilder Create(UserBuilder user, string password)
                    {
                        UserBuilder result = null;
                        MembershipUser member = Membership.CreateUser(user.Username, password, user.EMail);
                        user.Identifier = member.ProviderUserKey;
                        if (member != null)
                        {
                            result = CreateUserBuilder(user);
                        }
                        return result;
                    }

                    public static UserBuilder Read(object identitifier)
                    {
                        UserBuilder result = null;
                        MembershipUser member = Membership.GetUser(identitifier);
                        if (member != null)
                        {
                            result = ReadUserBuilder(member);
                        }
                        return result;
                    }

                    public static UserBuilder Read(string username)
                    {
                        UserBuilder result = null;
                        MembershipUser member = Membership.GetUser(username);
                        if (member != null)
                        {
                            result = ReadUserBuilder(member);
                        }
                        return result;
                    }

                    public static List<UserBuilder> List(object builderID)
                    {
                        List<UserBuilder> result = new List<UserBuilder>();
                        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
                        SqlCommand command = new SqlCommand();

                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "dalBuilderUserSearch";
                        DataAccessHelper.AddWithValue(command, "@BuilderID", builderID);
                        command.Connection = connection;
                        connection.Open();
                        try
                        {
                            SqlDataReader dataReader = command.ExecuteReader();
                            while (dataReader.Read())
                            {
                                UserBuilder user = new UserBuilder();
                                user.Identifier = dataReader["Identifier"];
                                user.BuilderID = (int)dataReader["BuilderID"];
                                user.CellPhone = dataReader["CellPhone"].ToString();
                                user.Fax = dataReader["Fax"].ToString();
                                user.FirstName = dataReader["FirstName"].ToString();
                                user.HomePhone = dataReader["HomePhone"].ToString();
                                user.LastName = dataReader["LastName"].ToString();
                                MembershipUser member = Membership.GetUser(user.Identifier);
                                user.EMail = member.Email;
                                user.LastLogin = member.LastLoginDate;
                                user.Username = member.UserName;
                                user.Identifier = member.ProviderUserKey;
                                result.Add(user);
                            }
                        }
                        finally
                        {
                            connection.Close();
                        }
                        return result;
                    }
                }
        */
        /*
                private static SystemUser CreateUser(SystemUser user)
                {
                    SystemUser result = null;
                    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
                    SqlCommand command = new SqlCommand();

                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "dalSystemUserCreate";
                    DataAccessHelper.AddWithValue(command, "@Identifier", user.Identifier);
                    DataAccessHelper.AddWithValue(command, "@CellPhone", user.CellPhone);
                    DataAccessHelper.AddWithValue(command, "@Fax", user.Fax);
                    DataAccessHelper.AddWithValue(command, "@FirstName", user.FirstName);
                    DataAccessHelper.AddWithValue(command, "@HomePhone", user.HomePhone);
                    DataAccessHelper.AddWithValue(command, "@LastName", user.LastName);
                    DataAccessHelper.AddWithValue(command, "@WorkPhone", user.WorkPhone);
                    command.Connection = connection;
                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                        result = new SystemUser();
                        result.CellPhone = user.CellPhone;
                        result.EMail = user.EMail;
                        result.Fax = user.Fax;
                        result.FirstName = user.FirstName;
                        result.HomePhone = user.HomePhone;
                        result.Identifier = user.Identifier;
                        result.LastLogin = user.LastLogin;
                        result.LastName = user.LastName;
                        result.Username = user.Username;
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return result;
                }

                private static SystemUser ReadUser(MembershipUser member)
                {
                    SystemUser user = new SystemUser();
                    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
                    SqlCommand command = new SqlCommand();

                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "dalSystemUserRead";
                    DataAccessHelper.AddWithValue(command, "@Identifier", member.ProviderUserKey);
                    command.Connection = connection;
                    connection.Open();
                    try
                    {
                        SqlDataReader dataReader = command.ExecuteReader();
                        if (dataReader.Read())
                        {
                            user.Identifier = member.ProviderUserKey;
                            user.CellPhone = dataReader["CellPhone"].ToString();
                            user.EMail = member.Email;
                            user.Fax = dataReader["Fax"].ToString();
                            user.FirstName = dataReader["FirstName"].ToString();
                            user.HomePhone = dataReader["HomePhone"].ToString();
                            user.LastName = dataReader["LastName"].ToString();
                            user.LastLogin = member.LastLoginDate;
                            user.Username = member.UserName;
                            user.WorkPhone = dataReader["WorkPhone"].ToString();
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return user;
                }

                public static SystemUser Create(SystemUser user, string password)
                {
                    SystemUser result = null;
                    MembershipUser member = Membership.CreateUser(user.Username, password, user.EMail);
                    user.Identifier = member.ProviderUserKey;
                    if (member != null)
                    {
                        result = CreateUser(user);
                    }
                    return result;
                }

                public static SystemUser Read(object identitifier)
                {
                    SystemUser result = null;
                    System.Guid guidID = new Guid(identitifier.ToString());
                    MembershipUser member = Membership.GetUser(guidID);
                    if (member != null)
                    {
                        result = ReadUser(member);
                    }
                    return result;
                }

                public static SystemUser Read(string username)
                {
                    SystemUser result = null;
                    MembershipUser member = Membership.GetUser(username);
                    if (member != null)
                    {
                        result = ReadUser(member);
                    }
                    return result;
                }

                public static string GetPassword(SystemUser user)
                {
                    MembershipUser member = Membership.GetUser(user.Identifier);
                    return member.GetPassword();
                }

                public static bool ChangePassword(SystemUser user, string oldPassword, string newPassword)
                {
                    MembershipUser member = Membership.GetUser(user.Identifier);
                    return member.ChangePassword(oldPassword, newPassword);
                }

                public static bool UserPasswordChangeRead(object identity)
                {
                    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Authentication"].ConnectionString);
                    SqlCommand command = new SqlCommand();

                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "dalMembershipForcePasswordChangeRead";
                    DataAccessHelper.AddWithValue(command, "@Identifier", identity);
                    SqlParameter passwordChange = DataAccessHelper.AddParameter(command, "@ForcePasswordChange", System.Data.SqlDbType.Bit, null, System.Data.ParameterDirection.Output);
                    command.Connection = connection;
                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return (bool)passwordChange.Value;
                }

                public static bool UserPasswordChangeRead(SystemUser user)
                {
                    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Authentication"].ConnectionString);
                    SqlCommand command = new SqlCommand();

                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "dalMembershipForcePasswordChangeRead";
                    DataAccessHelper.AddWithValue(command, "@Identifier", user.Identifier);
                    SqlParameter passwordChange = DataAccessHelper.AddParameter(command, "@ForcePasswordChange", System.Data.SqlDbType.Bit, null, System.Data.ParameterDirection.Output);
                    command.Connection = connection;
                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return (bool)passwordChange.Value;
                }

                public static void UserPasswordChangeUpdate(SystemUser user, bool forcePasswordChange)
                {
                    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Authentication"].ConnectionString);
                    SqlCommand command = new SqlCommand();

                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "dalMembershipForcePasswordChangeUpdate";
                    DataAccessHelper.AddWithValue(command, "@Identifier", user.Identifier);
                    DataAccessHelper.AddWithValue(command, "@ForcePasswordChange", forcePasswordChange);
                    command.Connection = connection;
                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                public static string GenerateUserPassword(int length)
                {            
                    const string passwordChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";
                    string result = "";
                    Random randomizer = new Random();          
                    for (int i = 0; i < length; i++)
                    {
                        result += passwordChars[randomizer.Next(0, passwordChars.Length - 1)];
                    }
                    return result;
                }

                public static string ResetUserPassword(SystemUser user)
                {
                    MembershipUser authenticatedUser = Membership.GetUser(user.Identifier);
                    return authenticatedUser.ResetPassword();
                }

                public static void CreateUserRoles(SystemUser user, List<string> roles)
                {
                    Roles.AddUserToRoles(user.Username, roles.ToArray());
                }

                public static List<string> ReadUserRoles(SystemUser user)
                {
                    return new List<string>(Roles.GetRolesForUser(user.Username));
                }

                public static bool Login(string username, string password)
                {
                    bool validated = false;
                    // This sets the user LastActivityDate and make the user appear online.
                    validated = Membership.ValidateUser(username, password);
                    if (validated)
                    {
                        MembershipUser user = Membership.GetUser(username);
                        if (UserPasswordChangeRead(user.ProviderUserKey))
                        {
                            throw new UserPasswordChange();
                        }
                    }
                    return validated;
                }

                public static void UserChangePassword(SystemUser user, string oldPassword, string newPassword)
                {
                    MembershipUser member = Membership.GetUser(user.Identifier);
                    member.ChangePassword(oldPassword, newPassword);
                }

                public static void Logoff(SystemUser user)
                {
                    //            Membership.
                }

                public static void AssociateUserBuilder(SystemUser user, object builderID)
                {
                    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
                    SqlCommand command = new SqlCommand();

                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "dalSystemUserBuilderAssociate";
                    DataAccessHelper.AddWithValue(command, "@SystemUserIdentifier", user.Identifier);
                    DataAccessHelper.AddWithValue(command, "@BuilderID", builderID);
                    command.Connection = connection;
                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                public static void DissociateUserBuilder(SystemUser user, object builderID)
                {
                    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
                    SqlCommand command = new SqlCommand();

                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "dalSystemUserBuilderDissociate";
                    DataAccessHelper.AddWithValue(command, "@SystemUserIdentifier", user.Identifier);
                    DataAccessHelper.AddWithValue(command, "@BuilderID", builderID);
                    command.Connection = connection;
                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                public static void DissociateUserSubcontractor(SystemUser user, object subcontractorID)
                {
                    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
                    SqlCommand command = new SqlCommand();

                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "dalSystemUserSubcontractorDissociate";
                    DataAccessHelper.AddWithValue(command, "@SystemUserIdentifier", user.Identifier);
                    DataAccessHelper.AddWithValue(command, "@SubcontractorID", subcontractorID);
                    command.Connection = connection;
                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                public static void SystemUserEMailCreate(SystemUser fromUser, SystemUser toUser, EMailTemplate emailTemplate, EMail email)
                {
                    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
                    SqlCommand command = new SqlCommand();
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "dalSystemUserEMailCreate";
                    DataAccessHelper.AddWithValue(command, "@FromSystemUserID", (int)fromUser.Identifier);
                    DataAccessHelper.AddWithValue(command, "@ToSystemUserID", (int)toUser.Identifier);
                    DataAccessHelper.AddWithValue(command, "@EMailTemplateID", (int)emailTemplate.Identifier);
                    DataAccessHelper.AddWithValue(command, "@To", email.To);
                    DataAccessHelper.AddWithValue(command, "@CC", email.CC);
                    DataAccessHelper.AddWithValue(command, "@BCC", email.BCC);
                    DataAccessHelper.AddWithValue(command, "@Subject", email.Subject);
                    DataAccessHelper.AddWithValue(command, "@Body", email.Body); 
                    command.Connection = connection;
                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        connection.Close();
                    }               
                }

                public static List<SystemUser> Search(Builder builder)
                {
                    List<SystemUser> result = new List<SystemUser>();
                    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
                    SqlCommand command = new SqlCommand();
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "dalSystemUserSearchBuilder";
                    DataAccessHelper.AddWithValue(command, "@BuilderID", (int)builder.Identifier);
                    command.Connection = connection;
                    connection.Open();
                    try
                    {
                        SqlDataReader dataReader = command.ExecuteReader();
                        while (dataReader.Read())
                        {
                            SystemUser user = new SystemUser();
                            user.Identifier = dataReader["SystemUserID"];
                            user.CellPhone = dataReader["CellPhone"].ToString();
                            user.Fax = dataReader["Fax"].ToString();
                            user.FirstName = dataReader["FirstName"].ToString();
                            user.HomePhone = dataReader["HomePhone"].ToString();
                            user.LastName = dataReader["LastName"].ToString();
                            user.WorkPhone = dataReader["WorkPhone"].ToString();
                            MembershipUser member = Membership.GetUser(user.Identifier);
                            user.Identifier = member.ProviderUserKey;
                            user.Username = member.UserName;
                            user.LastLogin = member.LastLoginDate;
                            user.EMail = member.Email;
                            result.Add(user);
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return result;
                }

                public static List<SystemUser> Search(Subcontractor subcontractor)
                {
                    List<SystemUser> result = new List<SystemUser>();
                    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
                    SqlCommand command = new SqlCommand();
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "dalSystemUserSearchSubcontractor";
                    DataAccessHelper.AddWithValue(command, "@SubcontractorID", (int)subcontractor.Identifier);
                    command.Connection = connection;
                    connection.Open();
                    try
                    {
                        SqlDataReader dataReader = command.ExecuteReader();
                        if (dataReader.Read())
                        {
                            SystemUser user = new SystemUser();
                            user.CellPhone = dataReader["CellPhone"].ToString();
                            user.Fax = dataReader["Fax"].ToString();
                            user.FirstName = dataReader["FirstName"].ToString();
                            user.HomePhone = dataReader["HomePhone"].ToString();
                            user.LastName = dataReader["LastName"].ToString();
                            user.WorkPhone = dataReader["WorkPhone"].ToString();
                            MembershipUser member = Membership.GetUser(user.Identifier);
                            user.Identifier = member.ProviderUserKey;
                            user.Username = member.UserName;
                            user.LastLogin = member.LastLoginDate;
                            user.EMail = member.Email;
                            result.Add(user);
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return result;
                }
            }
         */
    }
}
