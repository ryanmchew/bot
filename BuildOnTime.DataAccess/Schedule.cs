﻿using System;
using System.Text;
using System.Collections.Generic;
using BuildOnTime.BusinessObject;
using System.Configuration;
using System.Data.SqlClient;

namespace BuildOnTime.DataAccess
{
    #region Schedule
    public static class ScheduleDAL
    {
        public static Schedule Create(object builderID, string name)
        {
            Schedule result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleCreate";
            DataAccessHelper.AddWithValue(command, "BuilderID", int.Parse(builderID.ToString()));
            SqlParameter scheduleID = command.Parameters.Add("@ScheduleID", System.Data.SqlDbType.Int);
            scheduleID.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@Name", name);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new Schedule();
                result.Identifier = (int)scheduleID.Value;
                result.Name = name;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static Schedule Read(object identifier)
        {
            Schedule result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleRead";
            DataAccessHelper.AddWithValue(command, "@ScheduleID", identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new Schedule();
                    result.Identifier = (int)dataReader["ScheduleID"];
                    result.Name = dataReader["Name"].ToString();
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void Update(Schedule schedule)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleUpdate";
            DataAccessHelper.AddWithValue(command, "@ScheduleID", schedule.Identifier);
            DataAccessHelper.AddWithValue(command, "@Name", schedule.Name);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Delete(Schedule schedule)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleDelete";
            DataAccessHelper.AddWithValue(command, "@ScheduleID", schedule.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<Schedule> Search(object builderID, string name)
        {
            List<Schedule> result = new List<Schedule>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleSearch";
            DataAccessHelper.AddWithValue(command, "@BuilderID", builderID);
            if (name != null)
            {
                DataAccessHelper.AddWithValue(command, "@Name", name + '%');
            }
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Schedule schedule = new Schedule();
                    schedule.Identifier =(int)dataReader["ScheduleID"];
                    schedule.Name = dataReader["Name"].ToString();
                    result.Add(schedule);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
    }
    #endregion

    #region ScheduleTask
    public static class ScheduleTaskDAL
    {
        public static ScheduleTask Create(object scheduleIdentifier, object defaultEmployeeID, object defaultSubcontractorID, string task, int dayNumber, int duration, TaskGroupEnum? group)
        {
            ScheduleTask result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleTaskCreate";
            SqlParameter scheduleTaskID = command.Parameters.Add("@ScheduleTaskID", System.Data.SqlDbType.Int);
            scheduleTaskID.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@ScheduleID", scheduleIdentifier);
            DataAccessHelper.AddWithValue(command, "@DefaultEmployeeID", defaultEmployeeID);
            DataAccessHelper.AddWithValue(command, "@DefaultSubcontractorID", defaultSubcontractorID);
            if (group != null)
            {
                DataAccessHelper.AddWithValue(command, "@GroupName", group.ToString());
            }
            else
            {
                DataAccessHelper.AddWithValue(command, "@GroupName", DBNull.Value);
            }
            DataAccessHelper.AddWithValue(command, "@Task", task);
            DataAccessHelper.AddWithValue(command, "@DayNumber", dayNumber);
            DataAccessHelper.AddWithValue(command, "@Duration", duration);
            SqlParameter updated = command.Parameters.Add("@Updated", System.Data.SqlDbType.DateTime);
            updated.Direction = System.Data.ParameterDirection.Output;
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new ScheduleTask();
                result.Identifier = (int)scheduleTaskID.Value;
                result.Updated = (DateTime)updated.Value;
                result.DayNumber = dayNumber;
                result.Duration = duration;
                result.ScheduleIdentifier = scheduleIdentifier;
                result.Name = task;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static ScheduleTask Read(object identifier)
        {
            ScheduleTask result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleTaskRead";
            DataAccessHelper.AddWithValue(command, "@ScheduleTaskID", identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new ScheduleTask();                    
                    result.Identifier = DataAccessHelper.FieldIntValue(dataReader, "ScheduleTaskID");
                    result.DayNumber = DataAccessHelper.FieldIntValue(dataReader, "DayNumber").Value;
                    result.Duration = DataAccessHelper.FieldIntValue(dataReader, "Duration").Value;
                    result.DefaultEmployeeID = DataAccessHelper.FieldIntValue(dataReader, "DefaultEmployeeID");
                    result.DefaultSubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "DefaultSubcontractorID");
                    if (DataAccessHelper.FieldStringValue(dataReader, "GroupName") != null)
                    {
                        result.Group = (TaskGroupEnum)Enum.Parse(typeof(TaskGroupEnum), dataReader["GroupName"].ToString());
                    }
                    result.ScheduleIdentifier = DataAccessHelper.FieldIntValue(dataReader, "ScheduleID"); ;
                    result.Name = DataAccessHelper.FieldStringValue(dataReader, "Task");
                    result.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "Updated").Value;
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void Update(ScheduleTask scheduleTask)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleTaskUpdate";
            DataAccessHelper.AddWithValue(command, "@ScheduleTaskID", scheduleTask.Identifier);
            DataAccessHelper.AddWithValue(command, "@ScheduleID", scheduleTask.ScheduleIdentifier);
            DataAccessHelper.AddWithValue(command, "@DefaultEmployeeID", scheduleTask.DefaultEmployeeID);
            DataAccessHelper.AddWithValue(command, "@DefaultSubcontractorID", scheduleTask.DefaultSubcontractorID);
            if (scheduleTask.Group != null)
            {
                DataAccessHelper.AddWithValue(command, "@GroupName", scheduleTask.Group.ToString());
            }
            else
            {
                DataAccessHelper.AddWithValue(command, "@GroupName", DBNull.Value);
            }
            DataAccessHelper.AddWithValue(command, "@Task", scheduleTask.Name);
            DataAccessHelper.AddWithValue(command, "@DayNumber", scheduleTask.DayNumber);
            DataAccessHelper.AddWithValue(command, "@Duration", scheduleTask.Duration);
            SqlParameter updated = command.Parameters.Add("@Updated", System.Data.SqlDbType.DateTime);
            updated.Direction = System.Data.ParameterDirection.Output;
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Delete(ScheduleTask scheduleTask)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleTaskDelete";
            DataAccessHelper.AddWithValue(command, "@ScheduleTaskID", scheduleTask.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<ScheduleTask> List(object scheduleID)
        {
            List<ScheduleTask> result = new List<ScheduleTask>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleTaskList";
            DataAccessHelper.AddWithValue(command, "@ScheduleID",scheduleID);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    ScheduleTask scheduleTask = new ScheduleTask();
                    scheduleTask.Identifier = DataAccessHelper.FieldIntValue(dataReader, "ScheduleTaskID");
                    scheduleTask.DayNumber = DataAccessHelper.FieldIntValue(dataReader, "DayNumber").Value;
                    scheduleTask.Duration = DataAccessHelper.FieldIntValue(dataReader, "Duration").Value;
                    scheduleTask.DefaultEmployeeID = DataAccessHelper.FieldIntValue(dataReader, "DefaultEmployeeID");
                    scheduleTask.DefaultSubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "DefaultSubcontractorID");
                    if (DataAccessHelper.FieldStringValue(dataReader, "GroupName") != null)
                    {
                        scheduleTask.Group = (TaskGroupEnum)Enum.Parse(typeof(TaskGroupEnum), dataReader["GroupName"].ToString());
                    }
                    scheduleTask.ScheduleIdentifier = DataAccessHelper.FieldIntValue(dataReader, "ScheduleID"); ;
                    scheduleTask.Name = DataAccessHelper.FieldStringValue(dataReader, "Task");
                    scheduleTask.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "Updated").Value;
                    result.Add(scheduleTask);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void TaskDependencyCreate(object scheduleID, object parentTaskID, object dependentTaskID)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleTaskDependentCreate";
            DataAccessHelper.AddWithValue(command, "@ScheduleID", scheduleID);
            DataAccessHelper.AddWithValue(command, "@ParentTaskID", parentTaskID);
            DataAccessHelper.AddWithValue(command, "@DependentTaskID", dependentTaskID);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void TaskDependencyDelete(object scheduleID, object parentTaskID, object dependentTaskID)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleTaskDependentDelete";
            DataAccessHelper.AddWithValue(command, "@ScheduleID", scheduleID);
            DataAccessHelper.AddWithValue(command, "@ParentTaskID", parentTaskID);
            DataAccessHelper.AddWithValue(command, "@DependentTaskID", dependentTaskID);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void TaskDependencyDeleteBySchedule(object scheduleID)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleTaskDependentDeleteBySchedule";
            DataAccessHelper.AddWithValue(command, "@ScheduleID", scheduleID);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<ScheduleTaskDependency> TaskDependencyList(object scheduleID)
        {
            List<ScheduleTaskDependency> result = new List<ScheduleTaskDependency>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalScheduleTaskDependentList";
            DataAccessHelper.AddWithValue(command, "@ScheduleID", scheduleID);
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    ScheduleTaskDependency taskDependency = new ScheduleTaskDependency();
                    taskDependency.ScheduleID = DataAccessHelper.FieldIntValue(dataReader, "ScheduleID").Value;
                    taskDependency.ParentID = DataAccessHelper.FieldIntValue(dataReader, "ParentTaskID").Value;
                    taskDependency.DependentID = DataAccessHelper.FieldIntValue(dataReader, "DependentTaskID").Value;
                    result.Add(taskDependency);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
    }
    #endregion
}
