﻿using System;
using System.Collections.Generic;
using System.Text;
using BuildOnTime.BusinessObject;
using System.Data.SqlClient;
using System.Configuration;

namespace BuildOnTime.DataAccess
{
    public static class SubdivisionDAL
    {
/*
        public static Subdivision Create(string name, string county, string state)
        {
            Subdivision result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubdivisionCreate";
            SqlParameter subdivisionID = command.Parameters.Add("@SubdivisionID", System.Data.SqlDbType.Int);
            subdivisionID.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@Name", name);
            DataAccessHelper.AddWithValue(command, "@County", county);
            DataAccessHelper.AddWithValue(command, "@State", state);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new Subdivision();
                result.Identifier = (int)subdivisionID.Value;
                result.County = county;
                result.State = state;
                result.Name = name;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static Subdivision Read(object identifier)
        {
            Subdivision result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubdivisionRead";
            DataAccessHelper.AddWithValue(command, "@SubdivisionID", identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new Subdivision();
                    result.Identifier = (int)dataReader["SubdivisionID"];
                    result.County = dataReader["County"].ToString();
                    result.Name = dataReader["Name"].ToString();
                    result.State = dataReader["State"].ToString();
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void Update(Subdivision subdivision)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubdivisionUpdate";
            DataAccessHelper.AddWithValue(command, "@SubdivisionID", subdivision.Identifier);
            DataAccessHelper.AddWithValue(command, "@Name", subdivision.Name);
            DataAccessHelper.AddWithValue(command, "@County", subdivision.County);
            DataAccessHelper.AddWithValue(command, "@State", subdivision.State);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Delete(Subdivision subdivision)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSubdivisionDelete";
            DataAccessHelper.AddWithValue(command, "@SubdivisionID", subdivision.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }
 */ 
    }
}
