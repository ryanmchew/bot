﻿using System;
using System.Collections.Generic;
using System.Text;
using BuildOnTime.BusinessObject;
using System.Data.SqlClient;
using System.Configuration;

namespace BuildOnTime.DataAccess
{
    public static class SiteDAL
    {
        public static Site Create(Site site)
        {
            Site result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSiteCreate";
            SqlParameter siteID = command.Parameters.Add("@SiteID", System.Data.SqlDbType.Int);
            siteID.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@BuilderID", site.BuilderID);
            DataAccessHelper.AddWithValue(command, "@LotIdentifier", site.LotIdentifier);
            DataAccessHelper.AddWithValue(command, "@AddressLine1", site.Address1);
            DataAccessHelper.AddWithValue(command, "@AddressLine2", DBNull.Value);
            DataAccessHelper.AddWithValue(command, "@City", site.City);
            DataAccessHelper.AddWithValue(command, "@State", site.State);
            DataAccessHelper.AddWithValue(command, "@PostalCode", site.PostalCode);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new Site();
                result.Identifier = (int)siteID.Value;
                result.Address1 = site.Address1;
                result.Address2 = site.Address2;
                result.LotIdentifier = site.LotIdentifier;
                result.PostalCode = site.PostalCode;
                result.State = site.State;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static Site Read(object identifier)
        {
            Site result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSiteRead";
            DataAccessHelper.AddWithValue(command, "@SiteID", identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new Site();
                    result.BuilderID = (int)dataReader["BuilderID"];
                    result.Identifier = (int)dataReader["SiteID"];
                    result.Address1 = dataReader["AddressLine1"].ToString();
                    result.Address2 = dataReader["AddressLine2"].ToString();
                    result.City = dataReader["City"].ToString();
                    result.LotIdentifier = dataReader["LotIdentifier"].ToString();
                    result.PostalCode = dataReader["PostalCode"].ToString();
                    result.State = dataReader["State"].ToString();
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void Update(Site site)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSiteUpdate";
            DataAccessHelper.AddWithValue(command, "@SiteID", site.Identifier);
            DataAccessHelper.AddWithValue(command, "@LotIdentifier", site.LotIdentifier);
            DataAccessHelper.AddWithValue(command, "@AddressLine1", site.Address1);
            DataAccessHelper.AddWithValue(command, "@AddressLine2", site.Address2);
            DataAccessHelper.AddWithValue(command, "@City", site.City);
            DataAccessHelper.AddWithValue(command, "@State", site.State);
            DataAccessHelper.AddWithValue(command, "@PostalCode", site.PostalCode);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Delete(Site site)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSiteDelete";
            DataAccessHelper.AddWithValue(command, "@SiteID", site.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<Site> Search(string lotIdentifier)
        {
            List<Site> result = new List<Site>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalSiteSearch";
            DataAccessHelper.AddWithValue(command, "@LotIdentifier", lotIdentifier + '%');
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Site site = new Site();
                    site.Identifier = (int)dataReader["SiteID"];
                    site.Address1 = dataReader["Address1"].ToString();
                    site.Address2 = dataReader["Address2"].ToString();
                    site.City = dataReader["City"].ToString();
                    site.LotIdentifier = dataReader["LotIdentifier"].ToString();
                    site.PostalCode = dataReader["PostalCode"].ToString();
                    site.State = dataReader["State"].ToString();
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
    }
}
