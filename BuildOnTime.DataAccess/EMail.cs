﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuildOnTime.BusinessObject;
using System.Data.SqlClient;
using System.Configuration;

namespace BuildOnTime.DataAccess
{
    public static class EMailTemplateDAL
    {
        public static EMailTemplate Create(EMailTemplate emailTemplate)
        {
            EMailTemplate result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailTemplateCreate";
            SqlParameter EMailTemplateID = command.Parameters.Add("@EMailTemplateID", System.Data.SqlDbType.Int);
            EMailTemplateID.Direction = System.Data.ParameterDirection.Output;
            SqlParameter Created = command.Parameters.Add("@Created", System.Data.SqlDbType.DateTime);
            EMailTemplateID.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@Name", emailTemplate.Name);
            DataAccessHelper.AddWithValue(command, "@Version", emailTemplate.Version);
            DataAccessHelper.AddWithValue(command, "@Subject", emailTemplate.Subject);
            DataAccessHelper.AddWithValue(command, "@Body", emailTemplate.Body);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new EMailTemplate();
                result.Identifier = (int)EMailTemplateID.Value;
                result.Name = emailTemplate.Name;
                result.Subject = emailTemplate.Subject;
                result.Created = (DateTime)Created.Value;
                result.Updated = (DateTime)Created.Value;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static EMailTemplate Read(object identifier)
        {
            EMailTemplate result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailTemplateRead";
            DataAccessHelper.AddWithValue(command, "@EMailTemplateID", identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new EMailTemplate();
                    result.Identifier = (int)dataReader["EMailTemplateID"];
                    result.Name = dataReader["Name"].ToString();
                    result.Version = dataReader["Version"].ToString();
                    result.Subject = dataReader["Subject"].ToString();
                    result.Body = dataReader["Body"].ToString();
                    result.Created = DataAccessHelper.FieldDateTimeValue(dataReader, "Created").Value;
                    result.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "Updated").Value;
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static EMailTemplate Read(EMailTemplateEnum emailTemplate)
        {
            EMailTemplate result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailTemplateReadByName";
            DataAccessHelper.AddWithValue(command, "@Name", emailTemplate.ToString());
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new EMailTemplate();
                    result.Identifier = (int)dataReader["EMailTemplateID"];
                    result.Name = dataReader["Name"].ToString();
                    result.Version = dataReader["Version"].ToString();
                    result.Subject = dataReader["Subject"].ToString();
                    result.Body = dataReader["Body"].ToString();
                    result.Created = DataAccessHelper.FieldDateTimeValue(dataReader, "Created").Value;
                    result.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "Updated").Value;
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void Update(EMailTemplate emailTemplate)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailTemplateUpdate";
            DataAccessHelper.AddWithValue(command, "@Name", emailTemplate.Name);
            DataAccessHelper.AddWithValue(command, "@Version", emailTemplate.Version);
            DataAccessHelper.AddWithValue(command, "@Subject", emailTemplate.Subject);
            DataAccessHelper.AddWithValue(command, "@Body", emailTemplate.Body);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Delete(EMailTemplate emailTemplate)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailTemplateDelete";
            DataAccessHelper.AddWithValue(command, "@EMailTemplateID", emailTemplate.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<EMailTemplate> List()
        {
            List<EMailTemplate> result = new List<EMailTemplate>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailTemplateList";
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    EMailTemplate emailTemplate = new EMailTemplate();
                    emailTemplate.Identifier = (int)dataReader["EMailTemplateID"];
                    emailTemplate.Name = dataReader["Name"].ToString();
                    emailTemplate.Version = dataReader["Version"].ToString();
                    emailTemplate.Subject = dataReader["Subject"].ToString();
                    emailTemplate.Body = dataReader["Body"].ToString();
                    emailTemplate.Created = DataAccessHelper.FieldDateTimeValue(dataReader, "Created").Value;
                    emailTemplate.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "Updated").Value;
                    result.Add(emailTemplate);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        /// <summary>
        /// Search will return a list of EMailTemplates that match the passed in criteria
        /// </summary>
        /// <param name="name">Name of the template to search for</param>
        /// <param name="version">Can specify a particular version if left null will return the most recently created.</param>
        /// <param name="resultCount">Specifies the number of records to return</param>
        /// <returns></returns>
        public static List<EMailTemplate> Search(string name, string version, int? resultCount)
        {
            List<EMailTemplate> result = new List<EMailTemplate>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailTemplateSearch";
            command.Connection = connection;
            DataAccessHelper.AddWithValue(command, "@Name", name);
            DataAccessHelper.AddWithValue(command, "@Version", version);
            DataAccessHelper.AddWithValue(command, "@ResultCount", resultCount);
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    EMailTemplate emailTemplate = new EMailTemplate();
                    emailTemplate.Identifier = (int)dataReader["EMailTemplateID"];
                    emailTemplate.Name = dataReader["Name"].ToString();
                    emailTemplate.Version = dataReader["Version"].ToString();
                    emailTemplate.Subject = dataReader["Subject"].ToString();
                    emailTemplate.Body = dataReader["Body"].ToString();
                    emailTemplate.Created = DataAccessHelper.FieldDateTimeValue(dataReader, "Created").Value;
                    emailTemplate.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "Updated").Value;
                    result.Add(emailTemplate);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
    }

    public static class EMailDAL
    {
        public static EMail Create(EMail eMail)
        {
            EMail result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailCreate";
            SqlParameter EMailID = command.Parameters.Add("@EMailID", System.Data.SqlDbType.Int);
            EMailID.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@FromSystemUserID", eMail.SenderSystemUserID);
            DataAccessHelper.AddWithValue(command, "@EMailTemplateID", eMail.EmailTemplateID);
            DataAccessHelper.AddWithValue(command, "@To", eMail.To);
            DataAccessHelper.AddWithValue(command, "@CC", eMail.CC);
            DataAccessHelper.AddWithValue(command, "@BCC", eMail.BCC);
            DataAccessHelper.AddWithValue(command, "@Subject", eMail.Subject);
            DataAccessHelper.AddWithValue(command, "@Body", eMail.Body);
            DataAccessHelper.AddWithValue(command, "@Sent", eMail.Sent);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new EMail();
                result.Identifier = (int)EMailID.Value;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static EMail Read(object identifier)
        {
            EMail result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailRead";
            DataAccessHelper.AddWithValue(command, "@EMailID", identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new EMail();
                    result.Identifier = (int)dataReader["EMailID"];
                    result.BCC = DataAccessHelper.FieldStringValue(dataReader, "BCC");
                    result.Body = DataAccessHelper.FieldStringValue(dataReader, "Body");
                    result.CC = DataAccessHelper.FieldStringValue(dataReader, "CC");
                    result.EmailTemplateID = DataAccessHelper.FieldIntValue(dataReader, "EMailTemplateID");
                    result.From = DataAccessHelper.FieldStringValue(dataReader, "From");
                    result.ReplyTo = DataAccessHelper.FieldStringValue(dataReader, "ReplyTo");
                    result.SenderSystemUserID = DataAccessHelper.FieldIntValue(dataReader, "SenderSystemUserID");
                    result.Sent = DataAccessHelper.FieldDateTimeValue(dataReader, "Sent").Value;
                    result.Subject = DataAccessHelper.FieldStringValue(dataReader, "Subject");
                    result.To = DataAccessHelper.FieldStringValue(dataReader, "To");
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void Update(EMail eMail)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailUpdate";
            DataAccessHelper.AddWithValue(command, "@EMailID", eMail.Identifier);
            DataAccessHelper.AddWithValue(command, "@FromSystemUserID", eMail.SenderSystemUserID);
            DataAccessHelper.AddWithValue(command, "@EMailTemplateID", eMail.EmailTemplateID);
            DataAccessHelper.AddWithValue(command, "@To", eMail.To);
            DataAccessHelper.AddWithValue(command, "@CC", eMail.CC);
            DataAccessHelper.AddWithValue(command, "@BCC", eMail.BCC);
            DataAccessHelper.AddWithValue(command, "@Subject", eMail.Subject);
            DataAccessHelper.AddWithValue(command, "@Body", eMail.Body);
            DataAccessHelper.AddWithValue(command, "@Sent", eMail.Sent);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Delete(EMail eMail)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailDelete";
            DataAccessHelper.AddWithValue(command, "@EMailID", eMail.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Search will return a list of EMails that match the passed in criteria
        /// </summary>
        /// <param name="name">Name of the template to search for</param>
        /// <param name="version">Can specify a particular version if left null will return the most recently created.</param>
        /// <param name="resultCount">Specifies the number of records to return</param>
        /// <returns></returns>
        public static List<EMail> Search(object systemUserID, DateTime startDate, DateTime endDate)
        {
            List<EMail> result = new List<EMail>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalEMailSearch";
            command.Connection = connection;
            DataAccessHelper.AddWithValue(command, "@FromSystemUserID", systemUserID);
            DataAccessHelper.AddWithValue(command, "@StartDate", startDate);
            DataAccessHelper.AddWithValue(command, "@EndDate", endDate);
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    EMail eMail = new EMail();
                    eMail.Identifier = DataAccessHelper.FieldIntValue(dataReader, "EMailID");
                    eMail.BCC = DataAccessHelper.FieldStringValue(dataReader, "BCC");
                    eMail.Body = DataAccessHelper.FieldStringValue(dataReader, "Body");
                    eMail.CC = DataAccessHelper.FieldStringValue(dataReader, "CC");
                    eMail.EmailTemplateID = DataAccessHelper.FieldIntValue(dataReader, "EMailTemplateID");
                    eMail.From = DataAccessHelper.FieldStringValue(dataReader, "From");
                    eMail.ReplyTo = DataAccessHelper.FieldStringValue(dataReader, "ReplyTo");
                    eMail.SenderSystemUserID = DataAccessHelper.FieldIntValue(dataReader, "SenderSystemUserID");
                    eMail.Sent = DataAccessHelper.FieldDateTimeValue(dataReader, "Sent").Value;
                    eMail.Subject = DataAccessHelper.FieldStringValue(dataReader, "Subject");
                    eMail.To = DataAccessHelper.FieldStringValue(dataReader, "To");
                    result.Add(eMail);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
    }
}
