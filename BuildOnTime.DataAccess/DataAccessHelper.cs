﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace BuildOnTime.DataAccess
{
    public static class DataAccessHelper
    {
        public static Guid? FieldGuidValue(SqlDataReader sqlReader, string fieldName)
        {
            // Assume we are being passed a good name
            int fieldPosition = sqlReader.GetOrdinal(fieldName);
            Guid? returnValue = null;
            // Check for null
            if (!sqlReader.IsDBNull(fieldPosition))
            {
                returnValue = sqlReader.GetGuid(fieldPosition);
            }
            return returnValue;
        }

        public static string FieldStringValue(SqlDataReader sqlReader, string fieldName)
        {
            // Assume we are being passed a good name
            int fieldPosition = sqlReader.GetOrdinal(fieldName);
            string returnValue = null;
            // Check for null
            if (!sqlReader.IsDBNull(fieldPosition))
            {
                returnValue = sqlReader.GetString(fieldPosition);
            }
            return returnValue;
        }

        public static int? FieldIntValue(SqlDataReader sqlReader, string fieldName)
        {
            // Assume we are being passed a good name
            int fieldPosition = sqlReader.GetOrdinal(fieldName);
            int? returnValue = null;
            // Check for null
            if (!sqlReader.IsDBNull(fieldPosition))
            {
                returnValue = (int)sqlReader.GetSqlInt32(fieldPosition);
            }
            return returnValue;
        }

        public static int? FieldIntValue(SqlDataReader sqlReader, string fieldName, int defaultValue)
        {
            // Assume we are being passed a good name
            int fieldPosition = sqlReader.GetOrdinal(fieldName);
            int returnValue = defaultValue;
            // Check for null
            if (!sqlReader.IsDBNull(fieldPosition))
            {
                returnValue = (int)sqlReader.GetSqlInt32(fieldPosition);
            }
            return returnValue;
        }

        public static Boolean FieldBooleanValue(SqlDataReader sqlReader, string fieldName)
        {
            int fieldPosition = sqlReader.GetOrdinal(fieldName);
            Boolean returnValue = false;
            // Check for null
            if (!sqlReader.IsDBNull(fieldPosition))
            {
                returnValue = sqlReader.GetBoolean(fieldPosition);
            }
            return returnValue;
        }

        public static DateTime? FieldDateTimeValue(SqlDataReader sqlReader, string fieldName)
        {
            int fieldPosition = sqlReader.GetOrdinal(fieldName);
            DateTime? returnValue = null;
            // Check for null
            if (!sqlReader.IsDBNull(fieldPosition))
            {
                returnValue = sqlReader.GetDateTime(fieldPosition);
            }
            return returnValue;
        }

        public static string ParamStringValue(SqlCommand sqlCommand, string paramName)
        {
            // Assume we are being passed a good name
            object value = sqlCommand.Parameters[paramName].Value;
            string returnValue = "";
            // Check for null
            if (value != null)
            {
                returnValue = value.ToString();
            }
            return returnValue;
        }

        public static int ParamIntValue(SqlCommand sqlCommand, string paramName)
        {
            // Assume we are being passed a good name
            int returnValue = 0;
            // Check for null
            if (sqlCommand.Parameters[paramName].Value != System.DBNull.Value)
            {
                returnValue = (int)sqlCommand.Parameters[paramName].Value;
            }
            return returnValue;
        }


        public static DateTime ParamDateTimeValue(SqlCommand sqlCommand, string paramName)
        {
            // Assume we are being passed a good name
            object value = sqlCommand.Parameters[paramName].Value;
            DateTime returnValue = DateTime.MinValue;
            // Check for null
            if (value != null)
            {
                returnValue = (DateTime)value;
            }
            return returnValue;
        }

        public static string ParamStringValue(SqlParameter parameter)
        {
            // Assume we are being passed a good parameter
            object value = parameter.Value;
            string returnValue = "";
            // Check for null
            if (value != null)
            {
                returnValue = value.ToString();
            }
            return returnValue;
        }

        public static int ParamIntValue(SqlParameter parameter)
        {
            // Assume we are being passed a good parameter
            object value = parameter.Value;
            int returnValue = 0;
            // Check for null
            if (value != System.DBNull.Value)
            {
                returnValue = (int)value;
            }
            return returnValue;
        }

        public static DateTime ParamDateTimeValue(SqlParameter parameter)
        {
            // Assume we are being passed a good name
            object value = parameter.Value;
            DateTime returnValue = DateTime.MinValue;
            // Check for null
            if (value != null)
            {
                returnValue = (DateTime)value;
            }
            return returnValue;
        }

        public static void AddWithValue(SqlCommand command, string name, object value)
        {
            if (value == null)
            {
                command.Parameters.AddWithValue(name, DBNull.Value);
            }
            else
            {
                command.Parameters.AddWithValue(name, value);
            }
        }

        public static void AddWithValue(SqlCommand command, string name, DateTime? dateTime)
        {
            if (!dateTime.HasValue)
            {
                command.Parameters.AddWithValue(name, DBNull.Value);
            }
            else
            {
                command.Parameters.AddWithValue(name, dateTime.Value);
            }
        }

        public static SqlParameter AddParameter(SqlCommand command, string name, SqlDbType type, int columnSize, Object value, ParameterDirection direction)
        {
            SqlParameter param = new SqlParameter();
            param.ParameterName = name;
            param.SqlDbType = type;
            if (value == null)
            {
                value = DBNull.Value;
            }
            param.Value = value;
            param.Direction = direction;
            param.Size = columnSize;
            command.Parameters.Add(param);
            return param;
        }

        public static SqlParameter AddParameter(SqlCommand command, string name, SqlDbType type, Object value, ParameterDirection direction)
        {
            SqlParameter param = new SqlParameter();
            param.ParameterName = name;
            param.SqlDbType = type;
            if (value == null)
            {
                value = DBNull.Value;
            }
            param.Value = value;
            param.Direction = direction;
            command.Parameters.Add(param);
            return param;
        }

        public static string SortOrder(string currentSort, string primaryColumn)
        {
            string sortOrder = " ASC";
            // Find out if we are reversing the same sort order.
            if (currentSort.Length > 0)
            {
                string[] sortArray = currentSort.Split(',');
                if (sortArray.Length > 0)
                {
                    int sortOrderPosition = sortArray[0].IndexOf(' ');
                    if (sortArray[0].Substring(0, sortOrderPosition).Equals(primaryColumn, StringComparison.OrdinalIgnoreCase))
                    {
                        if (sortArray[0].Substring(sortOrderPosition).Trim().Equals("ASC", StringComparison.OrdinalIgnoreCase))
                        {
                            sortOrder = " DESC";
                        }
                    }
                }
            }
            return primaryColumn + sortOrder;
        }

        public static string SortOrder(string currentSort, string primaryColumn, string additionalSorts)
        {
            string primarySort = SortOrder(currentSort, primaryColumn);
            additionalSorts = additionalSorts.Trim();
            if (!additionalSorts.StartsWith(","))
            {
                additionalSorts = ", " + additionalSorts;
            }
            return primarySort + additionalSorts;
        }
    }
}
