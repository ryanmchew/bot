﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using BuildOnTime.BusinessObject;

namespace BuildOnTime.DataAccess
{
    public static class JobTaskDAL
    {
        public static JobTask Create(JobTask jobTask)
        {
            JobTask result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskCreate";
            SqlParameter jobTaskID = command.Parameters.Add("@JobTaskID", System.Data.SqlDbType.Int);
            jobTaskID.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@JobID", int.Parse(jobTask.JobID.ToString()));
            DataAccessHelper.AddWithValue(command, "@ScheduleTaskID", int.Parse(jobTask.ScheduleTaskID.ToString()));
            DataAccessHelper.AddWithValue(command, "@TaskName", jobTask.TaskName);
            DataAccessHelper.AddWithValue(command, "@TimeFrame", jobTask.TimeFrame);
            DataAccessHelper.AddWithValue(command, "@Start", jobTask.Start);
            DataAccessHelper.AddWithValue(command, "@Finish", jobTask.Finish);
            DataAccessHelper.AddWithValue(command, "@ScheduleStart", jobTask.ScheduledStart);
            DataAccessHelper.AddWithValue(command, "@ScheduleFinish", jobTask.ScheduledFinish);
            DataAccessHelper.AddWithValue(command, "@Signoff", jobTask.Signoff);
            if (jobTask.Group.HasValue)
            {
                DataAccessHelper.AddWithValue(command, "@GroupName", jobTask.Group.Value.ToString());
            }
            else
            {
                DataAccessHelper.AddWithValue(command, "@GroupName", DBNull.Value);
            }
            SqlParameter updated = command.Parameters.Add("@Updated", System.Data.SqlDbType.DateTime);
            updated.Direction = System.Data.ParameterDirection.Output;
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new JobTask();
                result.Finish = jobTask.Finish;
                result.Start = jobTask.Start;
                result.Signoff = jobTask.Signoff;
                result.EmployeeID = jobTask.EmployeeID;
                result.Duration = jobTask.Duration;
                result.Group = jobTask.Group;
                result.JobID = jobTask.JobID;
                result.Identifier = int.Parse(jobTaskID.Value.ToString());
                result.ScheduledStart = jobTask.ScheduledStart;
                result.ScheduledFinish = jobTask.ScheduledFinish;
                result.SubcontractorID = jobTask.SubcontractorID;
                result.TaskName = jobTask.TaskName;
                result.TimeFrame = jobTask.TimeFrame;
                result.Updated = (DateTime)updated.Value;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static JobTask Read(object identifier)
        {
            JobTask result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskRead";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new JobTask();
                    result.Identifier = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID").Value;
                    result.JobID = DataAccessHelper.FieldIntValue(dataReader, "JobID").Value;
                    result.Duration = DataAccessHelper.FieldIntValue(dataReader, "Duration").Value;
                    result.ScheduleTaskID = DataAccessHelper.FieldIntValue(dataReader, "ScheduleTaskID");
                    result.ScheduledStart = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleStart").Value;
                    result.ScheduledFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleFinish").Value;
                    result.Start = DataAccessHelper.FieldDateTimeValue(dataReader, "Start").Value;
                    result.Finish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finish").Value;
                    result.SubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "SubcontractorID");
                    result.EmployeeID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID");
                    if (result.SubcontractorID != null)
                    {
                        result.WorkerAccepted = DataAccessHelper.FieldDateTimeValue(dataReader, "SubcontractorConfirmed");
                        result.WorkerFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "SubcontractorFinished");                        
                    }
                    if (result.EmployeeID != null)
                    {
                        result.WorkerAccepted = DataAccessHelper.FieldDateTimeValue(dataReader, "EmployeeConfirmed");
                        result.WorkerFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "EmployeeFinished");
                    }
                    string group = DataAccessHelper.FieldStringValue(dataReader, "GroupName");
                    if (group != null)
                    {
                        result.Group = (TaskGroupEnum)Enum.Parse(typeof(TaskGroupEnum), group);
                    }
                    result.Signoff = DataAccessHelper.FieldDateTimeValue(dataReader, "Signoff");
                    result.TaskName = DataAccessHelper.FieldStringValue(dataReader, "TaskName");
                    result.TimeFrame = DataAccessHelper.FieldStringValue(dataReader, "TimeFrame");
                    result.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "Updated").Value;
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void Update(JobTask jobTask)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskUpdate";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@JobID", int.Parse(jobTask.JobID.ToString()));
            if (jobTask.ScheduleTaskID != null)
            {
                DataAccessHelper.AddWithValue(command, "@ScheduleTaskID", int.Parse(jobTask.ScheduleTaskID.ToString()));
            }
            else
            {
                DataAccessHelper.AddWithValue(command, "@ScheduleTaskID", DBNull.Value);
            }
            DataAccessHelper.AddWithValue(command, "@TaskName", jobTask.TaskName);
            DataAccessHelper.AddWithValue(command, "@TimeFrame", jobTask.TimeFrame);
            DataAccessHelper.AddWithValue(command, "@Start", jobTask.Start);
            DataAccessHelper.AddWithValue(command, "@Finish", jobTask.Finish);
            DataAccessHelper.AddWithValue(command, "@ScheduleStart", jobTask.ScheduledStart);
            DataAccessHelper.AddWithValue(command, "@ScheduleFinish", jobTask.ScheduledFinish);
            DataAccessHelper.AddWithValue(command, "@Signoff", jobTask.Signoff);
            if (jobTask.Group.HasValue)
            {
                DataAccessHelper.AddWithValue(command, "@GroupName", jobTask.Group.Value.ToString());
            }
            else
            {
                DataAccessHelper.AddWithValue(command, "@GroupName", DBNull.Value);
            }
            SqlParameter updated = command.Parameters.Add("@Updated", System.Data.SqlDbType.DateTime);
            updated.Direction = System.Data.ParameterDirection.Output;
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Delete(JobTask jobTask)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskDelete";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", jobTask.Identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<JobTask> List(object jobID)
        {
            List<JobTask> result = new List<JobTask>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskList";
            DataAccessHelper.AddWithValue(command, "@JobID", int.Parse(jobID.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    JobTask jobTask = new JobTask();
                    jobTask.Identifier = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID").Value;
                    jobTask.JobID = DataAccessHelper.FieldIntValue(dataReader, "JobID").Value;
                    jobTask.ScheduleTaskID = DataAccessHelper.FieldIntValue(dataReader, "ScheduleTaskID");
                    jobTask.ScheduledStart = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleStart").Value;
                    jobTask.ScheduledFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleFinish").Value;
                    jobTask.Start = DataAccessHelper.FieldDateTimeValue(dataReader, "Start").Value;
                    jobTask.Finish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finish").Value;
                    jobTask.Duration = DataAccessHelper.FieldIntValue(dataReader, "Duration").Value;
                    jobTask.SubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "SubcontractorID");
                    jobTask.Signoff = DataAccessHelper.FieldDateTimeValue(dataReader, "Signoff");
                    jobTask.EmployeeID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID");
                    string group = DataAccessHelper.FieldStringValue(dataReader, "GroupName");
                    if (group != null)
                    {
                        jobTask.Group = (TaskGroupEnum)Enum.Parse(typeof(TaskGroupEnum), group);
                    }
                    jobTask.TaskName = DataAccessHelper.FieldStringValue(dataReader, "TaskName");
                    jobTask.TimeFrame = DataAccessHelper.FieldStringValue(dataReader, "TimeFrame");
                    jobTask.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "Updated").Value;
                    if (jobTask.SubcontractorID != null)
                    {
                        jobTask.WorkerAccepted = DataAccessHelper.FieldDateTimeValue(dataReader, "SubcontractorConfirmed");
                        jobTask.WorkerFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "SubcontractorFinished");
                    }
                    if (jobTask.EmployeeID != null)
                    {
                        jobTask.WorkerAccepted = DataAccessHelper.FieldDateTimeValue(dataReader, "EmployeeConfirmed");
                        jobTask.WorkerFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "EmployeeFinished");
                    }
                    result.Add(jobTask);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void SubcontractorAssociate(JobTask jobTask, Subcontractor subcontractor)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskSubcontractorAssociate";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", int.Parse(subcontractor.Identifier.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void SubcontractorAssociate(JobTask jobTask, object subcontractorID)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskSubcontractorAssociate";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", int.Parse(subcontractorID.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void SubcontractorDissociate(JobTask jobTask)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskSubcontractorDissociate";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void SubcontractorConfirm(JobTask jobTask, Subcontractor subcontractor, DateTime? confirmed)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskSubcontractorConfirm";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", int.Parse(subcontractor.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@Confirmed", confirmed);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void SubcontractorFinish(JobTask jobTask, Subcontractor subcontractor, DateTime? finished)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskSubcontractorFinish";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", int.Parse(subcontractor.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@Finished", finished);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void SubcontractorReject(JobTask jobTask, Subcontractor subcontractor, DateTime? rejected)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskSubcontractorReject";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", int.Parse(subcontractor.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@Rejected", rejected);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<JobTask> SearchSupervisorTask(object employeeID, DateTime searchStart, DateTime searchFinish)
        {
            List<JobTask> result = new List<JobTask>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskSupervisorList";
            DataAccessHelper.AddWithValue(command, "@EmployeeID", int.Parse(employeeID.ToString()));
            DataAccessHelper.AddWithValue(command, "@StartDateRange", searchStart);
            DataAccessHelper.AddWithValue(command, "@EndDateRange", searchFinish);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    JobTask jobTask = new JobTask();
                    jobTask.JobID = DataAccessHelper.FieldIntValue(dataReader, "JobID");
                    jobTask.Identifier = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID");
                    jobTask.ScheduleTaskID = DataAccessHelper.FieldIntValue(dataReader, "ScheduleTaskID");
                    jobTask.ScheduledStart = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleStart").Value;
                    jobTask.ScheduledFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleFinish").Value;
                    jobTask.Start = DataAccessHelper.FieldDateTimeValue(dataReader, "Start").Value;
                    jobTask.Finish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finish").Value;
                    jobTask.Duration = DataAccessHelper.FieldIntValue(dataReader, "Duration").Value;                    
                    jobTask.Signoff = DataAccessHelper.FieldDateTimeValue(dataReader, "Signoff");
                    jobTask.EmployeeID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID");                    
                    jobTask.SubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "SubcontractorID");
                    if (jobTask.EmployeeID != null)
                    {
                        jobTask.WorkerAccepted = DataAccessHelper.FieldDateTimeValue(dataReader, "EmployeeConfirmed");
                        jobTask.WorkerFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "EmployeeFinished");
                        jobTask.WorkerAssigned = DataAccessHelper.FieldDateTimeValue(dataReader, "EmployeeAssigned");
                    }
                    if (jobTask.SubcontractorID != null)
                    {
                        jobTask.WorkerAccepted = DataAccessHelper.FieldDateTimeValue(dataReader, "SubcontractorConfirmed");
                        jobTask.WorkerFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "SubcontractorFinished");
                        jobTask.WorkerAssigned = DataAccessHelper.FieldDateTimeValue(dataReader, "SubcontractorAssigned");
                    }
                    jobTask.TaskName = DataAccessHelper.FieldStringValue(dataReader, "TaskName");
                    jobTask.TimeFrame = DataAccessHelper.FieldStringValue(dataReader, "TimeFrame");
                    jobTask.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "JobTaskUpdated").Value;
                    string jobTaskGroup = DataAccessHelper.FieldStringValue(dataReader, "GroupName");
                    if (jobTaskGroup != null)
                    {
                        jobTask.Group = (TaskGroupEnum)Enum.Parse(typeof(TaskGroupEnum), jobTaskGroup);
                    }
                    result.Add(jobTask);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static List<JobTask> SearchEmployeeTask(object employeeID, DateTime searchStart, DateTime searchFinish)
        {
            List<JobTask> result = new List<JobTask>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskEmployeeList";
            DataAccessHelper.AddWithValue(command, "@EmployeeID", int.Parse(employeeID.ToString()));
            DataAccessHelper.AddWithValue(command, "@StartDateRange", searchStart);
            DataAccessHelper.AddWithValue(command, "@EndDateRange", searchFinish);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    JobTask jobTask = new JobTask();
                    jobTask.JobID = DataAccessHelper.FieldIntValue(dataReader, "JobID");
                    jobTask.Identifier = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID");
                    jobTask.ScheduleTaskID = DataAccessHelper.FieldIntValue(dataReader, "ScheduleTaskID");
                    jobTask.ScheduledStart = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleStart").Value;
                    jobTask.ScheduledFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleFinish").Value;
                    jobTask.Start = DataAccessHelper.FieldDateTimeValue(dataReader, "Start").Value;
                    jobTask.Finish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finish").Value;
                    jobTask.Duration = DataAccessHelper.FieldIntValue(dataReader, "Duration").Value;
                    jobTask.EmployeeID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID").Value;
                    jobTask.SubcontractorID = null;
                    jobTask.WorkerAccepted = DataAccessHelper.FieldDateTimeValue(dataReader, "Confirmed").Value;
                    jobTask.WorkerAssigned = DataAccessHelper.FieldDateTimeValue(dataReader, "Assigned");
                    jobTask.WorkerFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finished").Value;
                    jobTask.Signoff = DataAccessHelper.FieldDateTimeValue(dataReader, "Signoff").Value;
                    jobTask.TaskName = DataAccessHelper.FieldStringValue(dataReader, "TaskName");
                    jobTask.TimeFrame = DataAccessHelper.FieldStringValue(dataReader, "TimeFrame");
                    jobTask.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "Updated").Value;
                    string jobTaskGroup = DataAccessHelper.FieldStringValue(dataReader, "GroupName");
                    if (jobTaskGroup != null)
                    {
                        jobTask.Group = (TaskGroupEnum)Enum.Parse(typeof(TaskGroupEnum), jobTaskGroup);
                    }
                    result.Add(jobTask);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static List<JobTask> SearchSubcontractorTask(object subcontractorID, DateTime searchStart, DateTime searchFinish)
        {
            List<JobTask> result = new List<JobTask>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskSubcontractorList";
            DataAccessHelper.AddWithValue(command, "@SubcontractorID", int.Parse(subcontractorID.ToString()));
            DataAccessHelper.AddWithValue(command, "@StartDateRange", searchStart);
            DataAccessHelper.AddWithValue(command, "@EndDateRange", searchFinish);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    JobTask jobTask = new JobTask();
                    jobTask.BuilderID = DataAccessHelper.FieldIntValue(dataReader, "BuilderID");
                    jobTask.JobID = DataAccessHelper.FieldIntValue(dataReader, "JobID");
                    jobTask.Identifier = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID");
                    jobTask.ScheduleTaskID = DataAccessHelper.FieldIntValue(dataReader, "ScheduleTaskID");
                    jobTask.ScheduledStart = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleStart").Value;
                    jobTask.ScheduledFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleFinish").Value;
                    jobTask.Start = DataAccessHelper.FieldDateTimeValue(dataReader, "Start").Value;
                    jobTask.Finish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finish").Value;
                    jobTask.Duration = DataAccessHelper.FieldIntValue(dataReader, "Duration").Value;
                    jobTask.EmployeeID = null;
                    jobTask.SubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "SubcontractorID").Value;
                    jobTask.WorkerAccepted = DataAccessHelper.FieldDateTimeValue(dataReader, "Confirmed");
                    jobTask.WorkerAssigned = DataAccessHelper.FieldDateTimeValue(dataReader, "Assigned");
                    jobTask.WorkerFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finished");
                    jobTask.Signoff = DataAccessHelper.FieldDateTimeValue(dataReader, "Signoff");
                    jobTask.TaskName = DataAccessHelper.FieldStringValue(dataReader, "TaskName");
                    jobTask.TimeFrame = DataAccessHelper.FieldStringValue(dataReader, "TimeFrame");
                    jobTask.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "Updated").Value;
                    string jobTaskGroup = DataAccessHelper.FieldStringValue(dataReader, "GroupName");
                    if (jobTaskGroup != null)
                    {
                        jobTask.Group = (TaskGroupEnum)Enum.Parse(typeof(TaskGroupEnum), jobTaskGroup);
                    }
                    result.Add(jobTask);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void EmployeeAssociate(JobTask jobTask, Employee employee)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskEmployeeAssociate";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@EmployeeID", int.Parse(employee.Identifier.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void EmployeeAssociate(JobTask jobTask, object employeeID)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskEmployeeAssociate";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@EmployeeID", int.Parse(employeeID.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void EmployeeDissociate(JobTask jobTask)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskEmployeeDissociate";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void EmployeeConfirm(JobTask jobTask, Employee employee, DateTime? confirmed)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskEmployeeConfirm";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@EmployeeID", int.Parse(employee.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@Confirmed", confirmed);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void EmployeeFinish(JobTask jobTask, Employee employee, DateTime? finished)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskEmployeeFinish";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@EmployeeID", int.Parse(employee.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@Finished", finished);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void EmployeeReject(JobTask jobTask, Employee employee, DateTime? rejected)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskEmployeeReject";
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTask.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@EmployeeID", int.Parse(employee.Identifier.ToString()));
            DataAccessHelper.AddWithValue(command, "@Rejected", rejected);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<JobTask> SearchBuilderTask(object builderID, DateTime startDate, DateTime endDate)
        {
            List<JobTask> result = new List<JobTask>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskBuilderList";
            DataAccessHelper.AddWithValue(command, "@BuilderID", int.Parse(builderID.ToString()));
            DataAccessHelper.AddWithValue(command, "@StartDateRange", startDate);
            DataAccessHelper.AddWithValue(command, "@EndDateRange", endDate);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    JobTask jobTask = new JobTask();
                    jobTask.JobID = DataAccessHelper.FieldIntValue(dataReader, "JobID");
                    jobTask.Identifier = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID");
                    jobTask.ScheduleTaskID = DataAccessHelper.FieldIntValue(dataReader, "ScheduleTaskID");
                    jobTask.ScheduledStart = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleStart").Value;
                    jobTask.ScheduledFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "ScheduleFinish").Value;
                    jobTask.Start = DataAccessHelper.FieldDateTimeValue(dataReader, "Start").Value;
                    jobTask.Finish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finish").Value;
                    jobTask.Duration = DataAccessHelper.FieldIntValue(dataReader, "Duration").Value;
                    jobTask.EmployeeID = null;
                    jobTask.SubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "SubcontractorID");
                    jobTask.WorkerAccepted = DataAccessHelper.FieldDateTimeValue(dataReader, "Confirmed");
                    jobTask.WorkerFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finished");
                    jobTask.Signoff = DataAccessHelper.FieldDateTimeValue(dataReader, "Signoff");
                    jobTask.TaskName = DataAccessHelper.FieldStringValue(dataReader, "TaskName");
                    jobTask.TimeFrame = DataAccessHelper.FieldStringValue(dataReader, "TimeFrame");
                    jobTask.Updated = DataAccessHelper.FieldDateTimeValue(dataReader, "Updated").Value;
                    string jobTaskGroup = DataAccessHelper.FieldStringValue(dataReader, "GroupName");
                    if (jobTaskGroup != null)
                    {
                        jobTask.Group = (TaskGroupEnum)Enum.Parse(typeof(TaskGroupEnum), jobTaskGroup);
                    }
                    result.Add(jobTask);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }



        public static void TaskDependencyCreate(object jobID, object parentTaskID, object dependentTaskID)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskDependentCreate";
            DataAccessHelper.AddWithValue(command, "@JobID", jobID);
            DataAccessHelper.AddWithValue(command, "@ParentTaskID", parentTaskID);
            DataAccessHelper.AddWithValue(command, "@DependentTaskID", dependentTaskID);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void TaskDependencyDelete(object jobID, object parentTaskID, object dependentTaskID)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskDependentDelete";
            DataAccessHelper.AddWithValue(command, "@JobID", jobID);
            DataAccessHelper.AddWithValue(command, "@ParentTaskID", parentTaskID);
            DataAccessHelper.AddWithValue(command, "@DependentTaskID", dependentTaskID);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void TaskDependencyDeleteByJob(object jobID)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskDependentDeleteByJob";
            DataAccessHelper.AddWithValue(command, "@JobID", jobID);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<JobTaskDependency> TaskDependencyList(object jobID)
        {
            List<JobTaskDependency> result = new List<JobTaskDependency>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskDependentList";
            DataAccessHelper.AddWithValue(command, "@JobID", jobID);
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    JobTaskDependency taskDependency = new JobTaskDependency();
                    taskDependency.JobID = DataAccessHelper.FieldIntValue(dataReader, "JobID").Value;
                    taskDependency.ParentID = DataAccessHelper.FieldIntValue(dataReader, "ParentTaskID").Value;
                    taskDependency.DependentID = DataAccessHelper.FieldIntValue(dataReader, "DependentTaskID").Value;
                    result.Add(taskDependency);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

    }
}
