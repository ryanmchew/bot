﻿using System;
using System.Collections.Generic;
using System.Text;
using BuildOnTime.BusinessObject;
using System.Data.SqlClient;
using System.Configuration;

namespace BuildOnTime.DataAccess
{
    public static class JobDAL
    {
        public static Job Create(object builderID, object superID, object siteID, string name, string permit, string description)
        {
            Job result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();

            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobCreate";
            SqlParameter jobID = command.Parameters.Add("@JobID", System.Data.SqlDbType.Int);
            jobID.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@BuilderID", (int)builderID);
            DataAccessHelper.AddWithValue(command, "@EmployeeID", superID);
            DataAccessHelper.AddWithValue(command, "@SiteID", (int)siteID);
            DataAccessHelper.AddWithValue(command, "@Name", name);
            DataAccessHelper.AddWithValue(command, "@Permit", permit);
            DataAccessHelper.AddWithValue(command, "@Description", description);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new Job();
                result.Identifier = (int)jobID.Value;
                result.Name = name;
                result.Description = description;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static Job Read(object identifier)
        {
            Job result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobRead";
            DataAccessHelper.AddWithValue(command, "@JobID", identifier);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new Job();
                    result.BuilderID = DataAccessHelper.FieldIntValue(dataReader, "BuilderID");
                    result.Identifier = DataAccessHelper.FieldIntValue(dataReader, "JobID");
                    result.SuperID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID");
                    result.SiteID = DataAccessHelper.FieldIntValue(dataReader, "SiteID");
                    result.Name = DataAccessHelper.FieldStringValue(dataReader, "Name");
                    result.Permit = DataAccessHelper.FieldStringValue(dataReader, "Permit");
                    result.Description = DataAccessHelper.FieldStringValue(dataReader, "Description");
                    result.start = DataAccessHelper.FieldDateTimeValue(dataReader, "Start");
                    result.finish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finish");
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void Update(Job job)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobUpdate";
            DataAccessHelper.AddWithValue(command, "@JobID", job.Identifier);
            DataAccessHelper.AddWithValue(command, "@BuilderID", job.BuilderID);
            DataAccessHelper.AddWithValue(command, "@SiteID", job.SiteID);
            DataAccessHelper.AddWithValue(command, "@EmployeeID", job.SuperID);
            DataAccessHelper.AddWithValue(command, "@Name", job.Name);
            DataAccessHelper.AddWithValue(command, "@Permit", job.Permit);
            DataAccessHelper.AddWithValue(command, "@Description", job.Description);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Delete(Job job, int userId, string initiator)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobDelete";
            DataAccessHelper.AddWithValue(command, "@JobID", job.Identifier);
            DataAccessHelper.AddWithValue(command, "@UserId", userId);
            DataAccessHelper.AddWithValue(command, "@Initiator", initiator);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<Job> Search(object builderID, string name)
        {
            List<Job> result = new List<Job>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobSearch";
            DataAccessHelper.AddWithValue(command, "@BuilderID", builderID);
            DataAccessHelper.AddWithValue(command, "@Name", name + '%');
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Job job = new Job();
                    job.BuilderID = DataAccessHelper.FieldIntValue(dataReader, "BuilderID");
                    job.Identifier = DataAccessHelper.FieldIntValue(dataReader, "JobID");
                    job.SuperID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID");
                    job.SiteID = DataAccessHelper.FieldIntValue(dataReader, "SiteID");
                    job.Name = DataAccessHelper.FieldStringValue(dataReader, "Name");
                    job.Permit = DataAccessHelper.FieldStringValue(dataReader, "Permit");
                    job.Description = DataAccessHelper.FieldStringValue(dataReader, "Description");
                    job.start = DataAccessHelper.FieldDateTimeValue(dataReader, "Start");
                    job.finish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finish");
                    result.Add(job);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static List<Job> Search(object builderID, DateTime activeDate)
        {
            List<Job> result = new List<Job>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobSearchByDate";
            DataAccessHelper.AddWithValue(command, "@BuilderID", builderID);
            DataAccessHelper.AddWithValue(command, "@ActiveDate", activeDate);
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Job job = new Job();
                    job.BuilderID = DataAccessHelper.FieldIntValue(dataReader, "BuilderID");
                    job.Identifier = DataAccessHelper.FieldIntValue(dataReader, "JobID");
                    job.SuperID = DataAccessHelper.FieldIntValue(dataReader, "EmployeeID");
                    job.SiteID = DataAccessHelper.FieldIntValue(dataReader, "SiteID");
                    job.Name = DataAccessHelper.FieldStringValue(dataReader, "Name");
                    job.Permit = DataAccessHelper.FieldStringValue(dataReader, "Permit");
                    job.Description = DataAccessHelper.FieldStringValue(dataReader, "Description");
                    job.start = DataAccessHelper.FieldDateTimeValue(dataReader, "Start");
                    job.finish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finish");
                    result.Add(job);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;            
        }
    }

    public static class JobDelayDAL
    {
        public static JobDelay Create(JobDelay jobDelay)
        {
            JobDelay result;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobDelayCreate";
            SqlParameter jobDelayID = command.Parameters.Add("@JobDelayID", System.Data.SqlDbType.Int);
            jobDelayID.Direction = System.Data.ParameterDirection.Output;
            SqlParameter entered = command.Parameters.Add("@Entered", System.Data.SqlDbType.DateTime);
            entered.Direction = System.Data.ParameterDirection.Output;
            DataAccessHelper.AddWithValue(command, "@JobID", int.Parse(jobDelay.JobID.ToString()));
            DataAccessHelper.AddWithValue(command, "@DelayedBy", jobDelay.DelayedBy);
            DataAccessHelper.AddWithValue(command, "@DelayLength", jobDelay.DelayLength);
            if (jobDelay.DelayedByEmployeeID == null)
            {
                DataAccessHelper.AddWithValue(command, "@DelayedByEmployeeID", DBNull.Value);
            }
            else
            {
                DataAccessHelper.AddWithValue(command, "@DelayedByEmployeeID", int.Parse(jobDelay.DelayedByEmployeeID.ToString()));
            }
            if (jobDelay.DelayedBySubcontractorID == null)
            {
                DataAccessHelper.AddWithValue(command, "@DelayedBySubcontractorID", DBNull.Value);
            }
            else
            {
                DataAccessHelper.AddWithValue(command, "@DelayedBySubcontractorID", int.Parse(jobDelay.DelayedBySubcontractorID.ToString()));
            }
            DataAccessHelper.AddWithValue(command, "@Reason", jobDelay.Reason);
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                result = new JobDelay();
                jobDelay.CopyTo(result);
                result.Identifier = (int)jobDelayID.Value;
                result.Entered = (DateTime)entered.Value;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }


        public static JobDelay Read(object identifier)
        {
            JobDelay result = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobDelayRead";
            DataAccessHelper.AddWithValue(command, "@JobDelayID", int.Parse(identifier.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result = new JobDelay();
                    result.DelayedBy = DataAccessHelper.FieldStringValue(dataReader, "DelayedBy");
                    result.DelayedByEmployeeID = DataAccessHelper.FieldIntValue(dataReader, "DelayedByEmployeeID");
                    result.DelayedBySubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "DelayedBySubcontractorID");
                    result.DelayLength = DataAccessHelper.FieldIntValue(dataReader, "DelayLength").Value;
                    result.Entered = DataAccessHelper.FieldDateTimeValue(dataReader, "Entered").Value;
                    result.Identifier = DataAccessHelper.FieldIntValue(dataReader, "JobDelayedID");
                    result.JobID = DataAccessHelper.FieldIntValue(dataReader, "JobID");
                    result.Reason = DataAccessHelper.FieldStringValue(dataReader, "Reason");
                }
            }
            finally
            {
                connection.Close();
            }                       
            return result;
        }

        public static List<JobDelay> List(object jobID)
        {
            List<JobDelay> result = new List<JobDelay>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobDelayList";
            DataAccessHelper.AddWithValue(command, "@JobID", int.Parse(jobID.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    JobDelay jobDelay = new JobDelay();
                    jobDelay.DelayedBy = DataAccessHelper.FieldStringValue(dataReader, "DelayedBy");
                    jobDelay.DelayedByEmployeeID = DataAccessHelper.FieldIntValue(dataReader, "DelayedByEmployeeID");
                    jobDelay.DelayedBySubcontractorID = DataAccessHelper.FieldIntValue(dataReader, "DelayedBySubcontractorID");
                    jobDelay.Entered = DataAccessHelper.FieldDateTimeValue(dataReader, "Entered").Value;
                    jobDelay.Identifier = DataAccessHelper.FieldIntValue(dataReader, "JobDelayedID");
                    jobDelay.JobID = DataAccessHelper.FieldIntValue(dataReader, "JobID");
                    jobDelay.Reason = DataAccessHelper.FieldStringValue(dataReader, "Reason");
                    result.Add(jobDelay);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void AssociateJobTask(object jobDelayID, object jobTaskID, DateTime originalStart, DateTime originalFinish, DateTime start, DateTime finish)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobDelayJobTaskAssociate";
            DataAccessHelper.AddWithValue(command, "@JobDelayID", int.Parse(jobDelayID.ToString()));
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTaskID.ToString()));
            DataAccessHelper.AddWithValue(command, "@OriginalStart", originalStart);
            DataAccessHelper.AddWithValue(command, "@OriginalFinish", originalFinish);
            DataAccessHelper.AddWithValue(command, "@Start", start);
            DataAccessHelper.AddWithValue(command, "@Finish", finish);           
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<JobTaskDelay> ListJobTaskDelay(object jobID)
        {
            List<JobTaskDelay> result = new List<JobTaskDelay>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobTaskDelayList";
            DataAccessHelper.AddWithValue(command, "@JobID", int.Parse(jobID.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    JobTaskDelay jobTaskDelay = new JobTaskDelay();
                    jobTaskDelay.Entered = DataAccessHelper.FieldDateTimeValue(dataReader, "Entered").Value;
                    jobTaskDelay.Finish = DataAccessHelper.FieldDateTimeValue(dataReader, "Finish").Value;
                    jobTaskDelay.JobDelayID = DataAccessHelper.FieldIntValue(dataReader, "JobDelayID");
                    jobTaskDelay.JobTaskID = DataAccessHelper.FieldIntValue(dataReader, "JobTaskID");
                    jobTaskDelay.OriginalFinish = DataAccessHelper.FieldDateTimeValue(dataReader, "OriginalFinish").Value;
                    jobTaskDelay.OriginalStart = DataAccessHelper.FieldDateTimeValue(dataReader, "OriginalStart").Value;
                    jobTaskDelay.Start = DataAccessHelper.FieldDateTimeValue(dataReader, "Start").Value;
                    result.Add(jobTaskDelay);
                }
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void DissociateJobTask(object jobDelayID, object jobTaskID)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BuildOnTime"].ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "dalJobDelayJobTaskDissociate";
            DataAccessHelper.AddWithValue(command, "@JobDelayID", int.Parse(jobDelayID.ToString()));
            DataAccessHelper.AddWithValue(command, "@JobTaskID", int.Parse(jobTaskID.ToString()));
            command.Connection = connection;
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
