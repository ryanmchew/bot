﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuildOnTime.BusinessLogic;
using BuildOnTime.BusinessObject;

namespace BuildOnTime.Presentation
{
    public class CalendarEvent
    {
        public CalendarEvent() { }

        public CalendarEvent(string jobID, object jobTaskID, DateTime start, DateTime end, int duration, string display)
        {
            JobID = jobID;
            JobTaskID = jobTaskID;
            Start = start;
            Duration = duration;
            End = end;
            Display = display;
        }
        public string JobID { get; set; }
        public object JobTaskID { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Duration { get; set; }
        public string Display { get; set; }
        public string WorkerID { get; set; }
        public string TaskName { get; set; }
    }

    public class JobEvent : CalendarEvent
    {
        public JobEvent()
        {
            Dependents = new List<string>();
        }

        public List<string> Dependents { get; set; }
    }

    public static class CalendarEventPLL
    {
        public static List<CalendarEvent> CalendarEvents(Employee employee, DateTime start, DateTime end)
        {
            List<CalendarEvent> calendarEvents = new List<CalendarEvent>();
            List<JobTask> jobTasks = JobTaskBLL.SearchSupervisorTask(employee.Identifier, start, end);
            List<Job> jobs = JobBLL.ActiveJobs(employee.Builder.Identifier);
            foreach (JobTask jobTask in jobTasks)
            {
                string jobName = jobs.Find(delegate(Job j) { return (j.Identifier.Equals(jobTask.JobID)); }).Name;
                calendarEvents.Add(new CalendarEvent(jobTask.JobID.ToString(), jobTask.Identifier, jobTask.Start, jobTask.Finish, jobTask.Duration, jobName + "-" + jobTask.TaskName));
            }
            return calendarEvents;
        }

        public static List<CalendarEvent> SchedulingEvents(Builder builder, DateTime start, DateTime end)
        {
            List<CalendarEvent> calendarEvents = new List<CalendarEvent>();
            List<JobTask> jobTasks = JobTaskBLL.SearchBuilderTask(builder.Identifier, start, end);
            List<Job> jobs = JobBLL.ActiveJobs(builder.Identifier);
            List<Worker> workerList = WorkerPLL.List(builder);
            foreach (JobTask jobTask in jobTasks)
            {
                Worker worker = workerList.Find(delegate(Worker wk) { return wk.Identifier.Equals(jobTask.WorkerID); });
                calendarEvents.Add(new CalendarEvent(jobTask.JobID.ToString(), jobTask.Identifier, jobTask.Start, jobTask.Finish, jobTask.Duration, jobTask.TaskName + " (" + ((worker == null) ? "Not assigned" : worker.Name) + ")"));
            }
            return calendarEvents;
        }

        public static List<CalendarEvent> SchedulingEvents(Employee employee, DateTime start, DateTime end)
        {
            List<CalendarEvent> calendarEvents = new List<CalendarEvent>();
            List<JobTask> jobTasks = JobTaskBLL.SearchSupervisorTask(employee.Identifier, start, end);
            List<Job> jobs = JobBLL.ActiveJobs(employee.Builder.Identifier);
            List<Worker> workerList = WorkerPLL.List(employee.Builder);
            foreach (JobTask jobTask in jobTasks)
            {
                Worker worker = workerList.Find(delegate(Worker wk) { return wk.Identifier.Equals(jobTask.WorkerID); });
                calendarEvents.Add(new CalendarEvent(jobTask.JobID.ToString(), jobTask.Identifier, jobTask.Start, jobTask.Finish, jobTask.Duration, jobTask.TaskName + " (" + ((worker == null) ? "Not assigned" : worker.Name) + ")"));
            }
            return calendarEvents;
        }

        public static List<JobEvent> JobEvents(Employee employee, object jobID)
        {
            List<JobEvent> calendarEvents = new List<JobEvent>();
            List<JobTask> jobTasks = JobTaskBLL.List(jobID);
            List<JobTaskDependency> dependents = JobTaskBLL.TaskDependencyList(jobID);
            List<Job> jobs = JobBLL.ActiveJobs(employee.Builder.Identifier);
            List<Worker> workerList = WorkerPLL.List(employee.Builder);
            foreach (JobTask jobTask in jobTasks)
            {
                Worker worker = workerList.Find(delegate(Worker wk) { return wk.Identifier.Equals(jobTask.WorkerID); });
                List<string> dependentIDs = new List<string>();
                IEnumerable<string> ids = from d in dependents
                                          where d.ParentID.Equals(jobTask.Identifier)
                                          select d.DependentID.ToString();
                dependentIDs.AddRange(ids);
                calendarEvents.Add(new JobEvent { JobID = jobTask.JobID.ToString(), JobTaskID = jobTask.Identifier, Start = jobTask.Start, Duration = jobTask.Duration, End = jobTask.Finish, Display = jobTask.TaskName + " (" + ((worker == null) ? "Not assigned" : worker.Name) + ")", TaskName = jobTask.TaskName, WorkerID = (worker == null) ? "" : worker.Identifier, Dependents = dependentIDs });
            }
            return calendarEvents;
        }
    }
}
