﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuildOnTime.BusinessLogic;
using BuildOnTime.BusinessObject;

namespace BuildOnTime.Presentation
{
    public enum ViewStatusEnum
    {
        New,
        Recent,
        Old,
        NewUpdate,
        RecentUpdate,
        OldUpdate
    }

    public class SubJobTaskDisplay : JobTask
    {
        public string BuilderName { get; set; }
        public string JobName { get; set; }
        public string SuperName { get; set; }
        public ViewStatusEnum ViewStatus { get; set; }
        public DateTime? PreviousStart { get; set; }
    }

    public class JobTaskDisplay
    {
        public string Display { get; set; }
        public object JobID { get; set; }
        public string JobName { get; set; }
        public object Identifier { get; set; }
        public string TaskName { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public object SubcontractorID { get; set; }
        public string SubcontractorName { get; set; }

        public JobTaskDisplay(object subcontractorID, string subcontractorName, object jobID, string jobName, object identifier, string display, string taskName, DateTime start, DateTime finish)
        {
            this.Display = display;
            this.JobID = jobID;
            this.JobName = jobName;
            this.Identifier = identifier;
            this.TaskName = taskName;
            this.Start = start;
            this.Finish = finish;
            this.SubcontractorID = subcontractorID;
            this.SubcontractorName = subcontractorName;
        }
    }

    public class SubcontractorLoad
    {
        public object SubcontractorIdentifier { get; set; }
        public string SubcontractorName { get; set; }
        public List<JobTaskDisplay> JobTasks { get; set; }

        public int[] MonthView { get; set; }

        public SubcontractorLoad()
        {
            JobTasks = new List<JobTaskDisplay>();
            MonthView = new int[31];
        }
    }

    public static class SubcontractorPLL
    {
        private static int CompareSubLoads(SubcontractorLoad sl1, SubcontractorLoad sl2)
        {
            int result = 0;
            result = sl2.JobTasks.Count.CompareTo(sl1.JobTasks.Count);
            if (result == 0)
            {
                result = sl1.SubcontractorName.ToUpper().CompareTo(sl2.SubcontractorName.ToUpper());
            }
            return result;
        }

        public static List<JobTaskDisplay> SubcontractorTasks(object builderID, DateTime start, DateTime finish, out List<SubcontractorLoad> subcontractorLoads)
        {
            List<JobTaskDisplay> jobTaskDisplay = new List<JobTaskDisplay>();
            List<Subcontractor> subcontractorList = SubcontractorBLL.List(builderID);
            subcontractorLoads = new List<SubcontractorLoad>();
            foreach (Subcontractor sub in subcontractorList)
            {
                SubcontractorLoad subLoad = new SubcontractorLoad();
                subLoad.SubcontractorIdentifier = sub.Identifier;
                subLoad.SubcontractorName = sub.Name;
                subcontractorLoads.Add(subLoad);
            }
            List<JobTask> jobTasks = JobTaskBLL.SearchBuilderTask(builderID, start, finish);
            List<Job> jobs = JobBLL.Search(builderID, start);
            //jobTasks.Sort(delegate(JobTask jt1, JobTask jt2) { return ((jt1.Start.CompareTo(jt2.Start) == 0) ? jt1.Finish.CompareTo(jt2.Finish) : jt1.Start.CompareTo(jt2.Start)); });
            foreach (JobTask task in jobTasks)
            {
                SubcontractorLoad subLoad = subcontractorLoads.Find(delegate(SubcontractorLoad sl) { return (task.SubcontractorID == null) ? false : task.SubcontractorID.ToString().Equals(sl.SubcontractorIdentifier.ToString()); });
                if (subLoad != null)
                {
                    Job job = (task.JobID == null) ? null : jobs.Find(delegate(Job jb) { return task.JobID.ToString().Equals(jb.Identifier.ToString()); });
                    subLoad.JobTasks.Add(new JobTaskDisplay(subLoad.SubcontractorIdentifier, subLoad.SubcontractorName, task.JobID, ((job == null) ? "" : job.Name), task.Identifier, job.Name + " - " + task.TaskName, task.TaskName, task.Start, task.Finish));
                }
            }
            subcontractorLoads.Sort(CompareSubLoads);
            foreach (SubcontractorLoad load in subcontractorLoads)
            {
                if (load.JobTasks.Count > 0)
                {
                    jobTaskDisplay.AddRange(load.JobTasks);
                }
            }
            return jobTaskDisplay;
        }

        private static Builder FindBuilder(object builderID, List<Builder> builderList)
        {
            Builder builder = builderList.Find(delegate(Builder bldr) { return bldr.Identifier.ToString().Equals(builderID); });
            if (builder == null)
            {
                builder = BuilderBLL.Read(builderID);
                builderList.Add(builder);
            }
            return builder;
        }

        private static Job FindJob(object jobID, List<Job> jobList)
        {
            Job job = jobList.Find(delegate(Job jb) { return jb.Identifier.ToString().Equals(jobID.ToString()); });
            if (job == null)
            {
                job = JobBLL.Read(jobID);
                jobList.Add(job);
            }
            return job;
        }

        private static Employee FindSuper(object superID, List<Employee> superList)
        {
            Employee super = superList.Find(delegate(Employee emp) { return emp.Identifier.ToString().Equals(superID); });
            if (super == null)
            {
                super = EmployeeBLL.Read(superID);
                superList.Add(super);
            }
            return super;
        }

        private static List<JobTaskDelay> JobDelays(Job job, Dictionary<Job, List<JobTaskDelay>> jobDelays)
        {
            List<JobTaskDelay> result = null;
            if (!jobDelays.TryGetValue(job, out result))
            {
                result = JobDelayBLL.ListJobTaskDelay(job.Identifier);
            }
            result.Sort(JobDelayBLL.JobTaskDelayCompareMostRecent);
            return result;
        }

        public static List<SubJobTaskDisplay> SubcontractorTasks(Builder builder, Subcontractor subcontractor, DateTime start, DateTime finish)
        {
            List<SubJobTaskDisplay> jobTaskDisplays = new List<SubJobTaskDisplay>();
            List<JobTask> jobTasks = JobTaskBLL.SearchSubcontractorTask(subcontractor.Identifier, start, finish);
            foreach (JobTask task in jobTasks)
            {
                if (task.BuilderID.Equals(builder.Identifier))
                {
                    SubJobTaskDisplay jobTaskDisplay = new SubJobTaskDisplay();
                    task.CopyTo(jobTaskDisplay);
                    jobTaskDisplays.Add(jobTaskDisplay);
                }
            }
            return jobTaskDisplays;
        }

        public static List<SubJobTaskDisplay> SubcontractorTaskList(SystemUser systemUser, Subcontractor subcontractor, DateTime start, DateTime finish)
        {
            List<SubJobTaskDisplay> jobTaskDisplays = new List<SubJobTaskDisplay>();
            // Hold all the builders that one sub may have.
            List<Builder> builders = new List<Builder>();
            // Hold all the supers that one sub may have.
            List<Employee> supers = new List<Employee>();
            // Hold all the jobs that one sub might be working on.
            List<Job> jobs = new List<Job>();
            // List of recent updates
            Dictionary<Job, List<JobTaskDelay>> jobDelays = new Dictionary<Job, List<JobTaskDelay>>();
            // List audit trail; used to determine view status.
            //List<SystemUserAudit> taskViews = SystemUserAuditBLL.FindRecent(systemUser.Identifier, AuditEnum.View, AuditTargetEnum.TaskList);
            List<SystemUserAudit> loginAudits = SystemUserAuditBLL.FindRecent(systemUser.Identifier, AuditEnum.Login, AuditTargetEnum.Subcontractor);

            // This sorts so we should stay in sorted order.
            List<JobTask> jobTasks = JobTaskBLL.SearchSubcontractorTask(subcontractor.Identifier, start, finish);
            jobTasks.Sort(JobTaskBLL.CompareJobTaskByStartDay);
            foreach (JobTask task in jobTasks)
            {
                // Find job
                Job job = FindJob(task.JobID, jobs);
                // Find builder
                Builder builder = FindBuilder(job.BuilderID, builders);
                // Find super
                Employee super = FindSuper(job.SuperID, supers);
                // Find delays
                List<JobTaskDelay> delays = JobDelays(job, jobDelays);
                // Copy job information for delay purposes.
                SubJobTaskDisplay jobTaskDisplay = new SubJobTaskDisplay();
                task.CopyTo(jobTaskDisplay);
                jobTaskDisplay.JobName = job.Name;
                jobTaskDisplay.SuperName = EmployeeBLL.DisplayName(super);
                jobTaskDisplay.BuilderName = builder.Name;
                // If not accepted is in new status.
                jobTaskDisplay.ViewStatus = ViewStatusEnum.New;
                DateTime? loginThreshold = null;
                /*
                                if (loginAudits.Count > 2)
                                {
                                    loginThreshold = loginAudits[2].Occurred;
                                }
                 * else 
                 */
                if (loginAudits.Count > 1)
                {
                    loginThreshold = loginAudits[1].Occurred;
                }
                //                if (jobTaskDisplay.AssignmentStatus != TaskAssignmentStatusEnum.Assigned)
                {
                    if (delays.Exists(delegate(JobTaskDelay jtd) { return (jtd.JobTaskID.ToString().Equals(task.Identifier.ToString(), StringComparison.OrdinalIgnoreCase)); }))
                    {
                        JobTaskDelay delay = delays.Find(delegate(JobTaskDelay jtd) { return (jtd.JobTaskID.ToString().Equals(task.Identifier.ToString(), StringComparison.OrdinalIgnoreCase)); });
                        jobTaskDisplay.PreviousStart = delay.OriginalStart;
                        if (loginThreshold.HasValue)
                        {
                            JobTaskDelay taskDelay = delays.Find(delegate(JobTaskDelay jtd) { return (jtd.JobTaskID.ToString().Equals(task.Identifier.ToString(), StringComparison.OrdinalIgnoreCase) && (jtd.Entered.CompareTo(loginThreshold.Value) > 0)); });
                            if (taskDelay != null)
                            {
                                jobTaskDisplay.ViewStatus = ViewStatusEnum.NewUpdate;
                            }
                            else
                            {
                                DateTime threeWorkDaysBack = CalendarBLL.FindWorkDate(DateTime.Now.Date, -3);
                                taskDelay = delays.Find(delegate(JobTaskDelay jtd) { return (jtd.JobTaskID.ToString().Equals(task.Identifier.ToString(), StringComparison.OrdinalIgnoreCase) && (jtd.Entered.CompareTo(threeWorkDaysBack) > 0)); });
                                if (taskDelay != null)
                                {
                                    jobTaskDisplay.ViewStatus = ViewStatusEnum.RecentUpdate;
                                }
                                else
                                {
                                    jobTaskDisplay.ViewStatus = ViewStatusEnum.Old;
                                }
                            }
                        }
                    }
                    else
                    {
                        jobTaskDisplay.PreviousStart = null;
                        if (loginThreshold.HasValue)
                        {
                            if (jobTaskDisplay.Updated.CompareTo(loginThreshold.Value) > 0)
                            {
                                jobTaskDisplay.ViewStatus = ViewStatusEnum.New;
                            }
                            else
                            {
                                DateTime threeWorkDaysBack = CalendarBLL.FindWorkDate(DateTime.Now.Date, -3);
                                if (jobTaskDisplay.Updated.CompareTo(threeWorkDaysBack) > 0)
                                {
                                    jobTaskDisplay.ViewStatus = ViewStatusEnum.Recent;
                                }
                                else
                                {
                                    jobTaskDisplay.ViewStatus = ViewStatusEnum.Old;
                                }
                            }
                        }
                    }
                }
                jobTaskDisplays.Add(jobTaskDisplay);
            }
            return jobTaskDisplays;
        }
    }
}
