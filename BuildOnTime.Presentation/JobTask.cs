﻿using System;
using System.Collections.Generic;
using System.Text;
using BuildOnTime.BusinessObject;
using BuildOnTime.BusinessLogic;

namespace BuildOnTime.Presentation
{
    public class JobTaskJobTaskDelay: JobTaskSelect
    {
        public DateTime OriginalStart { get; set; }
        public DateTime OriginalFinish { get; set; }
        public bool Updated { get; set; }
    }

    public class JobTaskDelaySelect: JobTaskDelay
    {
        public JobTaskSelect JobTask {get; set;}
        public bool Selected { get; set; }
    }

    public class JobTaskSelect: JobTask
    {
        public bool Selected { get; set; }
    }

    public static class JobTaskSelectPLL
    {
        public static List<JobTaskSelect> List(object jobID)
        {
            List<JobTaskSelect> result = new List<JobTaskSelect>();
            List<JobTask> jobTasks = JobTaskBLL.List(jobID);
            foreach (JobTask jobTask in jobTasks)
            {
                JobTaskSelect jobTaskView = new JobTaskSelect();
                
                jobTask.CopyTo(jobTaskView);
                result.Add(jobTaskView);
            }
            result.Sort(JobTaskBLL.CompareJobTaskByStartDay);
            return result;
        }        
    }
}
