﻿using System;
using System.Collections.Generic;
using System.Text;
using BuildOnTime.BusinessObject;
using BuildOnTime.BusinessLogic;

namespace BuildOnTime.Presentation
{
    public static class WorkerPLL
    {
        public static int CompareWorkerByName(Worker worker1, Worker worker2)
        {
            return worker1.Name.CompareTo(worker2.Name);
        }

        public static List<Worker> List(Builder builder)
        {
            List<Worker> workerList = new List<Worker>();
            List<Subcontractor> subcontractors = SubcontractorBLL.List(builder.Identifier);
            foreach (Subcontractor sub in subcontractors)
            {
                workerList.Add(new Worker(sub.Name, WorkerEnum.Subcontractor, sub.Identifier.ToString()));
            }

            List<Employee> employees = EmployeeBLL.List(builder);
            foreach (Employee emp in employees)
            {
                workerList.Add(new Worker(emp.Name, WorkerEnum.Employee, emp.Identifier.ToString()));
            }
            workerList.Sort(CompareWorkerByName);
            return workerList;
        }
    }
    /*
        public class WorkerList : List<Worker>
        {
            public Worker FindEmployeeWorker(string employeeID)
            {
                return Find(WorkerPLL.WorkerID(WorkerEnum.Employee, employeeID));
            }

            public Worker FindSubcontractorWorker(string subcontractorID)
            {
                return Find(WorkerPLL.WorkerID(WorkerEnum.Subcontractor, subcontractorID));
            }

            public Worker Find(WorkerEnum workerType, string workerID)
            {
                switch (workerType)
                {
                    case WorkerEnum.Employee: return FindEmployeeWorker(workerID);
                        break;
                    case WorkerEnum.Subcontractor: return FindSubcontractorWorker(workerID);
                        break;
                    default: return null;
                }
            }

            public Worker Find(string workerID)
            {
                return this.Find(delegate(Worker w) { return (w.Identifier == null) ? false : w.Identifier.Equals(workerID); });
            }
        }

        public static class WorkerPLL
        {
            public static string WorkerID(JobTask jobTask)
            {
                // Should mainly be subs.
                string result = (jobTask.SubcontractorID != null) ? WorkerID(WorkerEnum.Subcontractor, jobTask.SubcontractorID.ToString()): null;
                // If not check employee.
                if (result == null)
                {
                    result = (jobTask.EmployeeID != null) ? WorkerID(WorkerEnum.Employee, jobTask.EmployeeID.ToString()) : null;
                }
                return result;
            }

            public static string WorkerID(WorkerEnum workerType, string workerID)
            {
                switch (workerType)
                {
                    case WorkerEnum.Employee: workerID = EMPLOYEE_PREFIX.ToString() + workerID;
                        break;
                    case WorkerEnum.Subcontractor: workerID = SUBCONTRACTOR_PREFIX.ToString() + workerID;
                        break;
                }
                return workerID;
            }

            public static string EmployeeID(string workerID)
            {
                return (!workerID.Contains(EMPLOYEE_PREFIX) ? null: workerID.TrimStart(EMPLOYEE_PREFIX.ToCharArray()));
            }

            public static string SubcontractorID(string workerID)
            {
                return (!workerID.Contains(SUBCONTRACTOR_PREFIX) ? null: workerID.TrimStart(SUBCONTRACTOR_PREFIX.ToCharArray()));
            }

            public static WorkerList List(Builder builder)
            {
                WorkerList workerList = new WorkerList();
                workerList.Add(new Worker("<No default>", null));
                List<Subcontractor> subcontractors = SubcontractorBLL.List(builder.Identifier);
                foreach (Subcontractor sub in subcontractors)
                {
                    workerList.Add(new Worker(sub.Name, SUBCONTRACTOR_PREFIX + sub.Identifier.ToString()));
                }

                List<Employee> employees = EmployeeBLL.List(builder);
                foreach (Employee emp in employees)
                {
                    workerList.Add(new Worker(emp.LastName + ", " + emp.FirstName, EMPLOYEE_PREFIX + emp.Identifier.ToString()));
                }
                return workerList;
            }
        }

        public class EmployeeCommunicae : Communication
        {
            public object EmployeeID { get; set; }
        }

        public class SubcontractorCommunicae : Communication
        {
            public object SubcontractorID { get; set; }
            public string ConfirmedBy { get; set; }
        }
    */
    public class CommunicationEMailTask
    {
        public CommunicationEMailTask(JobTask jobTask, Employee employee, CommunicationReasonEnum reason)
        {
            this.JobTask = jobTask;
            this.Employee = employee;
            this.Reason = reason;
        }

        public JobTask JobTask { get; set; }
        public Employee Employee { get; set; }
        public CommunicationReasonEnum Reason { get; set; }       
    }

    public static class CommunicationPLL
    {
        public static void SubcontractorEmail(List<CommunicationEMailTask> communicationEMailTask)
        {            
            if (communicationEMailTask.Count > 0)
            {
                communicationEMailTask.Sort(delegate(CommunicationEMailTask cet1, CommunicationEMailTask cet2) { return (cet1.JobTask.SubcontractorID.ToString().CompareTo(cet2.JobTask.SubcontractorID.ToString())); });
                int currentIndex = 0;
                string subcontractorID = null;
                List<CommunicationEMailTask> communicationNeeds = new List<CommunicationEMailTask>();
                List<JobTask> jobTasks = new List<JobTask>();
                while (currentIndex < communicationEMailTask.Count)
                {                    
                    if (communicationEMailTask[currentIndex].JobTask.SubcontractorID != null)
                    {
                        subcontractorID = communicationEMailTask[currentIndex].JobTask.SubcontractorID.ToString();
                        communicationNeeds.Clear();
                        communicationNeeds.Add(communicationEMailTask[currentIndex]);
                        // Loop through and grab all e-mails that need to be sent for one subcontractor.
                        while ((currentIndex + 1 < communicationEMailTask.Count) && communicationEMailTask[currentIndex + 1].JobTask.SubcontractorID.ToString().Equals(subcontractorID.ToString()))
                        {
                            currentIndex++;
                            communicationNeeds.Add(communicationEMailTask[currentIndex]);
                        }
                        Employee employee = communicationEMailTask[currentIndex].Employee;
                        EMail email = CommunicationBLL.SubCompositeEmail(communicationEMailTask[currentIndex].JobTask, employee.SystemUser);
                        SubcontractorCommunicate(communicationEMailTask[currentIndex].Employee.Builder.Identifier, subcontractorID, null, email.Identifier, employee.Identifier, CommunicationReasonEnum.Composite, CommunicationMethodEnum.EMail, email.To);
                    }
                    currentIndex++;
                }
            }
        }

        public static void SubcontractorEmail(JobTask jobTask, Employee employee, CommunicationReasonEnum reason)
        {
            EMail email = null;
            switch (reason)
            {
                case CommunicationReasonEnum.Scheduling:
                    email = CommunicationBLL.SubJobTaskRequest(jobTask, employee.SystemUser);                    
                    break;
                case CommunicationReasonEnum.Reminding:
                    email = CommunicationBLL.SubJobTaskReminder(jobTask, employee.SystemUser);
                    break;
                case CommunicationReasonEnum.Rescheduling:
                    email = CommunicationBLL.SubJobTaskUpdate(jobTask, employee.SystemUser);
                    break;
            }
            SubcontractorCommunicate(employee.Builder.Identifier, jobTask.SubcontractorID, jobTask.Identifier, email.Identifier, employee.Identifier, reason, CommunicationMethodEnum.EMail, email.To);
        }

        public static void SubcontractorCommunicate(object builderID, object subcontractorID, object jobTaskID, object emailID, object employeeCommunicatorID, CommunicationReasonEnum reason, CommunicationMethodEnum method, string channel)
        {
            BuildOnTime.BusinessObject.SubcontractorCommunication communication = new BuildOnTime.BusinessObject.SubcontractorCommunication();
            communication.BuilderID = builderID;
            communication.Channel = channel;
            communication.EMailID = emailID;
            communication.EmployeeCommunicatorID = employeeCommunicatorID;
            communication.JobTaskID = jobTaskID;
            communication.Method = method;
            communication.Notes = null;
            communication.Reason = reason;
            communication.SubcontractorID = subcontractorID;
            SubcontractorCommunicationBLL.Create(communication);
        }

        public static void EmployeeCommunicate(object builderID, object employeeID, object jobTaskID, object emailID, object employeeCommunicatorID, CommunicationReasonEnum reason, CommunicationMethodEnum method, string channel)
        {
            BuildOnTime.BusinessObject.EmployeeCommunication communication = new BuildOnTime.BusinessObject.EmployeeCommunication();
            communication.BuilderID = builderID;
            communication.Channel = channel;
            communication.EMailID = emailID;
            communication.EmployeeCommunicatorID = employeeCommunicatorID;
            communication.EmployeeID = employeeID;
            communication.JobTaskID = jobTaskID;
            communication.Method = method;
            communication.Notes = null;
            communication.Reason = reason;
            EmployeeCommunicationBLL.Create(communication);
        }
    }
}
