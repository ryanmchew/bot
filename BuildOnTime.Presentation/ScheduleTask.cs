﻿using System;
using System.Collections.Generic;
using System.Text;
using BuildOnTime.BusinessObject;
using BuildOnTime.BusinessLogic;

namespace BuildOnTime.Presentation
{
    public class ScheduleTaskDisplay : ScheduleTask
    {
        public string WorkerName { get; set; }
    }

    public class ScheduleTaskDisplayList : List<ScheduleTaskDisplay>
    {
        public List<Worker> Workers { get; set; }

        private ScheduleTaskDisplay CreateScheduleTaskDisplay(ScheduleTask task)
        {
            ScheduleTaskDisplay displayTask = new ScheduleTaskDisplay();
            task.CopyTo(displayTask);
            if ((task.DefaultEmployeeID != null))
            {
                displayTask.WorkerName = Workers.Find(delegate(Worker w){ return (w.Identifier == null) ? false: w.Identifier.Equals(Worker.WorkerID(WorkerEnum.Employee, task.DefaultEmployeeID)); }).Name;
            }
            if (task.DefaultSubcontractorID != null)
            {
                displayTask.WorkerName = Workers.Find(delegate(Worker w) { return (w.Identifier == null) ? false: w.Identifier.Equals(Worker.WorkerID(WorkerEnum.Subcontractor, task.DefaultSubcontractorID)); }).Name; 
            }
            return displayTask;
        }

        public ScheduleTaskDisplayList(Employee employee, string scheduleID)
        {
            this.Workers = WorkerPLL.List(employee.Builder);
            List<ScheduleTask> scheduleTasks = ScheduleTaskBLL.List(scheduleID);
            foreach (ScheduleTask task in scheduleTasks)
            {
                this.Add(CreateScheduleTaskDisplay(task));
            }
            this.Sort(ScheduleTaskBLL.CompareScheduleTaskByStartDay);
        }
    }
}
