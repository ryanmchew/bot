﻿using System;
using System.Collections.Generic;
using System.Text;
using BuildOnTime.BusinessObject;
using BuildOnTime.BusinessLogic;

namespace BuildOnTime.Presentation
{
    public class JobTaskView
    {
        public TaskStatusEnum Status { get; set; }
        public TaskAssignmentStatusEnum AssignmentStatus { get; set; }
        public object JobID { get; set; }
        public object JobTaskID { get; set; }
        public string WorkerID { get; set; }
        public string JobName { get; set; }
        public string TaskName { get; set; }
        public string WorkerName { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public DateTime? Finished { get; set; }
        public DateTime? Reviewed { get; set; }
        public string Channel { get; set; }
        public string Channel2 { get; set; }
        public string Channel3 { get; set; }
        public string Channel4 { get; set; }

        public JobTaskView(JobTask jobTask, string jobName, string workerName, string channel, string channel2, string channel3, string channel4)
        {
            this.Finish = jobTask.Finish;
            this.Finished = (jobTask.WorkerFinish != null) ? jobTask.WorkerFinish : jobTask.Finish;
            this.JobID = jobTask.JobID;
            this.JobName = jobName;
            this.JobTaskID = jobTask.Identifier;
            this.Reviewed = jobTask.Signoff;
            this.Start = jobTask.Start;
            this.Status = jobTask.Status;
            this.TaskName = jobTask.TaskName;
            this.WorkerID = jobTask.WorkerID;
            this.WorkerName = workerName;
            this.Channel = channel;
            this.Channel2 = channel2;
            this.Channel3 = channel3;
            this.Channel4 = channel4;
        }
    }

    public enum TaskActionEnum
    {
        Assign,
        NotAccepted,
        Accept,
        Reject,
        Reschedule,
        Remind,
        Finish,
        Review
    }

    public class TaskAction : JobTaskView
    {
        public TaskAction(JobTask jobTask, string jobName, TaskActionEnum action, string workerName, string channel, string channel2, string channel3, string channel4)
            : base(jobTask, jobName, workerName, channel, channel2, channel3, channel4)
        {
            this.Action = action;
        }

        public TaskActionEnum Action { get; set; }
    }

    public class TaskCommunicationAction : JobTaskView
    {
        public TaskCommunicationAction(JobTask jobTask, string jobName, CommunicationReasonEnum reason, string workerName, string channel, string channel2, string channel3, string channel4)
            : base(jobTask, jobName, workerName, channel, channel2, channel3, channel4)
        {
            Reason = reason;
        }

        public CommunicationReasonEnum Reason { get; set; }
    }

    public class JobCommunicationsDelays : Job
    {
        public List<EmployeeCommunication> EmployeeCommunications { get; set; }
        //public List<SubcontractorCommunication> SubcontractorCommunications { get; set; }
        public List<JobTaskDelay> Delays { get; set; }
    }

    public class SubcontractorWithCommunication: Subcontractor
    {
        public SubcontractorWithCommunication()
        {
            Communications = new List<SubcontractorCommunication>();
        }

        public SubcontractorWithCommunication(Subcontractor sub)
        {
            this.AddressLine1 = sub.AddressLine1;
            this.AddressLine2 = sub.AddressLine2;
            this.BuilderID = sub.BuilderID;
            this.City = sub.City;
            this.Communications = new List<SubcontractorCommunication>();
            this.Identifier = sub.Identifier;
            this.Name = sub.Name;
            this.OfficeFax = sub.OfficeFax;
            this.OfficePhone = sub.OfficePhone;
            this.PostalCode = sub.PostalCode;
            this.State = sub.State;
        }

        public List<SubcontractorCommunication> Communications { get; set; }
    }

    public class DailyTaskList
    {
        private Employee supervisor;
        private List<JobCommunicationsDelays> allJobs;
        private List<JobTask> allJobTasks;
        private List<SubcontractorWithCommunication> allSubcontractors;
        private List<Employee> allEmployees;

        public List<TaskAction> Assignments { get; set; }
        public List<TaskAction> NotAccepted { get; set; }
        public List<TaskAction> Reviews { get; set; }
        public List<TaskAction> InProcess { get; set; }
        public List<TaskCommunicationAction> PhoneCalls { get; set; }
        public List<TaskCommunicationAction> EMails { get; set; }

        private void FindTasks()
        {
            // Look back 5 days for task that may not have been reviewed.
            DateTime startDate = CalendarBLL.FindWorkDate(DateTime.Now.Date, -5);
            DateTime endDate = CalendarBLL.FindWorkDate(DateTime.Now.Date, 30);
            allJobTasks = JobTaskBLL.SearchSupervisorTask(supervisor.Identifier, startDate, endDate);
        }

        public int Compare(JobTaskView jtv1, JobTaskView jtv2)
        {
            int result = jtv1.Start.CompareTo(jtv2.Start);
            if (result == 0)
            {
                result = jtv1.Finish.CompareTo(jtv2.Finish);
                if (result == 0)
                {
                    result = jtv1.JobName.CompareTo(jtv2.JobName);
                    if (result == 0)
                    {
                        result = jtv1.TaskName.CompareTo(jtv2.TaskName);
                    }
                }
            }
            return result;
        }

        private void FindWorker(JobTask jobTask, out string workerName, out string channel, out string channel2, out string channel3, out string channel4, out CommunicationMethodEnum? method)
        {
            method = null;
            workerName = null;
            channel = null;
            channel2 = null;
            channel3 = null;
            channel4 = null;

            if (jobTask.SubcontractorID != null)
            {
                SubcontractorWithCommunication subcontractor = allSubcontractors.Find(delegate(SubcontractorWithCommunication sub) { return (sub.Identifier.ToString().Equals(jobTask.SubcontractorID.ToString())); });
                SubcontractorScheduling scheduling = SubcontractorSchedulingBLL.Read(subcontractor.Identifier);
                workerName = subcontractor.Name;                
                if (scheduling != null)
                {
                    channel = ((scheduling.EMail == null) || (scheduling.EMail.Trim().Length == 0)) ? scheduling.PrimaryPhone : scheduling.EMail;
                    channel2 = ((scheduling.EMail == null) || (scheduling.EMail.Trim().Length == 0)) ? scheduling.BackupPhone1 : scheduling.PrimaryPhone;
                    channel3 = ((scheduling.EMail == null) || (scheduling.EMail.Trim().Length == 0)) ? scheduling.BackupPhone2 : scheduling.BackupPhone1;
                    channel4 = ((scheduling.EMail == null) || (scheduling.EMail.Trim().Length == 0)) ? null : scheduling.BackupPhone2;
                    method = ((scheduling.EMail == null) || (scheduling.EMail.Trim().Length == 0)) ? CommunicationMethodEnum.Phone : CommunicationMethodEnum.EMail;
                }
            }
            else if (jobTask.EmployeeID != null)
            {
                Employee employee = allEmployees.Find(delegate(Employee emp) { return (emp.Identifier.ToString().Equals(jobTask.EmployeeID.ToString())); });
                workerName = employee.Name;
                if (employee != null)
                {
                    channel = ((employee.Email == null) || (employee.Email.Trim().Length == 0)) ? employee.WorkPhone : employee.Email;
                    channel2 = ((employee.Email == null) || (employee.Email.Trim().Length == 0)) ? employee.CellPhone : employee.WorkPhone;
                    channel3 = ((employee.Email == null) || (employee.Email.Trim().Length == 0)) ? employee.HomePhone : employee.CellPhone;
                    channel4 = ((employee.Email == null) || (employee.Email.Trim().Length == 0)) ? null : employee.HomePhone;
                    method = ((employee.Email == null) || (employee.Email.Trim().Length == 0)) ? CommunicationMethodEnum.Phone : CommunicationMethodEnum.EMail;
                }
            }
            else
            {
                workerName = null;
                channel = null;
                channel2 = null;
                channel3 = null;
                channel4 = null;
            }
        }

        private List<TaskAction> JobTaskListToTaskActionList(List<JobTask> taskActionList, TaskActionEnum action)
        {
            List<TaskAction> result = new List<TaskAction>();
            foreach (JobTask jobTask in taskActionList)
            {
                string jobName = allJobs.Find(delegate(JobCommunicationsDelays jb) { return (jb.Identifier.ToString().Equals(jobTask.JobID.ToString())); }).Name;
                string workerName, channel, channel2, channel3, channel4;
                CommunicationMethodEnum? method;
                FindWorker(jobTask, out workerName, out channel, out channel2, out channel3, out channel4, out method);
                result.Add(new TaskAction(jobTask, jobName, action, workerName, channel, channel2, channel3, channel4));
            }
            result.Sort(Compare);
            return result;
        }

        private void FindTaskActions()
        {
            DateTime today = DateTime.Now.Date;
            DateTime tomorrow = today.AddDays(1);
            DateTime oneWeek = CalendarBLL.FindWorkDate(DateTime.Now.Date, 5);
            DateTime threeWeek = CalendarBLL.FindWorkDate(DateTime.Now.Date, 15);
            // Find Assignments
            Assignments = JobTaskListToTaskActionList(allJobTasks.FindAll(delegate(JobTask jt) { return ((jt.SubcontractorID == null) && (jt.EmployeeID == null) && (jt.Finish > tomorrow) && (jt.Start < threeWeek)); }), TaskActionEnum.Assign);
            // Find 
            NotAccepted = JobTaskListToTaskActionList(allJobTasks.FindAll(delegate(JobTask jt) { return ((jt.SubcontractorID != null) && (jt.WorkerAccepted == null) && (jt.Finish > tomorrow) && (jt.Start < oneWeek)); }), TaskActionEnum.NotAccepted);
            // Find Reviews            
            Reviews = JobTaskListToTaskActionList(allJobTasks.FindAll(delegate(JobTask jt) { return ((jt.Finish < today) || (jt.WorkerFinish != null)) && (jt.Signoff == null); }), TaskActionEnum.Review);
            // Find InProcess
            InProcess = JobTaskListToTaskActionList(allJobTasks.FindAll(delegate(JobTask jt) { return (jt.Start <= tomorrow) && ((jt.Finish > today) && (jt.WorkerFinish == null)); }), TaskActionEnum.Remind);
        }


        private void NewTaskCommunicationAction(JobTask jobTask, string jobName, CommunicationReasonEnum reason)
        {
            string workerName, channel, channel2, channel3, channel4;
            CommunicationMethodEnum? method;
            FindWorker(jobTask, out workerName, out channel, out channel2, out channel3, out channel4, out method);
            if (channel != null)
            {
                TaskCommunicationAction taskAction = new TaskCommunicationAction(jobTask, jobName, reason, workerName, channel, channel2, channel3, channel4);
                if (method == CommunicationMethodEnum.Phone)
                {
                    PhoneCalls.Add(taskAction);
                }
                else
                {
                    // Do we already have a reschedule for this subcontractor?
                    //                TaskCommunicationAction existingSubAction = EMails.Find(delegate(TaskCommunicationAction tca) { return tca.WorkerID.Equals(taskAction.WorkerID, StringComparison.OrdinalIgnoreCase) && (tca.Reason == CommunicationReasonEnum.Rescheduling); });
                    //                if (existingSubAction == null)
                    //                {
                    EMails.Add(taskAction);
                    //                }
                }
            }
        }

        private List<SubcontractorCommunication> FindSubcontractorCommunications(SubcontractorWithCommunication sub)
        {
            if (sub.Communications.Count == 0)
            {
                DateTime lastMonth = DateTime.Now.Date.AddMonths(-1);
                sub.Communications = SubcontractorCommunicationBLL.Search(sub.Identifier, lastMonth, DateTime.Now);
            }
            return sub.Communications;
        }

        private void FindTaskCommunicationActions()
        {
            DateTime oneWeek = CalendarBLL.FindWorkDate(DateTime.Now.Date, 5);
            DateTime threeWeek = CalendarBLL.FindWorkDate(DateTime.Now.Date, 15);
            PhoneCalls = new List<TaskCommunicationAction>();
            EMails = new List<TaskCommunicationAction>();
            DateTime today = DateTime.Now.Date;
            // Find all tasks that finish today or later that are assigned meaning they aren't accepted.
            List<JobTask> possibleAssigningTasks = allJobTasks.FindAll(delegate(JobTask jt) { return (jt.AssignmentStatus == TaskAssignmentStatusEnum.Assigned) && (jt.Finish >= today) && (jt.Start < threeWeek); });
            foreach (JobTask task in possibleAssigningTasks)
            {
                JobCommunicationsDelays currentJob = allJobs.Find(delegate(JobCommunicationsDelays jc) { return (jc.Identifier.ToString().Equals(task.JobID.ToString())); });
                if (task.SubcontractorID != null)
                {
                    SubcontractorWithCommunication subComm = allSubcontractors.Find(delegate(SubcontractorWithCommunication swc) { return swc.Identifier.ToString().Equals(task.SubcontractorID.ToString(), StringComparison.OrdinalIgnoreCase); });
                    List<SubcontractorCommunication> communications = FindSubcontractorCommunications(subComm);
                    //SubcontractorCommunication communicae = communications.Find(delegate(SubcontractorCommunication sc) { return (sc.Created > task.WorkerAssigned); });
                    SubcontractorCommunication communicae = communications.Find(delegate(SubcontractorCommunication sc) { return (sc.Created > task.WorkerAssigned) && (sc.Reason.Equals(CommunicationReasonEnum.Composite) || (sc.Reason.Equals(CommunicationReasonEnum.Scheduling) && sc.JobTaskID.ToString().Equals(task.Identifier.ToString(), StringComparison.OrdinalIgnoreCase))); });
                    //SubcontractorCommunication communicae = currentJob.SubcontractorCommunications.Find(delegate(SubcontractorCommunication sc) { return sc.Reason.Equals(CommunicationReasonEnum.Scheduling) && sc.SubcontractorID.ToString().Equals(task.SubcontractorID.ToString()) && sc.JobTaskID.ToString().Equals(task.Identifier.ToString()); });
                    // If we can't find a scheduling communication lets add this task to the list.
                    if (communicae == null)
                    {
                        NewTaskCommunicationAction(task, currentJob.Name, CommunicationReasonEnum.Scheduling);
                    }
                }
            }

            List<JobTask> possibleAcceptedTasks = allJobTasks.FindAll(delegate(JobTask jt) { return (jt.AssignmentStatus == TaskAssignmentStatusEnum.Accepted) && (jt.Finish >= today); });
            foreach (JobTask task in possibleAcceptedTasks)
            {
                if (task.SubcontractorID != null)
                {
                    JobCommunicationsDelays currentJob = allJobs.Find(delegate(JobCommunicationsDelays jc) { return (jc.Identifier.ToString().Equals(task.JobID.ToString())); });
                    SubcontractorCommunication communicae;
                    bool rescheduled = false;
                    // Check for reschedule
                    JobTaskDelay jobTaskDelay = currentJob.Delays.FindLast(delegate(JobTaskDelay jtd) { return jtd.JobTaskID.ToString().Equals(task.Identifier.ToString()); });
                    if (jobTaskDelay != null)
                    {
                        SubcontractorWithCommunication subComm = allSubcontractors.Find(delegate(SubcontractorWithCommunication swc) { return swc.Identifier.ToString().Equals(task.SubcontractorID.ToString(), StringComparison.OrdinalIgnoreCase); });
                        List<SubcontractorCommunication> communications = FindSubcontractorCommunications(subComm);
                        communicae = communications.Find(delegate(SubcontractorCommunication sc) { return (sc.Created > jobTaskDelay.Entered) && (sc.Reason.Equals(CommunicationReasonEnum.Composite) || (sc.Reason.Equals(CommunicationReasonEnum.Rescheduling) && sc.JobTaskID.ToString().Equals(jobTaskDelay.JobTaskID.ToString(), StringComparison.OrdinalIgnoreCase))); });
                        //SubcontractorCommunication communicae = currentJob.SubcontractorCommunications.Find(delegate(SubcontractorCommunication sc) { return sc.Reason.Equals(CommunicationReasonEnum.Scheduling) && sc.SubcontractorID.ToString().Equals(task.SubcontractorID.ToString()) && sc.JobTaskID.ToString().Equals(task.Identifier.ToString()); });
                        // If we can't find a scheduling communication lets add this task to the list.
                        if (communicae == null)
                        {
                            NewTaskCommunicationAction(task, currentJob.Name, CommunicationReasonEnum.Rescheduling);
                            rescheduled = true;
                        }
/*
                        // Don't worry about which job was the last one to send a delay.
                        //communicae = currentJob.SubcontractorCommunications.FindLast(delegate(SubcontractorCommunication sc) { return sc.Reason.Equals(CommunicationReasonEnum.Rescheduling) && sc.SubcontractorID.ToString().Equals(task.SubcontractorID.ToString()) && sc.JobTaskID.ToString().Equals(task.Identifier.ToString()); });
                        communicae = currentJob.SubcontractorCommunications.FindLast(delegate(SubcontractorCommunication sc) { return sc.Reason.Equals(CommunicationReasonEnum.Rescheduling) && sc.SubcontractorID.ToString().Equals(task.SubcontractorID.ToString()); });
                        if ((communicae == null) || (jobTaskDelay.Entered.CompareTo(communicae.Created) > 0))
                        {
                            communicae = currentJob.SubcontractorCommunications.FindLast(delegate(SubcontractorCommunication sc) { return sc.Reason.Equals(CommunicationReasonEnum.Composite) && sc.SubcontractorID.ToString().Equals(task.SubcontractorID.ToString()); });
                            if ((communicae == null) || (jobTaskDelay.Entered.CompareTo(communicae.Created) > 0))
                            {
                                NewTaskCommunicationAction(task, currentJob.Name, CommunicationReasonEnum.Rescheduling);
                                rescheduled = true;
                            }
                        }
 */ 
                    }
                    if (!rescheduled)
                    {
                        // Make sure we don't send a reminder, task assignment, or reschedule for the last 2 days.
                        DateTime checkDate = CalendarBLL.FindWorkDate(DateTime.Now.Date, -2);
                        // Check for reminder
                        if (CalendarBLL.FindWorkDate(today, 3) > task.Start)
                        {
                            SubcontractorWithCommunication subComm = allSubcontractors.Find(delegate(SubcontractorWithCommunication swc) { return swc.Identifier.ToString().Equals(task.SubcontractorID.ToString(), StringComparison.OrdinalIgnoreCase); });
                            List<SubcontractorCommunication> communications = FindSubcontractorCommunications(subComm);
                            communicae = communications.Find(delegate(SubcontractorCommunication sc) { return (sc.Created > checkDate) && (sc.Reason.Equals(CommunicationReasonEnum.Composite) || (sc.Reason.Equals(CommunicationReasonEnum.Reminding))); });
                            //SubcontractorCommunication communicae = currentJob.SubcontractorCommunications.Find(delegate(SubcontractorCommunication sc) { return sc.Reason.Equals(CommunicationReasonEnum.Scheduling) && sc.SubcontractorID.ToString().Equals(task.SubcontractorID.ToString()) && sc.JobTaskID.ToString().Equals(task.Identifier.ToString()); });
/*
                            SubcontractorCommunication subReminders = currentJob.SubcontractorCommunications.Find(delegate(SubcontractorCommunication sc) { return sc.Reason.Equals(CommunicationReasonEnum.Reminding) && sc.SubcontractorID.ToString().Equals(task.SubcontractorID.ToString()) && sc.JobTaskID.ToString().Equals(task.Identifier.ToString()); });
                            SubcontractorCommunication subSchedule = currentJob.SubcontractorCommunications.Find(delegate(SubcontractorCommunication sc) { return sc.Reason.Equals(CommunicationReasonEnum.Scheduling) && sc.SubcontractorID.ToString().Equals(task.SubcontractorID.ToString()) && sc.JobTaskID.ToString().Equals(task.Identifier.ToString()) && (sc.Created < checkDate); });
                            SubcontractorCommunication subReschedule = currentJob.SubcontractorCommunications.Find(delegate(SubcontractorCommunication sc) { return sc.Reason.Equals(CommunicationReasonEnum.Rescheduling) && sc.SubcontractorID.ToString().Equals(task.SubcontractorID.ToString()) && sc.JobTaskID.ToString().Equals(task.Identifier.ToString()) && (sc.Created < checkDate); });
 */ 
                            // !!! Put back after demonstration !!!
                            if (communicae == null)
                            {
                                NewTaskCommunicationAction(task, currentJob.Name, CommunicationReasonEnum.Reminding);
                            }
                        }
                    }
                }
                else if (task.EmployeeID != null)
                {
                    JobCommunicationsDelays currentJob = allJobs.Find(delegate(JobCommunicationsDelays jc) { return (jc.Identifier.ToString().Equals(task.JobID.ToString())); });
                    EmployeeCommunication communicae = currentJob.EmployeeCommunications.Find(delegate(EmployeeCommunication sc) { return sc.Reason.Equals(CommunicationReasonEnum.Scheduling) && sc.EmployeeID.ToString().Equals(task.EmployeeID.ToString()) && sc.JobTaskID.ToString().Equals(task.Identifier.ToString()); });
                    // If we can't find a scheduling communication lets add this task to the list.
                    if (communicae == null)
                    {
                        NewTaskCommunicationAction(task, currentJob.Name, CommunicationReasonEnum.Scheduling);
                    }
                }
            }
            EMails.Sort(Compare);
            PhoneCalls.Sort(Compare);
        }

        public DailyTaskList(Employee supervisor)
        {
            this.supervisor = supervisor;
            // Get all the job tasks
            FindTasks();
            // Get all the jobs based on tasks that are loaded.
            // Need to go back a month just to make sure we don't miss any jobs
            List<Job> justJobs = JobBLL.ActiveJobs(supervisor.Builder.Identifier);
            allJobs = new List<JobCommunicationsDelays>();            
            foreach (Job job in justJobs)
            {
                JobCommunicationsDelays jac = new JobCommunicationsDelays();
                job.CopyTo(jac);
                jac.EmployeeCommunications = EmployeeCommunicationBLL.SearchJob(jac.Identifier);
                //jac.SubcontractorCommunications = SubcontractorCommunicationBLL.SearchJob(jac.Identifier);
                jac.Delays = JobDelayBLL.ListJobTaskDelay(job.Identifier);
                jac.Delays.Sort(delegate(JobTaskDelay jtd, JobTaskDelay jtd2) { return jtd.Entered.CompareTo(jtd2.Entered); });
                allJobs.Add(jac);
            }
            allEmployees = EmployeeBLL.List(supervisor.Builder);
            List<Subcontractor> subs = SubcontractorBLL.List(supervisor.Builder.Identifier);
            allSubcontractors = new List<SubcontractorWithCommunication>();
            foreach (Subcontractor sub in subs)
            {
                allSubcontractors.Add(new SubcontractorWithCommunication(sub));
            }
            // Find all the required Task Actions
            FindTaskActions();
            FindTaskCommunicationActions();
        }
    }

    public static class DailyTaskPLL
    {
        /*
            public enum TaskCommunicationEnum
            {
                Assigning,
                Accepting,
                Rejecting,
                Rescheduling,
                Reminding,
                Finishing
            }
        */

        /*
                public static int CompareCommunicationTask(TaskCommunication communicationTask1, TaskCommunication communicationTask2)
                {
                    int result = 0;
                    if ((communicationTask1 == null) && (communicationTask2 != null))
                    {
                        result = -1;
                    }
                    else
                    {
                        if ((communicationTask2 == null) && (communicationTask1 != null))
                        {
                            result = 1;
                        }
                        else
                        {
                            result = communicationTask1.StartDate.CompareTo(communicationTask2.StartDate);
                            if (result == 0)
                            {
                                result = communicationTask1.FinishDate.CompareTo(communicationTask2.FinishDate);
                            }
                        }
                    }
                    return result;
                }

                public delegate bool KeepEntryDelegate(TaskCommunication communicationTask);


                        private static TaskCommunication JobTaskConversion(JobTask task)
                        {
                            // Add tasks that need to be confirmed.
                            TaskCommunication communication = new TaskCommunication();
                            communication.JobID = task.JobID;
                            communication.JobTaskID = task.Identifier;
                            communication.TaskName = task.TaskName;
                            communication.Reason = CommunicationReasonEnum.Assign;
                            communication.WorkerFinish = task.WorkerFinish;
                            communication.ConfirmFinish = task.Signoff;
                            communication.StartDate = task.Start;
                            communication.FinishDate = task.Finish;

                            // If there isn't a sub or employee assigned it needs to be assigned.
                            if ((task.SubcontractorID == null) && (task.EmployeeID == null))
                            {
                                communication.Reason = CommunicationReasonEnum.Assign;
                            }
                            else if (task.SubcontractorID != null)
                            {
                                //SubcontractorCommunication.
                                communication.Reason = CommunicationReasonEnum.Schedule;
                                List<SubcontractorCommunication> taskCommunications = SubcontractorCommunicationBLL.Search(task.Identifier);
                                foreach (SubcontractorCommunication communicae in taskCommunications)
                                {
                                    // If we have already scheduled then maybe we need a reminder or update.
                                    if (communicae.Reason == CommunicationReasonEnum.Schedule)
                                    {
                                        communication.Reason = CommunicationReasonEnum.Reminder;
                                    }
                                }
                                Subcontractor subcontractor = SubcontractorBLL.Read(task.SubcontractorID);
                                communication.WorkerName = subcontractor.Name;
                                communication.WorkerID = WorkerPLL.WorkerID(WorkerEnum.Subcontractor, subcontractor.Identifier.ToString());
                                SubcontractorScheduling subScheduling = SubcontractorSchedulingBLL.Read(task.SubcontractorID);
                                if ((subScheduling.EMail != null) && (subScheduling.EMail.Trim().Length > 0))
                                {
                                    communication.Method = CommunicationMethodEnum.EMail;
                                    communication.Channel = subScheduling.EMail;
                                    communication.Channel2 = subScheduling.PrimaryPhone;
                                    communication.Channel3 = subScheduling.BackupPhone1;
                                    communication.Channel4 = subScheduling.BackupPhone2;
                                }
                                else
                                {
                                    communication.Method = CommunicationMethodEnum.Phone;
                                    communication.Channel = subScheduling.PrimaryPhone;
                                    communication.Channel2 = subScheduling.BackupPhone1;
                                    communication.Channel3 = subScheduling.BackupPhone2;
                                }
                            }
                            else if (task.EmployeeID != null)
                            {
                                communication.Reason = CommunicationReasonEnum.Schedule;
                                List<EmployeeCommunication> taskCommunications = EmployeeCommunicationBLL.Search(task.Identifier);
                                foreach (EmployeeCommunication communicae in taskCommunications)
                                {
                                    if (communicae.Reason == CommunicationReasonEnum.Schedule)
                                    {
                                        communication.Reason = CommunicationReasonEnum.Reminder;
                                    }
                                }
                                Employee employee = EmployeeBLL.Read(task.EmployeeID);
                                communication.WorkerName = employee.LastName + ", " + employee.FirstName;
                                communication.WorkerID = WorkerPLL.WorkerID(WorkerEnum.Employee, employee.Identifier.ToString());
                                if ((employee.EMail != null) && (employee.EMail.Trim().Length > 0))
                                {
                                    communication.Method = CommunicationMethodEnum.EMail;
                                    communication.Channel = employee.EMail;
                                    communication.Channel2 = employee.WorkPhone;
                                    communication.Channel3 = employee.CellPhone;
                                    communication.Channel4 = employee.HomePhone;
                                }
                                else
                                {
                                    communication.Method = CommunicationMethodEnum.Phone;
                                    communication.Channel = employee.WorkPhone;
                                    communication.Channel2 = employee.CellPhone;
                                    communication.Channel3 = employee.HomePhone;
                                }
                            }
                            return communication;
                        }

                        public static TaskCommunication Read(object jobTaskID)
                        {
                            JobTask jobTask = JobTaskBLL.Read(jobTaskID);
                            return JobTaskConversion(jobTask);
                        }


                public static bool TwoDayWindow(TaskCommunication commTask)
                {
                    DateTime currentDate = DateTime.Now.Date;
                    DateTime endDate = currentDate.AddDays(2);
                    if (currentDate.DayOfWeek.Equals(DayOfWeek.Friday) || currentDate.DayOfWeek.Equals(DayOfWeek.Saturday))
                    {
                        endDate = currentDate.AddDays(4);
                    }
                    if (currentDate.DayOfWeek.Equals(DayOfWeek.Sunday))
                    {
                        endDate = currentDate.AddDays(3);
                    }
                    return (commTask.StartDate < endDate);
                }


                public static List<TaskCommunication> SupervisorCommunicationList(object employeeID, DateTime startDate, DateTime endDate, KeepEntryDelegate keepEntry)
                {
                    List<TaskCommunication> result = new List<TaskCommunication>();
                    List<JobTask> tasks = JobTaskBLL.SearchSupervisorTask(employeeID, startDate, endDate);
                    foreach (JobTask task in tasks)
                    {
                        TaskCommunication communication = JobTaskConversion(task);
                        // Do we have a delegate called keepEntry? if not add the entry else call keepEntry and see.
                        if ((keepEntry == null) ? true : (keepEntry(communication)))
                        {
                            result.Add(communication);
                        }
                    }
                    result.Sort(CompareCommunicationTask);
                    return result;
                }

                private static List<TaskCommunication> SupervisorCommunicationList(object employeeID, DateTime startDate, DateTime endDate)
                {
                    return SupervisorCommunicationList(employeeID, startDate, endDate, null);
                }

                public static List<TaskCommunication> ThreeWeekTasks(object employeeID, KeepEntryDelegate keepEntry)
                {
                    List<TaskCommunication> result;
                    DateTime currentDate = DateTime.Now.Date;
                    DateTime endDate = currentDate.AddDays(21);
                    if (currentDate.DayOfWeek.Equals(DayOfWeek.Friday) || currentDate.DayOfWeek.Equals(DayOfWeek.Saturday))
                    {
                        endDate = currentDate.AddDays(23);
                    }
                    if (currentDate.DayOfWeek.Equals(DayOfWeek.Sunday))
                    {
                        endDate = currentDate.AddDays(22);
                    }
                    result = SupervisorCommunicationList(employeeID, currentDate, endDate, keepEntry);
                    return result;
                }

                public static List<TaskCommunication> ThreeWeekTaskAssignments(object employeeID)
                {
        //            return ThreeWeekTasks(employeeID, delegate(TaskCommunication c) { return (c.Reason == CommunicationReasonEnum.Assign); });
                }

                public static bool ThreeWeekEMailCommunicationCheck(TaskCommunication communication)
                {
                    // Do we have an assigned person that needs to be e-mailed.
                    bool result = (communication.Reason != CommunicationReasonEnum.Assign) && (communication.Method == CommunicationMethodEnum.EMail);
                    if (result)
                    {
                        result = (communication.Reason == CommunicationReasonEnum.Reminder) && (communication.StartDate < DateTime.Now.Date.AddDays(7));
                    }
                    return result;
                }

                public static List<TaskCommunication> ThreeWeekEMailCommunications(object employeeID)
                {
                    return ThreeWeekTasks(employeeID, delegate(TaskCommunication c) { return ((c.Reason != CommunicationReasonEnum.Assign) && (c.Method == CommunicationMethodEnum.EMail)); });
                }

                public static List<TaskCommunication> ThreeWeekPhoneCommunications(object employeeID)
                {
                    return ThreeWeekTasks(employeeID, delegate(TaskCommunication c) { return ((c.Reason != CommunicationReasonEnum.Assign) && (c.Method == CommunicationMethodEnum.Phone)); });
                }

                public static List<TaskCommunication> TwoDayTaskList(object employeeID)
                {
                    DateTime currentDate = DateTime.Now.Date;
                    DateTime endDate = currentDate.AddDays(2);
                    if (currentDate.DayOfWeek.Equals(DayOfWeek.Friday) || currentDate.DayOfWeek.Equals(DayOfWeek.Saturday))
                    {
                        endDate = currentDate.AddDays(4);
                    }
                    if (currentDate.DayOfWeek.Equals(DayOfWeek.Sunday))
                    {
                        endDate = currentDate.AddDays(3);
                    }
                    return SupervisorCommunicationList(employeeID, currentDate, endDate);
                }
         */
    }
}
