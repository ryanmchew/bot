﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuildOnTime.BusinessObject;
using BuildOnTime.DataAccess;

namespace BuildOnTime.BusinessLogic
{
    public static class JobBLL
    {
        public static List<Job> ActiveJobs(object builderID)
        {
            return JobBLL.Search(builderID, DateTime.Now.Date.AddMonths(-1));
        }

        public static Job Create(object builderID, object superID, object siteID, string name, string permit, string description)
        {
            return JobDAL.Create(builderID, superID, siteID, name, permit, description);
        }

        public static Job CreateJobSchedule(object builderID, object superID, int scheduleID, DateTime startDate, string name, string permit, string description, string lot, string address1, string city, string postalCode, string state)
        {
            List<ScheduleTask> scheduleList = ScheduleTaskBLL.List(scheduleID);
            Site site = new Site { Address1 = address1, BuilderID = int.Parse(builderID.ToString()), City = city, State = state, LotIdentifier = lot, PostalCode = postalCode };
            int siteID = SiteDAL.Create(site).Identifier.Value;
            Job job = JobDAL.Create(builderID, superID, siteID, name, permit, description);
            List<JobTask> jobTasks = JobTaskBLL.CreateJobSchedule(job, startDate, scheduleList);
            JobBLL.CreateDependencyTree(job.Identifier, scheduleID);
            return job;
        }

        public static Job Read(object identifier)
        {
            return JobDAL.Read(identifier);
        }

        public static void Update(Job job)
        {
            JobDAL.Update(job);
        }

        public static void Delete(Job job, int userId, string initiator)
        {
            JobDAL.Delete(job, userId, initiator);
        }

        public static List<Job> Search(object builderID, DateTime activeDate)
        {
            return JobDAL.Search(builderID, activeDate);
        }

        public static List<Job> Search(object builderID, string name)
        {
            return JobDAL.Search(builderID, name);
        }

        public static void CreateDependencyTree(object jobID, object scheduleID)
        {
            List<JobTask> jobTasks = JobTaskDAL.List(jobID);
            List<ScheduleTaskDependency> scheduleTaskDependencies = ScheduleTaskBLL.TaskDependencyList(scheduleID);
            foreach (ScheduleTaskDependency taskToCreate in scheduleTaskDependencies)
            {
                JobTask parentTask = jobTasks.Find(delegate(JobTask jt) { return jt.ScheduleTaskID.Equals(taskToCreate.ParentID); });
                JobTask childTask = jobTasks.Find(delegate(JobTask jt) { return jt.ScheduleTaskID.Equals(taskToCreate.DependentID); });
                if ((parentTask != null) && (childTask != null))
                {
                    JobTaskDAL.TaskDependencyCreate(jobID, parentTask.Identifier, childTask.Identifier);
                }
            }
        }
    }

    public static class JobTaskBLL
    {
        /// <summary>
        /// Return 0 if scheduleTasks are equal, return 1 if scheduleTask1 is greater than scheduleTask2,
        /// and return -1 if scheduleTask2 is greater than scheduleTask1
        /// </summary>
        /// <param name="scheduleTask1"></param>
        /// <param name="scheduleTask2"></param>
        /// <returns></returns>
        public static int CompareJobTaskByStartDay(JobTask jobTask1, JobTask jobTask2)
        {
            int result = 0;
            if ((jobTask1 == null) && (jobTask2 != null))
            {
                result = -1;
            }
            else
            {
                if ((jobTask2 == null) && (jobTask1 != null))
                {
                    result = 1;
                }
                else
                {
                    result = jobTask1.Start.CompareTo(jobTask2.Start);
                    if (result == 0)
                    {
                        result = jobTask1.Finish.CompareTo(jobTask2.Finish);
                    }
                }
            }
            return result;
        }

        public static List<JobTask> CreateJobSchedule(Job job, DateTime startDate, List<ScheduleTask> taskList)
        {
            List<JobTask> result = new List<JobTask>();
            taskList.Sort(ScheduleTaskBLL.CompareScheduleTaskByStartDay);
            // The list is sorted lets go ahead and turn the schedule into calendar dates.
            foreach (ScheduleTask task in taskList)
            {
                // Calculate start and end date based upon calendar.
                JobTask jobTask = new JobTask();
                // Find dates
                jobTask.EmployeeID = task.DefaultEmployeeID;
                jobTask.Group = task.Group;
                jobTask.ScheduleTaskID = task.Identifier;
                jobTask.SubcontractorID = task.DefaultSubcontractorID;
                jobTask.JobID = job.Identifier;
                jobTask.ScheduledStart = CalendarBLL.FindWorkDate(startDate, task.DayNumber);
                jobTask.ScheduledFinish = CalendarBLL.FindWorkDate(jobTask.ScheduledStart, task.Duration);
                jobTask.Start = jobTask.ScheduledStart;
                jobTask.Finish = jobTask.ScheduledFinish;
                jobTask.TaskName = task.Name;
                result.Add(jobTask);
            }
            foreach (JobTask jobTask in result)
            {
                JobTaskBLL.Create(jobTask);
            }
            return result;
        }

        public static JobTask Create(JobTask jobTask)
        {
            JobTask result = JobTaskDAL.Create(jobTask);
            if (result.EmployeeID != null)
            {
                EmployeeAssociate(result, result.EmployeeID);
            }
            if (result.SubcontractorID != null)
            {
                SubcontractorAssociate(result, result.SubcontractorID);
            }
            return result;
        }

        public static JobTask Read(object identifier)
        {
            return JobTaskDAL.Read(identifier);
        }

        public static void Update(JobTask jobTask)
        {
            JobTaskDAL.Update(jobTask);
        }

        public static void Delete(JobTask jobTask)
        {
            JobTaskDAL.Delete(jobTask);
        }

        public static List<JobTask> List(object jobID)
        {
            return JobTaskDAL.List(jobID);
        }

        public static List<JobTask> SearchEmployeeTask(object employeeID, DateTime startDate, DateTime endDate)
        {
            return JobTaskDAL.SearchEmployeeTask(employeeID, startDate, endDate);
        }

        public static List<JobTask> SearchBuilderTask(object builderID, DateTime startDate, DateTime endDate)
        {
            return JobTaskDAL.SearchBuilderTask(builderID, startDate, endDate);
        }

        public static List<JobTask> SearchSubcontractorTask(object subcontractorID, DateTime startDate, DateTime endDate)
        {
            return JobTaskDAL.SearchSubcontractorTask(subcontractorID, startDate, endDate);
        }

        public static List<JobTask> SearchSupervisorTask(object employeeID, DateTime startDate, DateTime endDate)
        {
            return JobTaskDAL.SearchSupervisorTask(employeeID, startDate, endDate);
        }

        private static bool DelayWorkDates(JobTaskDelay jtd, JobTask childTask, int delayDuration, out DateTime startDate, out DateTime finishDate)
        {
            bool result = true;
            // Did tasks originally start on the same day?
            if (jtd.OriginalStart.Equals(childTask.Start))
            {
                startDate = CalendarBLL.FindWorkDate(childTask.Start, delayDuration);
                finishDate = CalendarBLL.FindWorkDate(childTask.Finish, delayDuration);
            }
            // Did child tasks start before original task was finished?
            else if ((jtd.OriginalStart < childTask.Start) && (childTask.Start < jtd.OriginalFinish))
            {
                // Find the exact delay between the two starts.
                int startDelay = CalendarBLL.WorkDateDifference(jtd.OriginalStart, childTask.Start);
                int childDuration = CalendarBLL.WorkDateDifference(childTask.Start, childTask.Finish);
                startDate = CalendarBLL.FindWorkDate(jtd.Start, startDelay);
                finishDate = CalendarBLL.FindWorkDate(startDate, childDuration);
            }
            // Else child task must need to start after parent task is finished.
            else
            {
                // DelayBy is the earliest date the task could start
                int delayBy = CalendarBLL.WorkDateDifference(childTask.Start, jtd.Finish);
                // If we are delaying a task we should use this date.
                if ((delayBy > 0) && (delayDuration > 0))
                {
                    startDate = CalendarBLL.FindWorkDate(childTask.Start, delayBy);
                    finishDate = CalendarBLL.FindWorkDate(childTask.Finish, delayBy);
                }
                // If we are moving a task to an earlier date use the original delay duration so it doesn't get closer.
                else if ((delayBy < 0) && (delayDuration < 0))
                {
                    startDate = CalendarBLL.FindWorkDate(childTask.Start, delayDuration);
                    finishDate = CalendarBLL.FindWorkDate(childTask.Finish, delayDuration);
                }
                else
                {
                    result = false;
                    startDate = childTask.Start;
                    finishDate = childTask.Finish;
                }
            }
            return result;
        }

        public static void JobTaskDelaySchedules(List<JobTask> jobTasks, List<JobTaskDependency> dependencies, JobTaskDelay jtd, List<JobTaskDelay> delays)
        {
            int delayTime = CalendarBLL.WorkDateDifference(jtd.OriginalStart, jtd.Start);
            List<JobTaskDependency> childDependents = dependencies.FindAll(delegate(JobTaskDependency dep) { return jtd.JobTaskID.ToString().Equals(dep.ParentID.ToString()); });            
            foreach (JobTaskDependency child in childDependents)
            {
                JobTask childTask = jobTasks.Find(delegate(JobTask jt) { return jt.Identifier.ToString().Equals(child.DependentID.ToString()); });                
                DateTime startDate, finishDate;
                if (DelayWorkDates(jtd, childTask, delayTime, out startDate, out finishDate))
                {
                    JobTaskDelay childTaskDelay = delays.Find(delegate(JobTaskDelay td) { return td.JobTaskID.Equals(childTask.Identifier); });
                    if (childTaskDelay != null)
                    {
                        // Is our date later?  If so make some updates.
                        if (startDate > childTaskDelay.Start)
                        {
                            childTaskDelay.Start = startDate;
                            childTaskDelay.Finish = finishDate;
                            childTaskDelay.JobTaskDelayerID = jtd.JobTaskID;
                        }
                    }
                    else
                    {
                        childTaskDelay = new JobTaskDelay();
                        childTaskDelay.JobTaskID = childTask.Identifier;
                        childTaskDelay.Entered = jtd.Entered;
                        childTaskDelay.JobDelayID = jtd.JobDelayID;
                        childTaskDelay.OriginalStart = childTask.Start;
                        childTaskDelay.OriginalFinish = childTask.Finish;
                        childTaskDelay.JobTaskDelayerID = jtd.JobTaskID;
                        childTaskDelay.Group = jtd.Group;
                        childTaskDelay.Start = startDate;
                        childTaskDelay.Finish = finishDate;
                        delays.Add(childTaskDelay);
                    }
                    JobTaskDelaySchedules(jobTasks, dependencies, childTaskDelay, delays);
                }
            }
        }

        public static void JobTaskDelayGroupSchedules(List<JobTask> jobTasks, List<JobTaskDependency> dependencies, JobTaskDelay jtd, List<JobTaskDelay> delays)
        {
            int delayTime = CalendarBLL.WorkDateDifference(jtd.OriginalStart, jtd.Start);
            List<JobTaskDependency> childDependents = dependencies.FindAll(delegate(JobTaskDependency dep) { return jtd.JobTaskID.ToString().Equals(dep.ParentID.ToString()); });
            foreach (JobTaskDependency child in childDependents)
            {
                JobTask childTask = jobTasks.Find(delegate(JobTask jt) { return jt.Identifier.ToString().Equals(child.DependentID.ToString()); });
                if (childTask.Group.Equals(jtd.Group))
                {
                    DateTime startDate, finishDate;
                    if (DelayWorkDates(jtd, childTask, delayTime, out startDate, out finishDate))
                    {
                        // Look to make sure we haven't already delayed this task before.
                        JobTaskDelay childTaskDelay = delays.Find(delegate(JobTaskDelay td) { return td.JobTaskID.Equals(childTask.Identifier); });
                        if (childTaskDelay != null)
                        {
                            // Is our date later?  If so make some updates.
                            if (startDate > childTaskDelay.Start)
                            {
                                childTaskDelay.Start = startDate;
                                childTaskDelay.Finish = finishDate;
                                childTaskDelay.JobTaskDelayerID = jtd.JobTaskID;
                            }
                        }
                        else
                        {
                            childTaskDelay = new JobTaskDelay();
                            childTaskDelay.JobTaskID = childTask.Identifier;
                            childTaskDelay.Entered = jtd.Entered;
                            childTaskDelay.JobDelayID = jtd.JobDelayID;
                            childTaskDelay.OriginalStart = childTask.Start;
                            childTaskDelay.OriginalFinish = childTask.Finish;
                            childTaskDelay.JobTaskDelayerID = jtd.JobTaskID;
                            childTaskDelay.Group = jtd.Group;
                            childTaskDelay.Start = startDate;
                            childTaskDelay.Finish = finishDate;
                            delays.Add(childTaskDelay);
                        }
                        JobTaskDelayGroupSchedules(jobTasks, dependencies, childTaskDelay, delays);
                    }
                }
            }
        }

        public static List<JobTaskDependency> TaskDependencyList(object jobID)
        {
            return JobTaskDAL.TaskDependencyList(jobID);
        }

        public static List<JobTaskDelay> JobTaskDelayGroup(object jobID, object jobTaskID, int delayLength, List<JobTask> jobTasks)
        {
            List<JobTask> dependencies = JobTaskDAL.List(jobID);
            List<JobTaskDelay> delays = new List<JobTaskDelay>();
            JobTask parentTask = jobTasks.Find(delegate(JobTask jt) { return (jt.Identifier.ToString().Equals(jobTaskID.ToString())); });
            if (parentTask.Group != null)
            {
                List<JobTask> jobTaskGroup = jobTasks.FindAll(delegate(JobTask jt) { return jt.Group.Equals(parentTask.Group) && (jt.Start >= parentTask.Start); });
                foreach (JobTask jobTask in jobTaskGroup)
                {
                    JobTaskDelay taskDelay = new JobTaskDelay();
                    taskDelay.JobTaskID = jobTask.Identifier;
                    taskDelay.OriginalStart = jobTask.Start;
                    taskDelay.OriginalFinish = jobTask.Finish;
                    taskDelay.Group = jobTask.Group;
                    int duration = CalendarBLL.WorkDateDifference(jobTask.Start, jobTask.Finish);
                    taskDelay.Start = CalendarBLL.FindWorkDate(jobTask.Start, delayLength);
                    taskDelay.Finish = CalendarBLL.FindWorkDate(taskDelay.Start, duration);
                    delays.Add(taskDelay);
                }
            }
            else
            {
                JobTaskDelay taskDelay = new JobTaskDelay();
                taskDelay.JobTaskID = parentTask.Identifier;
                taskDelay.OriginalStart = parentTask.Start;
                taskDelay.OriginalFinish = parentTask.Finish;
                taskDelay.Group = parentTask.Group;
                int duration = CalendarBLL.WorkDateDifference(parentTask.Start, parentTask.Finish);
                taskDelay.Start = CalendarBLL.FindWorkDate(parentTask.Start, delayLength);
                taskDelay.Finish = CalendarBLL.FindWorkDate(taskDelay.Start, duration);
                delays.Add(taskDelay);
            }
            return delays;
        }

        public static List<JobTaskDelay> JobTaskDelayGroupDependents(object jobID, object jobTaskID, int delayLength, List<JobTask> jobTasks)
        {
            List<JobTaskDependency> dependencies = JobTaskDAL.TaskDependencyList(jobID);
            List<JobTaskDelay> delays = new List<JobTaskDelay>();
            JobTask parentTask = jobTasks.Find(delegate(JobTask jt) { return (jt.Identifier.ToString().Equals(jobTaskID.ToString())); });
            if (dependencies.Count > 0)
            {
                List<JobTaskDependency> childDependents = dependencies.FindAll(delegate(JobTaskDependency jtd) { return jtd.ParentID.ToString().Equals(jobTaskID.ToString()); });
                JobTaskDelay parentDelay = new JobTaskDelay();
                parentDelay.JobTaskID = jobTaskID;
                parentDelay.OriginalStart = parentTask.Start;
                parentDelay.OriginalFinish = parentTask.Finish;
                parentDelay.Group = parentTask.Group;
                parentDelay.Start = CalendarBLL.FindWorkDate(parentTask.Start, delayLength);
                parentDelay.Finish = CalendarBLL.FindWorkDate(parentTask.Finish, delayLength);
                delays.Add(parentDelay);
                JobTaskDelayGroupSchedules(jobTasks, dependencies, parentDelay, delays);
            } 
            return delays;
        }

        public static List<JobTaskDelay> JobTaskDelaySchedule(object jobID, object jobTaskID, int delayLength, List<JobTask> jobTasks)
        {
            List<JobTaskDependency> dependencies = JobTaskDAL.TaskDependencyList(jobID);
            List<JobTaskDelay> delays = new List<JobTaskDelay>();
            JobTask parentTask = jobTasks.Find(delegate(JobTask jt) { return (jt.Identifier.ToString().Equals(jobTaskID.ToString())); });
            if (dependencies.Count > 0)
            {
                List<JobTaskDependency> childDependents = dependencies.FindAll(delegate(JobTaskDependency jtd) { return jtd.ParentID.ToString().Equals(jobTaskID.ToString()); });
                JobTaskDelay parentDelay = new JobTaskDelay();
                parentDelay.JobTaskID = jobTaskID;
                parentDelay.OriginalStart = parentTask.Start;
                parentDelay.OriginalFinish = parentTask.Finish;
                parentDelay.Group = parentTask.Group;
                parentDelay.Start = CalendarBLL.FindWorkDate(parentTask.Start, delayLength);
                parentDelay.Finish = CalendarBLL.FindWorkDate(parentTask.Finish, delayLength);
                delays.Add(parentDelay);
                JobTaskDelaySchedules(jobTasks, dependencies, parentDelay, delays);
            }
/*
 !!! WHY WAS THIS HERE !!!
            else
            {
                List<JobTask> jobTaskGroup = jobTasks.FindAll(delegate(JobTask jt) { return jt.Group.Equals(parentTask.Group) && (jt.Start >= parentTask.Start); });
                foreach (JobTask jobTask in jobTaskGroup)
                {
                    JobTaskDelay taskDelay = new JobTaskDelay();
                    taskDelay.JobTaskID = jobTask.Identifier;
                    taskDelay.OriginalStart = jobTask.Start;
                    taskDelay.OriginalFinish = jobTask.Finish;
                    taskDelay.Group = jobTask.Group;
                    int duration = CalendarBLL.WorkDateDifference(jobTask.Start, jobTask.Finish);
                    taskDelay.Start = CalendarBLL.FindWorkDate(jobTask.Start, delayLength);
                    taskDelay.Finish = CalendarBLL.FindWorkDate(taskDelay.Start, duration);
                    delays.Add(taskDelay);
                }
            }
 */ 
            return delays;
        }

        public static List<JobTaskDelay> JobTaskDelaySchedule(object jobID, object jobTaskID, int delayLength)
        {
            List<JobTask> jobTasks = List(jobID);
            return JobTaskDelaySchedule(jobID, jobTaskID, delayLength, jobTasks);
/*
            List<JobTaskDependency> dependencies = JobTaskDAL.TaskDependencyList(jobID);
            List<JobTaskDelay> delays = new List<JobTaskDelay>();
            JobTask parentTask = jobTasks.Find(delegate(JobTask jt) { return (jt.Identifier.ToString().Equals(jobTaskID.ToString())); });
            if (dependencies.Count > 0)
            {
                List<JobTaskDependency> childDependents = dependencies.FindAll(delegate(JobTaskDependency jtd) { return jtd.ParentID.ToString().Equals(jobTaskID.ToString()); });                
                JobTaskDelay parentDelay = new JobTaskDelay();
                parentDelay.JobTaskID = jobTaskID;
                parentDelay.OriginalStart = parentTask.Start;
                parentDelay.OriginalFinish = parentTask.Finish;
                parentDelay.Group = parentTask.Group;
                parentDelay.Start = CalendarBLL.FindWorkDate(parentTask.Start, delayLength);
                parentDelay.Finish = CalendarBLL.FindWorkDate(parentTask.Finish, delayLength);
                delays.Add(parentDelay);
                JobTaskDelaySchedules(jobTasks, dependencies, parentDelay, delays);
            }
            else
            {
                List<JobTask> jobTaskGroup = jobTasks.FindAll(delegate(JobTask jt) { return jt.Group.Equals(parentTask.Group) && (jt.Start >= parentTask.Start); });
                foreach (JobTask jobTask in jobTaskGroup)
                {
                    JobTaskDelay taskDelay = new JobTaskDelay();
                    taskDelay.JobTaskID = jobTask.Identifier;
                    taskDelay.OriginalStart = jobTask.Start;
                    taskDelay.OriginalFinish = jobTask.Finish;
                    taskDelay.Group = jobTask.Group;
                    int duration = CalendarBLL.WorkDateDifference(jobTask.Start, jobTask.Finish);
                    taskDelay.Start = CalendarBLL.FindWorkDate(jobTask.Start, delayLength);
                    taskDelay.Finish = CalendarBLL.FindWorkDate(taskDelay.Start, duration);
                    delays.Add(taskDelay);
                }
            }
            return delays;
 */ 
        }

        public static void EmployeeAssociate(JobTask jobTask, Employee employee)
        {
            JobTaskDAL.EmployeeAssociate(jobTask, employee);
        }

        public static void EmployeeAssociate(JobTask jobTask, object employeeID)
        {
            JobTaskDAL.EmployeeAssociate(jobTask, employeeID);
        }

        public static void EmployeeDissociate(JobTask jobTask)
        {
            JobTaskDAL.EmployeeDissociate(jobTask);
        }

        public static void EmployeeConfirm(JobTask jobTask, Employee employee, SystemUser initiator)
        {
            JobTaskDAL.EmployeeConfirm(jobTask, employee, DateTime.Now);
            Job job = JobBLL.Read(jobTask.JobID);
            // Communicate with the builder's super through their preferred communication method.
            //            CommunicationBLL.BuilderJobTaskAccept(jobTask, subcontractor, initiator);
        }

        public static void EmployeeReject(JobTask jobTask, Employee employee, SystemUser initiator)
        {
            JobTaskDAL.EmployeeReject(jobTask, employee, DateTime.Now);
            Job job = JobBLL.Read(jobTask.JobID);
            // Communicate with the builder's super through their preferred communication method.
            //  CommunicationBLL.BuilderJobTaskReject(jobTask, subcontractor, initiator);
        }

        public static void EmployeeFinish(JobTask jobTask, Employee employee, SystemUser initiator)
        {
            JobTaskDAL.EmployeeFinish(jobTask, employee, DateTime.Now);
            Job job = JobBLL.Read(jobTask.JobID);
            // Communicate with the builder's super through their preferred communication method.
            //    CommunicationBLL.BuilderJobTaskFinish(jobTask, subcontractor, initiator);
        }

        public static void SubcontractorAssociate(JobTask jobTask, Subcontractor subcontractor)
        {
            JobTaskDAL.SubcontractorAssociate(jobTask, subcontractor);
        }

        public static void SubcontractorAssociate(JobTask jobTask, object subcontractorID)
        {
            JobTaskDAL.SubcontractorAssociate(jobTask, subcontractorID);
        }

        public static void SubcontractorDissociate(JobTask jobTask)
        {
            JobTaskDAL.SubcontractorDissociate(jobTask);
        }

        public static void SubcontractorConfirm(JobTask jobTask, Subcontractor subcontractor, SystemUser initiator)
        {
            JobTaskDAL.SubcontractorConfirm(jobTask, subcontractor, DateTime.Now);
            Job job = JobBLL.Read(jobTask.JobID);
            // Communicate with the builder's super through their preferred communication method.
            CommunicationBLL.BuilderJobTaskAccept(jobTask, subcontractor, initiator);
        }

        public static void SubcontractorReject(JobTask jobTask, Subcontractor subcontractor, SystemUser initiator)
        {
            JobTaskDAL.SubcontractorReject(jobTask, subcontractor, DateTime.Now);
            Job job = JobBLL.Read(jobTask.JobID);
            // Communicate with the builder's super through their preferred communication method.
            CommunicationBLL.BuilderJobTaskReject(jobTask, subcontractor, initiator);
        }

        public static void SubcontractorFinish(JobTask jobTask, Subcontractor subcontractor, SystemUser initiator)
        {
            JobTaskDAL.SubcontractorFinish(jobTask, subcontractor, DateTime.Now);
            Job job = JobBLL.Read(jobTask.JobID);
            // Communicate with the builder's super through their preferred communication method.
            CommunicationBLL.BuilderJobTaskFinish(jobTask, subcontractor, initiator);
        }

        public static void WorkerDissociate(JobTask jobTask)
        {
            JobTaskBLL.EmployeeDissociate(jobTask);
            JobTaskBLL.SubcontractorDissociate(jobTask);
        }
    }

    public static class JobDelayBLL
    {
        public static JobDelay Create(JobDelay jobDelay)
        {
            return JobDelayDAL.Create(jobDelay);
        }

        public static JobDelay Read(object jobDelayID)
        {
            return JobDelayDAL.Read(jobDelayID);
        }

        public static List<JobDelay> List(object jobID)
        {
            return JobDelayDAL.List(jobID);
        }

        public static void AssociateJobTask(object jobDelayID, object jobTaskID, DateTime originalStart, DateTime originalFinish, DateTime start, DateTime finish)
        {
            JobDelayDAL.AssociateJobTask(jobDelayID, jobTaskID, originalStart, originalFinish, start, finish);
        }

        public static void DissociateJobTask(object jobDelayID, object jobTaskID)
        {
            JobDelayDAL.DissociateJobTask(jobDelayID, jobTaskID);
        }

        public static List<JobTaskDelay> ListJobTaskDelay(object jobID)
        {
            return JobDelayDAL.ListJobTaskDelay(jobID);
        }

        public static int JobTaskDelayCompareMostRecent(JobTaskDelay jt1, JobTaskDelay jt2)
        {
            return jt2.Entered.CompareTo(jt1.Entered);
        }
    }
}
