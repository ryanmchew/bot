﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuildOnTime.BusinessObject;
using BuildOnTime.DataAccess;

namespace BuildOnTime.BusinessLogic
{
    public static class EmployeeBLL
    {
        public static string DisplayName(Employee employee)
        {
            return employee.LastName + ", " + employee.FirstName;
        }

        public static string RolesToString(List<Role> roles)
        {
            return EmployeeDAL.RolesToString(roles);
        }

        public static List<Role> StringToRoles(string roles)
        {
            return EmployeeDAL.StringToRoles(roles);
        }

        public static Employee CreateEMailPassword(Employee employee)
        {
            string password = SystemUserBLL.GenerateUserPassword();
            Employee result = Create(employee, password);
            // If we had a system user send them an e-mail.
            if (result.SystemUser != null)
            {
                CommunicationBLL.BuilderWelcome(employee.Builder, employee.SystemUser, password);
            }
            return result;
        }


        public static List<Employee> ListSuper(Builder builder)
        {            
            List<Employee> employees = EmployeeDAL.List(builder);
            return employees.Where(em => em.Roles.Contains(Role.Superintendent)).ToList();
        }


        public static Employee Create(Employee employee, string password)
        {
            // Check to see if we have an e-mail if so we need to create a systemUser 
            employee.SystemUser = null;
            if ((employee.Email != null) && (employee.Email.Length > 0))
            {
                SystemUser newSystemUser = new SystemUser(null, employee.Email, employee.Email, employee.Roles);
                employee.SystemUser = newSystemUser;
                SystemUserBLL.ValidateNewUser(newSystemUser);
            }
            return EmployeeDAL.Create(employee, password);
        }

        public static Employee Read(object identifier)
        {
            return EmployeeDAL.Read(identifier);
        }

        public static Employee ReadBySystemUser(object identifier)
        {
            return EmployeeDAL.ReadBySystemUser(identifier);
        }

        public static Employee Update(Employee employee)
        {
            return EmployeeDAL.Update(employee);
        }

        public static List<Employee> List(Builder builder)
        {
            return EmployeeDAL.List(builder);
        }

        public static void Delete(Employee employee)
        {
            EmployeeDAL.Delete(employee);
        }
    }

    public static class EmployeeCommunicationBLL
    {
        public static EmployeeCommunication Create(EmployeeCommunication employeeCommunication)
        {
            return EmployeeCommunicationDAL.Create(employeeCommunication);
        }

        public static List<EmployeeCommunication> Search(object jobTaskID)
        {
            return EmployeeCommunicationDAL.Search(jobTaskID);
        }

        public static List<EmployeeCommunication> SearchJob(object jobID)
        {
            return EmployeeCommunicationDAL.SearchJob(jobID);
        }
    }
}
