﻿using System;
using System.Text;
using BuildOnTime.BusinessObject;
using BuildOnTime.DataAccess;
using System.Collections.Generic;

namespace BuildOnTime.BusinessLogic
{
    public class SystemUserBLL
    {
        public static Role[] ROLE_ADMINISTRATOR = { Role.Administrator, Role.Superintendent };
        public static Role[] ROLE_SUBCONTRACTOR = { Role.Subcontractor };
        public static Role[] ROLE_SUPERINTENDENT = { Role.Superintendent };      

        public static string GenerateUserPassword()
        {
            int length = SystemUserDAL.RequiredPasswordLength();
            const string passwordChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";
            string result = "";
            Random randomizer = new Random();
            for (int i = 0; i < length; i++)
            {
                result += passwordChars[randomizer.Next(0, passwordChars.Length - 1)];
            }
            return result;
        }

        public static string ResetPassword(string username)
        {
            SystemUser user = SystemUserDAL.Read(username);
            if (user == null)
            {
                throw new SystemUserException("Username is not valid.");
            }
            string generatedPassword = SystemUserDAL.ResetPassword(username);
            string newPassword = GenerateUserPassword();
            SystemUserDAL.ChangePassword(user, generatedPassword, newPassword);
            // If we had a system user send them an e-mail.
            CommunicationBLL.SystemUserPasswordReset(user, newPassword);
            return newPassword;
        }

        public static SystemUser Create(SystemUser systemUser, string password)
        {
            return SystemUserDAL.Create(systemUser, password);
        }

        public static SystemUser Read(object identifier)
        {
            return SystemUserDAL.Read(identifier);
        }

        public static SystemUser Read(string username)
        {
            return SystemUserDAL.Read(username);
        }

        public static SystemUser ReadByEMail(string email)
        {
            return SystemUserDAL.ReadByEMail(email);
        }

        public static void ValidateNewUser(SystemUser user)
        {
            try
            {
                CommunicationBLL.ValidateEMailAddress(user.EMail);
            }
            catch (Exception e)
            {
                
                throw new SystemUserException("Email address is invalid.", e);
            }

            if (SystemUserBLL.ReadByEMail(user.EMail) != null)
            {
                throw new SystemUserException("User already exists.");
            }
        }

        public static void UserPasswordChangeUpdate(SystemUser user, bool forcePasswordChange)
        {
            SystemUserDAL.UserPasswordChangeUpdate(user, forcePasswordChange);
        }

        public static bool Login(string username, string password, out SystemUser systemUser)
        {
            return SystemUserDAL.Login(username, password, out systemUser);
        }

        public static void AssociateUserSubcontractor(SystemUser user, object subcontractorID)
        {
            SystemUserDAL.AssociateUserSubcontractor(user, subcontractorID);
        }

        public static List<SystemUser> SearchBySubcontractor(object subcontractorID)
        {
            return SystemUserDAL.SearchBySubcontractor(subcontractorID);
        }

/*
        public static List<Role> GetRoles(SystemUser user)
        {
            List<Role> result = new List<Role>();
            List<string> userRoles = SystemUserDAL.ReadUserRoles(user);
            foreach (string role in userRoles)
            {
                Role userRole = (Role)Enum.Parse(typeof(Role), role);
                result.Add(userRole);
            }
            return result;
        }
 */ 
    }

    public static class SystemUserAuditBLL
    {
        public static SystemUserAudit Create(object systemUserID, AuditEnum action, AuditTargetEnum target, CatalystEnum catalyst, string details)
        {
            return SystemUserAuditDAL.Create(systemUserID, action, target, catalyst, details);
        }

        public static SystemUserAudit Read(object identifier)
        {
            return SystemUserAuditDAL.Read(identifier);
        }

        public static void Delete(object identifier)
        {
            SystemUserAuditDAL.Delete(identifier);
        }

        public static List<SystemUserAudit> FindRecent(object systemUserID, AuditEnum action, AuditTargetEnum target)
        {
            return SystemUserAuditDAL.FindRecent(systemUserID, action, target);
        }
    }
}
