﻿using System;
using System.Text;
using System.Collections.Generic;
using BuildOnTime.BusinessObject;
using BuildOnTime.DataAccess;

namespace BuildOnTime.BusinessLogic
{
    public static class SiteBLL
    {
        public static Site Create(Site site)
        {
            return SiteDAL.Create(site);
        }

        public static Site Read(object identifier)
        {
            return SiteDAL.Read(identifier);
        }

        public static void Update(Site site)
        {
            SiteDAL.Update(site);
        }

        public static void Delete(Site site)
        {
            SiteDAL.Delete(site);
        }

        public static List<Site> Search(string lotIdentifier)
        {
            return SiteDAL.Search(lotIdentifier);
        }
    }
}
