﻿using System;
using System.Collections.Generic;
using System.Text;
using BuildOnTime.BusinessObject;
using BuildOnTime.DataAccess;

namespace BuildOnTime.BusinessLogic
{
    public static class SubcontractorBLL
    {
        public static Subcontractor Create(Subcontractor subcontractor)
        {
            return SubcontractorDAL.Create(subcontractor);
        }

        public static SystemUser CreateUserEMailPassword(Subcontractor subcontractor, string email)
        {
            SystemUser systemUser = new SystemUser(null, email, email, new List<Role>(SystemUserBLL.ROLE_SUBCONTRACTOR));
            SystemUserBLL.ValidateNewUser(systemUser);
            string password = SystemUserBLL.GenerateUserPassword();
            SystemUser subcontractorUser = SystemUserBLL.Create(systemUser, SystemUserBLL.GenerateUserPassword());
            SystemUserBLL.AssociateUserSubcontractor(subcontractorUser, subcontractor.Identifier);
            Builder builder = BuilderBLL.Read(subcontractor.BuilderID);
            CommunicationBLL.SubWelcome(builder, subcontractorUser, password);
            return subcontractorUser;
        }

        public static Subcontractor Read(object subcontractorID)
        {
            return SubcontractorDAL.Read(subcontractorID);
        }

        public static Subcontractor ReadByName(string name)
        {
            return SubcontractorDAL.ReadByName(name);
        }

        public static void Update(Subcontractor subcontractor)
        {
            SubcontractorDAL.Update(subcontractor);
        }

        public static void Delete(Subcontractor subcontractor)
        {
            SubcontractorDAL.Delete(subcontractor);
        }

        public static List<Subcontractor> List(object builderID)
        {
            return SubcontractorDAL.List(builderID);
        }

        public static List<JobTask> ListJobTask(object subcontractorID, DateTime startDate, DateTime endDate)
        {
            return JobTaskDAL.SearchSubcontractorTask(subcontractorID, startDate, endDate);
        }

        public static List<Subcontractor> List(SystemUser subcontractorUser)
        {
            return SubcontractorDAL.List(subcontractorUser);
        }

        public static int CompareSubcontractor(Subcontractor sub1, Subcontractor sub2)
        {
            return sub1.Name.CompareTo(sub2.Name);
        }
    }

    public static class SubcontractorSchedulingBLL
    {
        public static SubcontractorScheduling Create(SubcontractorScheduling subcontractorScheduling)
        {
            return SubcontractorSchedulingDAL.Create(subcontractorScheduling);
        }

        private static void CreateUser(object builderID, SubcontractorScheduling subcontractorScheduling)
        {
            Builder builder = BuilderBLL.Read(builderID);
            SystemUser subcontractorUser = null;
            if ((subcontractorScheduling.EMail != null) && (subcontractorScheduling.EMail.Length > 0))
            {
                SystemUser newSystemUser = new SystemUser(null, subcontractorScheduling.EMail, subcontractorScheduling.EMail, new List<Role>(SystemUserBLL.ROLE_SUBCONTRACTOR));
                SystemUserBLL.ValidateNewUser(newSystemUser);
                string password = SystemUserBLL.GenerateUserPassword();
                subcontractorUser = SystemUserBLL.Create(newSystemUser, SystemUserBLL.GenerateUserPassword());
                SystemUserBLL.AssociateUserSubcontractor(subcontractorUser, subcontractorScheduling.SubcontractorID);
                CommunicationBLL.SubWelcome(builder, subcontractorUser, password);
            }
        }

        public static SubcontractorScheduling CreateEMailPassword(object builderID, SubcontractorScheduling subcontractorScheduling)
        {
            CreateUser(builderID, subcontractorScheduling);
            return SubcontractorSchedulingDAL.Create(subcontractorScheduling);
        }

        public static SubcontractorScheduling Read(object identifier)
        {
            return SubcontractorSchedulingDAL.Read(identifier);
        }

        public static Subcontractor Read(SystemUser user)
        {
            return SubcontractorDAL.Read(user);
        }

        public static void Update(object builderID, SubcontractorScheduling subcontractorScheduling)
        {
            if (SystemUserBLL.ReadByEMail(subcontractorScheduling.EMail) == null)
            {
                CreateUser(builderID, subcontractorScheduling);
            }
            SubcontractorSchedulingDAL.Update(subcontractorScheduling);
        }

        public static void Delete(SubcontractorScheduling subcontractorScheduling)
        {
            SubcontractorSchedulingDAL.Delete(subcontractorScheduling);
        }
    }

    public static class SubcontractorCommunicationBLL
    {
        public static SubcontractorCommunication Create(SubcontractorCommunication subcontractorCommunication)
        {
            return SubcontractorCommunicationDAL.Create(subcontractorCommunication);
        }

        public static List<SubcontractorCommunication> Search(object jobTaskID)
        {
            return SubcontractorCommunicationDAL.Search(jobTaskID);
        }

        public static List<SubcontractorCommunication> Search(object subcontractorID, DateTime start, DateTime end)
        {
            return SubcontractorCommunicationDAL.SearchSubcontractorCommunication(subcontractorID, start, end);
        }

        public static List<SubcontractorCommunication> SearchJob(object jobID)
        {
            return SubcontractorCommunicationDAL.SearchJob(jobID);
        }
    }
}
