﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Net.Mail;
using BuildOnTime.DataAccess;
using BuildOnTime.BusinessObject;
using System.Collections.Specialized;

namespace BuildOnTime.BusinessLogic
{
    public static class CommunicationBLL
    {
        // These constants need to be supplied by e-mail sender
        public const string PASSWORD = "<!--|Password|-->";
        public const string JOB_ID = "<!--|JobID|-->";
        public const string JOB_TASK_ID = "<!--|JobTaskID|-->";
        public const string JOB_TASK_DESCRIPTION = "<!--|JobTaskDescription|-->";
        public const string SUBCONTRACTOR_NAME = "<!--|SubName|-->";
        // These constants need to be supplied by parameters passed in
        public const string BUILDER_NAME = "<!--|BuilderName|-->";
        // These constants are looked up in config files
        private const string SUBCONTRACTOR_JOB_URL = "<!--|SubJobURL|-->";
        private const string BUILDER_JOB_URL = "<!--|BuilderJobURL|-->";        
        private const string LOGIN_URL = "<!--|LoginURL|-->";
        private const string ABOUT_US_URL = "<!--|AboutUsURL|-->";
        private const string CONTACT_US_URL = "<!--|ContactUsURL|-->";


        /// <summary>
        /// This procedure throws a FormatException when the email address is not valid.
        /// </summary>
        /// <param name="email"></param>
        public static void ValidateEMailAddress(string email)
        {
            MailAddress mail = new MailAddress(email);
        }

        private static void AddEMails(MailAddressCollection mac, string eMails)
        {
            if ((eMails != null) && (eMails.Trim().Length > 0))
            {
                string[] mailAddresses = eMails.Split(new char[] { ';' });
                foreach (string mailAddress in mailAddresses)
                {
                    if (mailAddress.Trim().Length > 0)
                    {
                        mac.Add(mailAddress);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="from"></param>
        /// <param name="recipients"></param>
        /// <param name="subject"></param>
        /// <param name="htmlBody"></param>
        public static void SendEMail(string sender, string from, string recipients, string carbonCopy, string blindCarbonCopy, string subject, string htmlBody)
        {
            // Grab authentication
            MailMessage message = new MailMessage();
            message.Sender = new MailAddress("support@buildontime.com");
            message.From = new MailAddress("support@buildontime.com");
            AddEMails(message.To, recipients);
            AddEMails(message.CC, carbonCopy);
            if (blindCarbonCopy == null)
            {
                blindCarbonCopy = ConfigurationManager.AppSettings["BlindCarbon"];
            }
            AddEMails(message.Bcc, blindCarbonCopy);
            message.Subject = subject;
            message.ReplyToList.Add(message.From);
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.IsBodyHtml = true;
            message.Body = htmlBody;
            SmtpClient mailClient = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]);            
            mailClient.UseDefaultCredentials = false;
            // Only pass if needed.
            if ((ConfigurationManager.AppSettings["SMTPUser"] != null) && (ConfigurationManager.AppSettings["SMTPUser"].Trim().Length > 0))
            {
                mailClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUser"], ConfigurationManager.AppSettings["SMTPPassword"]);
            }
            Boolean useSSL = false;
            if (ConfigurationManager.AppSettings["SMTPSSL"] != null)
            {
                useSSL = Boolean.Parse(ConfigurationManager.AppSettings["SMTPSSL"]);
            }
            mailClient.EnableSsl = useSSL;
            mailClient.Send(message);            
        }

        public static EMail SendSupportEMail(Builder builder, SystemUser sender, List<string> recievers, NameValueCollection substitutions, string carbonCopy, string blindCarbonCopy, string subject, string htmlBody)
        {
            string loginURL = ConfigurationManager.AppSettings["WebRoot"] + ConfigurationManager.AppSettings["LoginPage"];
            string aboutURL = ConfigurationManager.AppSettings["WebRoot"] + ConfigurationManager.AppSettings["AboutUsPage"];
            string contactURL = ConfigurationManager.AppSettings["WebRoot"] + ConfigurationManager.AppSettings["ContactUsPage"];
            string subJobURL = ConfigurationManager.AppSettings["WebRoot"] + ConfigurationManager.AppSettings["SubJobPage"];
            string builderJobURL = ConfigurationManager.AppSettings["WebRoot"] + ConfigurationManager.AppSettings["BuilderJobPage"];
            if (recievers.Count == 1)
            {
                loginURL = loginURL + "?Login=" + Uri.EscapeUriString(recievers[0]);
            }
            htmlBody = htmlBody.Replace(LOGIN_URL, loginURL);
            htmlBody = htmlBody.Replace(BUILDER_NAME, (builder != null) ? builder.Name : "");
            htmlBody = htmlBody.Replace(ABOUT_US_URL, aboutURL);
            htmlBody = htmlBody.Replace(CONTACT_US_URL, contactURL);
            htmlBody = htmlBody.Replace(BUILDER_JOB_URL, builderJobURL);
            htmlBody = htmlBody.Replace(SUBCONTRACTOR_JOB_URL, subJobURL);

            subject = subject.Replace(LOGIN_URL, loginURL);
            subject = subject.Replace(BUILDER_NAME, (builder != null) ? builder.Name : "");
            subject = subject.Replace(ABOUT_US_URL, aboutURL);
            subject = subject.Replace(CONTACT_US_URL, contactURL);
            subject = subject.Replace(BUILDER_JOB_URL, builderJobURL);
            subject = subject.Replace(SUBCONTRACTOR_JOB_URL, subJobURL);


            if (substitutions != null)
            {
                string find;
                string replace;
                for (int i = 0; i < substitutions.Count; i++)
                {
                    find = substitutions.GetKey(i);
                    replace = substitutions.Get(i);
                    htmlBody = htmlBody.Replace(find, replace);
                    subject = subject.Replace(find, replace);
                }
            }
            string sendTo = "";
            foreach (string reciever in recievers)
            {
                sendTo += reciever + ";";
            }
            string senderAddress = ConfigurationManager.AppSettings.Get("SMTPUser");
            string senderReplyTo = senderAddress;
            EMail email = null;
            if (sender != null)
            {
                senderReplyTo = senderAddress;
                email = new EMail();
                email.BCC = blindCarbonCopy;
                email.Body = htmlBody;
                email.CC = carbonCopy;
                email.EmailTemplateID = null;
                email.From = "support@buildontime.com";
                email.To = string.Join(";", recievers.ToArray());
                email.SenderSystemUserID = sender.Identifier;
                email.Sent = DateTime.Now;
                EMailRecordBLL.Create(email);
            }
            SendEMail(senderAddress, senderReplyTo, sendTo, carbonCopy, blindCarbonCopy, subject, htmlBody);
            return email;
        }

        public static EMail SendEMail(Builder builder, SystemUser sender, List<string> recievers, NameValueCollection substitutions, string carbonCopy, string blindCarbonCopy, string subject, string htmlBody)
        {
            string loginURL = ConfigurationManager.AppSettings["WebRoot"] + ConfigurationManager.AppSettings["LoginPage"];
            string aboutURL = ConfigurationManager.AppSettings["WebRoot"] + ConfigurationManager.AppSettings["AboutUsPage"];
            string contactURL = ConfigurationManager.AppSettings["WebRoot"] + ConfigurationManager.AppSettings["ContactUsPage"];
            string subJobURL = ConfigurationManager.AppSettings["WebRoot"] + ConfigurationManager.AppSettings["SubJobPage"];
            string builderJobURL = ConfigurationManager.AppSettings["WebRoot"] + ConfigurationManager.AppSettings["BuilderJobPage"];
            if (recievers.Count == 1)
            {
                loginURL = loginURL + "?Login=" + Uri.EscapeUriString(recievers[0]);
            }
            htmlBody = htmlBody.Replace(LOGIN_URL, loginURL);
            htmlBody = htmlBody.Replace(BUILDER_NAME, (builder != null) ? builder.Name: "");
            htmlBody = htmlBody.Replace(ABOUT_US_URL, aboutURL);
            htmlBody = htmlBody.Replace(CONTACT_US_URL, contactURL);
            htmlBody = htmlBody.Replace(BUILDER_JOB_URL, builderJobURL);
            htmlBody = htmlBody.Replace(SUBCONTRACTOR_JOB_URL, subJobURL);
            
            subject = subject.Replace(LOGIN_URL, loginURL);
            subject = subject.Replace(BUILDER_NAME, (builder != null) ? builder.Name : "");
            subject = subject.Replace(ABOUT_US_URL, aboutURL);
            subject = subject.Replace(CONTACT_US_URL, contactURL);
            subject = subject.Replace(BUILDER_JOB_URL, builderJobURL);
            subject = subject.Replace(SUBCONTRACTOR_JOB_URL, subJobURL);

            
            if (substitutions != null)
            {
                string find;
                string replace;
                for (int i = 0; i < substitutions.Count; i++)
                {
                    find = substitutions.GetKey(i);
                    replace = substitutions.Get(i);
                    htmlBody = htmlBody.Replace(find, replace);
                    subject = subject.Replace(find, replace);
                }
            }
            string sendTo = "";
            foreach(string reciever in recievers)
            {
                sendTo += reciever + ";";
            }
            string senderAddress = ConfigurationManager.AppSettings.Get("SMTPUser");
            string senderReplyTo = senderAddress;
            EMail email = null;
            if (sender != null)
            {
                senderReplyTo = sender.EMail;
                email = new EMail();
                email.BCC = blindCarbonCopy;
                email.Body = htmlBody;
                email.CC = carbonCopy;
                email.EmailTemplateID = null;
                email.From = "support@buildontime.com";                
                email.To = string.Join(";", recievers.ToArray());
                email.SenderSystemUserID = sender.Identifier;
                email.Sent = DateTime.Now;
                EMailRecordBLL.Create(email);
            }
            SendEMail(senderAddress, senderReplyTo, sendTo, carbonCopy, blindCarbonCopy, subject, htmlBody);
            return email;
        }

        public static EMail BuilderWelcome(Builder builder, SystemUser systemUser, string password)
        {
            EMailTemplate template = EMailTemplateBLL.Read(EMailTemplateEnum.BuilderWelcome);
            NameValueCollection replacements = new NameValueCollection();
            replacements.Add(PASSWORD, password);
            List<string> recievers = new List<string>();
            recievers.Add(systemUser.EMail);
            return SendEMail(builder, null, recievers, replacements, null, null, template.Subject, template.Body); 
        }

        private static void jobTaskTemplateSubstitutions(JobTask jobTask, Subcontractor sub, out NameValueCollection replacements, out Job job, out Builder builder, out Employee super)
        {
            replacements = new NameValueCollection();
            replacements.Add(JOB_ID, jobTask.JobID.ToString());
            replacements.Add(JOB_TASK_ID, jobTask.Identifier.ToString());
            replacements.Add(JOB_TASK_DESCRIPTION, jobTask.TaskName.ToString());
            replacements.Add(SUBCONTRACTOR_NAME, sub.Name);
            job = JobBLL.Read(jobTask.JobID);
            builder = BuilderBLL.Read(job.BuilderID);
            // ??? Is this right ???
            super = EmployeeBLL.Read(job.SuperID);
        }

        private static EMail jobTaskSuperEMail(JobTask jobTask, Subcontractor sub, SystemUser initiator, EMailTemplateEnum templateEnum)
        {
            NameValueCollection nameValues;
            Job job;
            Builder builder;
            Employee super;
            EMailTemplate template = EMailTemplateBLL.Read(templateEnum);
            jobTaskTemplateSubstitutions(jobTask, sub, out nameValues, out job, out builder, out super);
            List<string> recievers = new List<string>();
            recievers.Add(super.Email);
            return SendEMail(builder, initiator, recievers, nameValues, null, null, template.Subject, template.Body); 
        }

        private static EMail jobTaskSubEMail(JobTask jobTask, SystemUser initiator, EMailTemplateEnum templateEnum)
        {
            NameValueCollection nameValues;
            Job job;
            Builder builder;
            Employee super;
            SubcontractorScheduling subSchedule;
            EMailTemplate template = EMailTemplateBLL.Read(templateEnum);
            Subcontractor sub = SubcontractorBLL.Read(jobTask.SubcontractorID);
            jobTaskTemplateSubstitutions(jobTask, sub, out nameValues, out job, out builder, out super);
            subSchedule = SubcontractorSchedulingBLL.Read(sub.Identifier);
            List<string> recievers = new List<string>();
            recievers.Add(subSchedule.EMail);
            List<SystemUser> systemUsers = SystemUserBLL.SearchBySubcontractor(sub.Identifier);
            foreach (SystemUser user in systemUsers)
            {
                if (!recievers.Contains(user.EMail))
                {
                    recievers.Add(user.EMail);
                }
            }
            return SendEMail(builder, initiator, recievers, nameValues, null, null, template.Subject, template.Body);
        }

        public static EMail BuilderJobTaskAccept(JobTask jobTask, Subcontractor sub, SystemUser initiator)
        {
            return jobTaskSuperEMail(jobTask, sub, initiator, EMailTemplateEnum.BuilderJobAccepted);            
        }

        public static EMail BuilderJobTaskReject(JobTask jobTask, Subcontractor sub, SystemUser initiator)
        {
            return jobTaskSuperEMail(jobTask, sub, initiator, EMailTemplateEnum.BuilderJobRejected);
        }

        public static EMail BuilderJobTaskFinish(JobTask jobTask, Subcontractor sub, SystemUser initiator)
        {
            return jobTaskSuperEMail(jobTask, sub, initiator, EMailTemplateEnum.BuilderJobFinished);
        }

        public static EMail SubJobTaskReminder(JobTask jobTask, SystemUser initiator)
        {
            return jobTaskSubEMail(jobTask, initiator, EMailTemplateEnum.SubJobReminder);            
        }

        public static EMail SubJobTaskRequest(JobTask jobTask, SystemUser initiator)
        {
            return jobTaskSubEMail(jobTask, initiator, EMailTemplateEnum.SubJobRequest);
        }
        
        public static EMail SubJobTaskUpdate(JobTask jobTask, SystemUser initiator)
        {
            return jobTaskSubEMail(jobTask, initiator, EMailTemplateEnum.SubJobUpdated);
        }

        public static EMail SubCompositeEmail(JobTask jobTask, SystemUser initiator)
        {
            return jobTaskSubEMail(jobTask, initiator, EMailTemplateEnum.SubScheduleUpdate);
        }

        public static EMail SubWelcome(Builder builder, SystemUser systemUser, string password)
        {
            EMailTemplate template = EMailTemplateBLL.Read(EMailTemplateEnum.SubWelcome);
            NameValueCollection replacements = new NameValueCollection();
            replacements.Add(PASSWORD, password);
            List<string> recievers = new List<string>();
            recievers.Add(systemUser.EMail);
            return SendEMail(builder, systemUser, recievers, replacements, null, null, template.Subject, template.Body); 
        }

        public static EMail SystemUserPasswordReset(SystemUser systemUser, string password)
        {
            EMailTemplate template = EMailTemplateBLL.Read(EMailTemplateEnum.PasswordReset);
            NameValueCollection replacements = new NameValueCollection();
            replacements.Add(PASSWORD, password);
            List<string> recievers = new List<string>();
            recievers.Add(systemUser.EMail);
            return SendSupportEMail(null, systemUser, recievers, replacements, null, null, template.Subject, template.Body); 
        }
    }

    public static class EMailTemplateBLL
    {
        public static EMailTemplate Create(EMailTemplate EMailTemplate)
        {
            return EMailTemplateDAL.Create(EMailTemplate);
        }

        public static EMailTemplate Read(object identifier)
        {
            return EMailTemplateDAL.Read(identifier);
        }

        public static EMailTemplate Read(EMailTemplateEnum emailTemplate)
        {
            return EMailTemplateDAL.Read(emailTemplate);
        }

        public static void Update(EMailTemplate EMailTemplate)
        {
            EMailTemplateDAL.Update(EMailTemplate);
        }

        public static void Delete(EMailTemplate EMailTemplate)
        {
            EMailTemplateDAL.Delete(EMailTemplate);
        }

        public static List<EMailTemplate> Search(string name)
        {
            return EMailTemplateDAL.Search(name, null, null);
        }
    }

    public static class EMailRecordBLL
    {
        public static EMail Create(EMail EMail)
        {
            return EMailDAL.Create(EMail);
        }

        public static EMail Read(object identifier)
        {
            return EMailDAL.Read(identifier);
        }

        public static void Update(EMail EMail)
        {
            EMailDAL.Update(EMail);
        }

        public static void Delete(EMail EMail)
        {
            EMailDAL.Delete(EMail);
        }

        public static List<EMail> Search(object systemUserID, DateTime startDate, DateTime endDate)
        {
            return EMailDAL.Search(systemUserID, startDate, endDate);
        }
    }
}
