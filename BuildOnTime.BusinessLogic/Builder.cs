﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuildOnTime.BusinessObject;
using BuildOnTime.DataAccess;

namespace BuildOnTime.BusinessLogic
{
    public static class BuilderBLL
    {
        public static Builder Create(Builder builder)
        {
            return BuilderDAL.Create(builder);
        }

        public static Builder Read(object identifier)
        {
            return BuilderDAL.Read(identifier);
        }

        public static void Update(Builder builder)
        {
            BuilderDAL.Update(builder);
        }

        public static void Delete(Builder builder)
        {
            BuilderDAL.Delete(builder);
        }

        public static List<Builder> List()
        {
            return BuilderDAL.List();
        }

        /// <summary>
        /// BuilderSignup takes a new builder with an employee record.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="employee"></param>
        public static void BuilderSignup(Builder builder, Employee employee)
        {
            // !!! Validate that we have all necessary information  !!!
/*
            CommunicationBLL.ValidateEMailAddress(employee.EMail);
            if (SystemUserBLL.ReadByEMail(employee.EMail) != null)
            {
                throw new Exception();
            }
 */ 
            Builder newBuilder = BuilderBLL.Create(builder);
            employee.Builder = newBuilder;
            employee.Roles.Add(Role.Administrator);
            employee.Roles.Add(Role.Superintendent);
            string password = SystemUserBLL.GenerateUserPassword();
            Employee newEmployee = EmployeeBLL.Create(employee, password);
            // Since this is the signup the new employee has to have an associated systemUser.
            CommunicationBLL.BuilderWelcome(builder, newEmployee.SystemUser, password);
        }
/*
        public static void BuilderSignup(Builder builder, UserBuilder user)
        {
            // !!! Validate that we have all necessary information  !!!
            Builder newBuilder = BuilderBLL.Create(builder);
            user.BuilderID = newBuilder.Identifier;
            List<string> roles = new List<string>();
            roles.Add(Role.Administrator.ToString());
            roles.Add(Role.Superintendent.ToString());
            EmployeeBLL.Create(
            UserBLL.CreateUserEMailPassword(newBuilder, user, roles);
        }
 */ 
    }
}
