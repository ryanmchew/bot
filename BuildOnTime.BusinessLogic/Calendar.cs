﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuildOnTime.BusinessLogic
{
    public enum MonthEnum
    {
        January = 1,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    }

    public static class CalendarBLL
    {
        /// <summary>
        /// This method counts working days.
        /// </summary>
        /// <param name="startDate">Start date to start counting from</param>
        /// <param name="duration">Length of days to count</param>
        /// <returns></returns>
        public static DateTime FindWorkDate(DateTime startDate, int duration)
        {
            DateTime result = startDate;
            // First find start date
            int directional = 1;
            if (duration < 0)
            {
                directional = -1;
            }
            duration = Math.Abs(duration);
            int i = 0;
            while (i < duration)
            {
                // Make sure we are going in the right direction.
                result = result.AddDays(directional);
                if ((result.DayOfWeek != DayOfWeek.Saturday) && (result.DayOfWeek != DayOfWeek.Sunday))
                {
                    i++;
                }
            }
            return result;
        }

        public static bool IsWorkDate(DateTime date)
        {
            return ((date.DayOfWeek != DayOfWeek.Saturday) && (date.DayOfWeek != DayOfWeek.Sunday));
        }

        public static int WorkDateDifference(DateTime date1, DateTime date2)
        {
            int sign = (date1 <= date2) ? 1: -1;
            int dayCounter = 0;
            DateTime dateMover = (date1 <= date2) ? date1 : date2;
            DateTime bigDate = (date1 <= date2) ? date2 : date1;
            while (dateMover < bigDate)
            {
                if ((dateMover.DayOfWeek != DayOfWeek.Saturday) && (dateMover.DayOfWeek != DayOfWeek.Sunday))
                {
                    dayCounter++;
                }
                dateMover = dateMover.AddDays(1);
            }
            return sign * dayCounter;
        }
    }
}
