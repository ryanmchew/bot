﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuildOnTime.BusinessObject;
using BuildOnTime.DataAccess;
using System.Configuration;

namespace BuildOnTime.BusinessLogic
{
   /*
    public static class UserBLL
    {
        
        public static bool Login(string username, string password)
        {
            return UserDAL.Login(username, password);
        }

        public static bool ChangePassword(SystemUser user, string oldPassword, string newPassword)
        {
            return UserDAL.ChangePassword(user, oldPassword, newPassword);
        }

        /// <summary>
        /// This procedure can throw several exceptions:
        ///     FormatException - in the case of an poorly formed e-mail
        ///     
        /// </summary>
        /// <param name="user"></param>
        public static void ValidateUserInformation(SystemUser user)
        {
            CommunicationBLL.ValidateEMailAddress(user.EMail);
        }

        /// <summary>
        /// This procedure can throw several exceptions:
        ///     FormatException - in the case of an poorly formed e-mail
        ///     
        /// </summary>
        /// <param name="user"></param>
        public static void ValidateNewUserInformation(SystemUser user)
        {
            ValidateUserInformation(user);
            CommunicationBLL.ValidateEMailAddress(user.EMail);
        }

        /// <summary>
        /// This procedure expects to have a user validated with ValidateNewUserInformation sent to it.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string CreateUserGeneratePassword(ref SystemUser user)
        {
            string password = UserDAL.GenerateUserPassword(7);
            user.Username = user.EMail;
            SystemUser persistedUser = UserDAL.Create(user, password);
            UserDAL.UserPasswordChangeUpdate(persistedUser, true);
            user = persistedUser;
            return password;
        }

        public static string GetPassword(SystemUser user)
        {
            return UserDAL.GetPassword(user);
        }

        public static void UserPasswordChangeUpdate(SystemUser user, bool forcePasswordChange)
        {
            UserDAL.UserPasswordChangeUpdate(user, forcePasswordChange);
        }

        private static void CreateSystemUserEMailPassword(Builder builder, SystemUser user, List<string> roles)
        {
            string password = CreateUserGeneratePassword(ref user);
            UserBLL.CreateUserRoles(user, roles);
            CommunicationBLL.BuilderWelcome(builder, user, password);
        }

        public static void CreateUserEMailPassword(Builder builder, UserSubcontractor user, List<string> roles)
        {
            CreateSystemUserEMailPassword(builder, user, roles);
            UserDAL.AssociateUserSubcontractor(user, user.SubcontractorID);
        }

        public static void CreateUserEMailPassword(Builder builder, UserBuilder user, List<string> roles)
        {
            if ((user.EMail != null) && (user.EMail.Length > 0))
            {
                CreateSystemUserEMailPassword(builder, user, roles);
                UserDAL.AssociateUserBuilder(user, user.BuilderID);
            }
            else
            {
                //UserDAL.C
            }            
        }

        public static SystemUser CreateUser(SystemUser user, string password)
        {
            return UserDAL.Create(user, password);
        }

        public static SystemUser ReadUser(object identity)
        {
            return UserDAL.Read(identity);
        }

        public static SystemUser ReadUser(string username)
        {
            return UserDAL.Read(username);
        }

        public static UserBuilder ReadBuilderUser(object userID)
        {
            SystemUser user = ReadUser(userID);
            UserBuilder result = null;
            Builder builder = BuilderDAL.Read(user);
            // Only return the UserBuilder if the user is associated with a builder
            if (builder != null)
            {
                result = new UserBuilder(user);
                result.BuilderID = builder.Identifier;
            }
            return result;
        }

        public static UserBuilder ReadBuilderUser(string username)
        {
            SystemUser user = ReadUser(username);
            UserBuilder result = null;
            Builder builder = BuilderDAL.Read(user);
            // Only return the UserBuilder if the user is associated with a builder
            if (builder != null)
            {
                result = new UserBuilder(user);
                result.BuilderID = builder.Identifier;
            }
            return result;
        }

        public static UserSubcontractor ReadSubcontractorUser(object userID)
        {
            SystemUser user = ReadUser(userID);
            UserSubcontractor result = null;
            Subcontractor subcontractor = SubcontractorDAL.Read(user);
            // Only return the UserSubcontractor if the user is associated with a Subcontractor
            if (subcontractor != null)
            {
                result = new UserSubcontractor(user);
                result.SubcontractorID = subcontractor.Identifier;
            }
            return result;
        }

        public static UserSubcontractor ReadSubcontractorUser(string userName)
        {
            SystemUser user = ReadUser(userName);
            UserSubcontractor result = null;
            Subcontractor subcontractor = SubcontractorDAL.Read(user);
            // Only return the UserSubcontractor if the user is associated with a Subcontractor
            if (subcontractor != null)
            {
                result = new UserSubcontractor(user);
                result.SubcontractorID = subcontractor.Identifier;
            }
            return result;
        }

        public static void CreateUserRoles(SystemUser user, List<string> roles)
        {
            UserDAL.CreateUserRoles(user, roles);
        }

        public static List<string> ReadUserRoles(SystemUser user)
        {
            return UserDAL.ReadUserRoles(user);
        }

        public static List<Role> GetRoles(SystemUser user)
        {
            List<Role> result = new List<Role>();
            List<string> userRoles = ReadUserRoles(user);
            foreach (string role in userRoles)
            {
                Role userRole = (Role)Enum.Parse(typeof(Role), role);
                result.Add(userRole);
            }
            return result;
        }

        public static List<SystemUser> List(object builderID)
        {
            Builder builder = BuilderDAL.Read(builderID);
            return UserDAL.Search(builder);
        }
   */
    }

