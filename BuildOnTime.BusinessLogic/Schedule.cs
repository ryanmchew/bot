﻿using System;
using System.Collections.Generic;
using System.Text;
using BuildOnTime.BusinessObject;
using BuildOnTime.DataAccess;

namespace BuildOnTime.BusinessLogic
{
    public static class ScheduleBLL
    {
        public static Schedule Create(object builderID, string name)
        {
            return ScheduleDAL.Create(builderID, name);
        }

        public static Schedule Read(object identifier)
        {
            return ScheduleDAL.Read(identifier);
        }

        public static void Update(Schedule schedule)
        {
            ScheduleDAL.Update(schedule);
        }

        public static void Delete(Schedule schedule)
        {
            ScheduleDAL.Delete(schedule);
        }

        public static Schedule Save(Schedule schedule)
        {
            Schedule result = null;
            if (schedule.Identifier == null)
            {
                result = ScheduleDAL.Create(schedule.BuilderID, schedule.Name);
            }
            else
            {
                ScheduleDAL.Update(schedule);
                result = schedule;
            }
            return result;
        }

        public static List<Schedule> Search(object builderID, string name)
        {
            return ScheduleDAL.Search(builderID, name);
        }
    }

    public static class ScheduleTaskBLL
    {
        /// <summary>
        /// Return 0 if scheduleTasks are equal, return 1 if scheduleTask1 is greater than scheduleTask2,
        /// and return -1 if scheduleTask2 is greater than scheduleTask1
        /// </summary>
        /// <param name="scheduleTask1"></param>
        /// <param name="scheduleTask2"></param>
        /// <returns></returns>
        public static int CompareScheduleTaskByStartDay(ScheduleTask scheduleTask1, ScheduleTask scheduleTask2)
        {
            int result = 0;
            if ((scheduleTask1 == null) && (scheduleTask2 != null))
            {
                result = -1;
            }
            else
            {
                if ((scheduleTask2 == null) && (scheduleTask1 != null))
                {
                    result = 1;
                }
                else
                {
                    result = scheduleTask1.DayNumber.CompareTo(scheduleTask2.DayNumber);
                    if (result == 0)
                    {
                        result = scheduleTask1.Duration.CompareTo(scheduleTask2.Duration);
                    }
                }
            }
            return result;
        }

        public static ScheduleTask Create(object scheduleIdentifier, object deafultEmployeeID, object defaultSubcontractorID, string task, int dayNumber, int duration, TaskGroupEnum? group)
        {
            return ScheduleTaskDAL.Create(scheduleIdentifier, deafultEmployeeID, defaultSubcontractorID, task, dayNumber, duration, group);
        }

        public static ScheduleTask Read(object identifier)
        {
            return ScheduleTaskDAL.Read(identifier);
        }

        public static void Update(ScheduleTask scheduleTask)
        {
            ScheduleTaskDAL.Update(scheduleTask);
        }

        public static void Delete(ScheduleTask scheduleTask)
        {
            ScheduleTaskDAL.Delete(scheduleTask);
        }

        public static void Save(ScheduleTask scheduleTask)
        {
            if (scheduleTask.Identifier == null)
            {
                Create(scheduleTask.ScheduleIdentifier, scheduleTask.DefaultEmployeeID, scheduleTask.DefaultSubcontractorID, scheduleTask.Name, scheduleTask.DayNumber, scheduleTask.Duration, scheduleTask.Group);
            }
            else
            {
                Update(scheduleTask);
            }
        }

        public static List<ScheduleTask> List(object scheduleID)
        {
            List<ScheduleTask> result = ScheduleTaskDAL.List(scheduleID);
            result.Sort(delegate(ScheduleTask st1, ScheduleTask st2) { return st1.DayNumber.CompareTo(st2.DayNumber); });
            return result;
        }

        public static void TaskDependencyCreate(object scheduleID, object parentTaskID, object dependentTaskID)
        {
            ScheduleTaskDAL.TaskDependencyCreate(scheduleID, parentTaskID, dependentTaskID);
        }

        public static void TaskDependencyDelete(object scheduleID, object parentTaskID, object dependentTaskID)
        {
            ScheduleTaskDAL.TaskDependencyDelete(scheduleID, parentTaskID, dependentTaskID);
        }

        public static void TaskDependencyDeleteBySchedule(object scheduleID)
        {
            ScheduleTaskDAL.TaskDependencyDeleteBySchedule(scheduleID);
        }

        public static List<ScheduleTaskDependency> TaskDependencyList(object scheduleID)
        {
            return ScheduleTaskDAL.TaskDependencyList(scheduleID);
        }
    }
}
